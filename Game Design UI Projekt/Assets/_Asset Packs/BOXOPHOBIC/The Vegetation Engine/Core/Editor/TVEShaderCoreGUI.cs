﻿//Cristian Pop - https://boxophobic.com/

using UnityEngine;
using UnityEditor;

public class TVEShaderCoreGUI : ShaderGUI
{
    //bool enabled = true;
    //bool switched = false;

    Material material;
    int surface;
    int blend;
    int clip;
    int queueOffset;
    int queueOffsetRange;
    int shaderQuality = -1;
    int shaderQualityOld = -1;

    //enum RenderShader
    //{
    //    Simple = 10,
    //    Standard = 20,
    //    Advanced = 30,
    //}

    public override void AssignNewShaderToMaterial(Material material, Shader oldShader, Shader newShader)
    {
        base.AssignNewShaderToMaterial(material, oldShader, newShader);

        if (newShader.name.Contains("Bark Advanced Lit"))
        {
            if (material.enableInstancing == true)
            {
                material.enableInstancing = false;
                Debug.Log("[The Vegetation Engine] Material Instancing disabled when Standard High Tessellation shader is used! Re-enable it when Standard Medium and Low is used!");
            }
        }

        //switched = true;
    }

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] props)
    {
        base.OnGUI(materialEditor, props);

        material = materialEditor.target as Material;

        //if (enabled == true)
        //{
        //    GetShaderQuality();
        //    enabled = false;
        //}

        //if (switched == true)
        //{
        //    GetShaderQuality();
        //    switched = false;
        //}

        //SetShaderQuality();

        // Set Base render settings
        TheVegetationEngine.TVEShaderUtils.SetRenderSettings(material);

        SetColorMode();
        SetSubsurfaceTexValue();
        SetKeywords();
        SetLegacyProps();

        AvoidInstancingWithTessellation();

        materialEditor.LightmapEmissionProperty(0);
        foreach (Material target in materialEditor.targets)
            target.globalIlluminationFlags &= ~MaterialGlobalIlluminationFlags.EmissiveIsBlack;
    }

    //void GetShaderQuality()
    //{
    //    if (material.shader.name.Contains("Simple"))
    //    {
    //        material.SetInt("__shader", 10);
    //    }
    //    else if (material.shader.name.Contains("Standard"))
    //    {
    //        material.SetInt("__shader", 20);
    //    }
    //    else if (material.shader.name.Contains("Advanced"))
    //    {
    //        material.SetInt("__shader", 30);
    //    }
    //}

    //void SetShaderQuality()
    //{
    //    shaderQuality = material.GetInt("__shader");

    //    if (shaderQualityOld != shaderQuality)
    //    {
    //        var shaderName = material.shader.name;

    //        shaderName = shaderName.Replace("Simple", "XXX");
    //        shaderName = shaderName.Replace("Standard", "XXX");
    //        shaderName = shaderName.Replace("Advanced", "XXX");

    //        AssignNewShaderToMaterial(material, material.shader, Shader.Find(shaderName.Replace("XXX", ((RenderShader)material.GetInt("__shader")).ToString())));

    //        shaderQualityOld = shaderQuality;
    //    }
    //}

    void SetColorMode()
    {
        if (material.HasProperty("_MainColorMode"))
        {
            if (material.GetInt("_MainColorMode") == 0)
            {
                material.SetColor("_MainColorVariation", material.GetColor("_MainColor"));
            }
        }
    }

    void SetSubsurfaceTexValue()
    {
        if (material.HasProperty("_SubsurfaceTex"))
        {
            if (material.GetTexture("_SubsurfaceTex") != null)
            {
                material.SetInt("_SubsurfaceTexValue", 0);
            }
            else
            {
                material.SetInt("_SubsurfaceTexValue", 1);
            }
        }
    }

    void SetKeywords()
    {
        if (material.HasProperty("_DetailMode"))
        {
            if (material.GetInt("_DetailMode") == 0)
            {
                material.EnableKeyword("_DETAIL_OFF");
                material.DisableKeyword("_DETAIL_DETAIL");
                material.DisableKeyword("_DETAIL_HEIGHT");
            }
            else if (material.GetInt("_DetailMode") == 1)
            {
                material.DisableKeyword("_DETAIL_OFF");
                material.EnableKeyword("_DETAIL_DETAIL");
                material.DisableKeyword("_DETAIL_HEIGHT");
            }
            else if (material.GetInt("_DetailMode") == 2)
            {
                material.DisableKeyword("_DETAIL_OFF");
                material.DisableKeyword("_DETAIL_DETAIL");
                material.EnableKeyword("_DETAIL_HEIGHT");
            }
        }
    }

    void SetLegacyProps()
    {
        material.SetColor("_Color", material.GetColor("_MainColor"));
        material.SetTexture("_MainTex", material.GetTexture("_MainAlbedoTex"));
        material.SetTextureScale("_MainTex", new Vector2(material.GetVector("_MainUVs").x, material.GetVector("_MainUVs").y));
        material.SetTextureOffset("_MainTex", new Vector2(material.GetVector("_MainUVs").z, material.GetVector("_MainUVs").w));
    }

    void AvoidInstancingWithTessellation()
    {
        if (material.shader.name.Contains("Bark Advanced Lit"))
        {
            material.enableInstancing = false;
        }
    }
}

