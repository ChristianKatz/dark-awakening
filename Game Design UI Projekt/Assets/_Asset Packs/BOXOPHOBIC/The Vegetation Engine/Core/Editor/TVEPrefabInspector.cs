﻿//Cristian Pop - https://boxophobic.com/

using UnityEngine;
using UnityEditor;

namespace TheVegetationEngine
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(TVEPrefab))]
    public class TVEPrefabInspector : Editor
    {
        private static readonly string excludeProps = "m_Script";

        public override void OnInspectorGUI()
        {
            DrawInspector();

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Open Prefab Convertor"/*, GUILayout.Width(160)*/))
            {
                TVEPrefabConverter window = EditorWindow.GetWindow<TVEPrefabConverter>(false, "Prefab Convertor", true);
                window.minSize = new Vector2(480, 280);
                window.Show();
            }

            if (GUILayout.Button("Open Prefab Settings"/*, GUILayout.Width(160)*/))
            {
                TVEPrefabSettings window = EditorWindow.GetWindow<TVEPrefabSettings>(false, "Prefab Settings", true);
                window.minSize = new Vector2(389, 200);
                window.Show();         
            }

            GUILayout.EndHorizontal();

            GUILayout.Space(5);
        }

        void DrawInspector()
        {
            serializedObject.Update();

            DrawPropertiesExcluding(serializedObject, excludeProps);

            serializedObject.ApplyModifiedProperties();
        }
    }
}


