﻿//Cristian Pop - https://boxophobic.com/

using UnityEngine;
using UnityEditor;

namespace TheVegetationEngine
{
    public class TVEShaderUtils
    {
        public static void SetRenderSettings(Material material)
        {
            // Set Render Mode
            if (material.HasProperty("__surface") && material.HasProperty("__blend"))
            {
                int surface = material.GetInt("__surface");
                int blend = material.GetInt("__blend");
                int zwrite = material.GetInt("__zw");

                if (surface == 0)
                {
                    material.SetOverrideTag("RenderType", "Opaque");
                    material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Geometry;

                    // Standard and Universal Render Pipeline
                    material.SetInt("__src", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("__dst", (int)UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt("__zw", 1);
                    material.SetInt("__blend", 0);
                    material.SetInt("__premul", 0);

                    // HD Render Pipeline
                    material.DisableKeyword("_SURFACE_TYPE_TRANSPARENT");
                    material.DisableKeyword("_ENABLE_FOG_ON_TRANSPARENT");

                    material.DisableKeyword("_BLENDMODE_ALPHA");
                    material.DisableKeyword("_BLENDMODE_ADD");
                    material.DisableKeyword("_BLENDMODE_PRE_MULTIPLY");

                    material.SetInt("_RenderQueueType", 1);
                    material.SetInt("_SurfaceType", 0);
                    material.SetInt("_BlendMode", 0);
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt("_AlphaSrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_AlphaDstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt("_ZWrite", zwrite);
                    material.SetInt("_TransparentZWrite", zwrite);
                    material.SetInt("_ZTestDepthEqualForOpaque", 3);
                    material.SetInt("_ZTestGBuffer", 4);
                    material.SetInt("_ZTestTransparent", 0);
                }
                else
                {
                    material.SetOverrideTag("RenderType", "Transparent");
                    material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;

                    // Alpha Blend
                    if (blend == 0)
                    {
                        // Standard and Universal Render Pipeline
                        material.SetInt("__src", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                        material.SetInt("__dst", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                        material.SetInt("__premul", 0);

                        // HD Render Pipeline
                        material.EnableKeyword("_SURFACE_TYPE_TRANSPARENT");
                        material.EnableKeyword("_ENABLE_FOG_ON_TRANSPARENT");

                        material.EnableKeyword("_BLENDMODE_ALPHA");
                        material.DisableKeyword("_BLENDMODE_ADD");
                        material.DisableKeyword("_BLENDMODE_PRE_MULTIPLY");

                        material.SetInt("_RenderQueueType", 5);
                        material.SetInt("_SurfaceType", 1);
                        material.SetInt("_BlendMode", 0);
                        material.SetInt("_SrcBlend", 1);
                        material.SetInt("_DstBlend", 10);
                        material.SetInt("_AlphaSrcBlend", 1);
                        material.SetInt("_AlphaDstBlend", 10);
                        material.SetInt("_ZWrite", zwrite);
                        material.SetInt("_TransparentZWrite", zwrite);
                        material.SetInt("_ZTestDepthEqualForOpaque", 0);
                        material.SetInt("_ZTestGBuffer", 4);
                        // Set as lit material
                        material.SetInt("_ZTestTransparent", 0);
                        //material.SetInt("_ZTestTransparent", (int)UnityEngine.Rendering.CompareFunction.LessEqual);
                    }
                    // Premultiply Blend
                    else if (blend == 1)
                    {
                        // Standard and Universal Render Pipeline
                        material.SetInt("__src", (int)UnityEngine.Rendering.BlendMode.One);
                        material.SetInt("__dst", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                        material.SetInt("__premul", 1);

                        // HD Render Pipeline
                        material.EnableKeyword("_SURFACE_TYPE_TRANSPARENT");
                        material.EnableKeyword("_ENABLE_FOG_ON_TRANSPARENT");

                        material.DisableKeyword("_BLENDMODE_ALPHA");
                        material.DisableKeyword("_BLENDMODE_ADD");
                        material.EnableKeyword("_BLENDMODE_PRE_MULTIPLY");

                        material.SetInt("_RenderQueueType", 5);
                        material.SetInt("_SurfaceType", 1);
                        material.SetInt("_BlendMode", 4);
                        material.SetInt("_SrcBlend", 1);
                        material.SetInt("_DstBlend", 10);
                        material.SetInt("_AlphaSrcBlend", 1);
                        material.SetInt("_AlphaDstBlend", 10);
                        material.SetInt("_ZWrite", zwrite);
                        material.SetInt("_TransparentZWrite", zwrite);
                        material.SetInt("_ZTestDepthEqualForOpaque", 0);
                        material.SetInt("_ZTestGBuffer", 4);
                        // Set as lit material
                        material.SetInt("_ZTestTransparent", 0);
                        //material.SetInt("_ZTestTransparent", (int)UnityEngine.Rendering.CompareFunction.LessEqual);

                    }
                    // Deprecated
                    //else if (blend == 2)
                    //{
                    //    material.SetInt("__src", (int)UnityEngine.Rendering.BlendMode.One);
                    //    material.SetInt("__dst", (int)UnityEngine.Rendering.BlendMode.One);
                    //    material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    //    material.SetInt("__premul", 0);
                    //}
                    //else if (blend == 3)
                    //{
                    //    material.SetInt("__src", (int)UnityEngine.Rendering.BlendMode.DstColor);
                    //    material.SetInt("__dst", (int)UnityEngine.Rendering.BlendMode.Zero);
                    //    material.DisableKeyword("_ALPHAPREMULTIPLY");
                    //    material.SetInt("__premul", 0);
                    //}
                }
            }

            // Set Cull Mode
            if (material.HasProperty("__cull"))
            {
                int cull = material.GetInt("__cull");

                material.SetInt("_CullMode", cull);
                material.SetInt("_TransparentCullMode", cull);
                material.SetInt("_CullModeForward", cull);

                // Needed for HD Render Pipeline
                material.DisableKeyword("_DOUBLESIDED_ON");
            }

            // Set Clip Mode
            if (material.HasProperty("__clip"))
            {
                int clip = material.GetInt("__clip");
                float cutoff = material.GetFloat("_Cutoff");

                //material.SetFloat("_Cutoff", cutoff * clip);

                if (clip == 0)
                {
                    material.DisableKeyword("_ALPHATEST_ON");

                    // HD Render Pipeline
                    material.SetInt("_AlphaCutoffEnable", 0);
                }
                else
                {
                    material.EnableKeyword("_ALPHATEST_ON");

                    // Internal
                    material.SetFloat("__cliptreshold", cutoff);

                    // HD Render Pipeline
                    material.SetInt("_AlphaCutoffEnable", 1);
                }

                // HD Render Pipeline
                material.SetFloat("_AlphaCutoff", cutoff);
                material.SetFloat("_AlphaCutoffPostpass", cutoff);
                material.SetFloat("_AlphaCutoffPrepass", cutoff);
                material.SetFloat("_AlphaCutoffShadow", cutoff);
            }

            // Set Render Queue Offset
            if (material.HasProperty("__priority"))
            {
                int priority = material.GetInt("__priority");
                int queueOffsetRange = 0;

                if (material.HasProperty("_IsUniversalPipeline"))
                {
                    queueOffsetRange = 50;
                }

                // HD Render Pipeline
                material.SetInt("_TransparentSortPriority", priority);

                material.renderQueue = material.renderQueue + queueOffsetRange + priority;
            }

            // Set Normals Mode
            if (material.HasProperty("__normals"))
            {
                int normals = material.GetInt("__normals");

                // Standard, Universal, HD Render Pipeline
                // Flip 0
                if (normals == 0)
                {
                    material.SetVector("__normalsoptions", new Vector4(-1, -1, -1, 0));
                    material.SetVector("_DoubleSidedConstants", new Vector4(-1, -1, -1, 0));
                }
                // Mirror 1
                else if (normals == 1)
                {
                    material.SetVector("__normalsoptions", new Vector4(1, 1, -1, 0));
                    material.SetVector("_DoubleSidedConstants", new Vector4(1, 1, -1, 0));
                }
                // None 2
                else if (normals == 2)
                {
                    material.SetVector("__normalsoptions", new Vector4(1, 1, 1, 0));
                    material.SetVector("_DoubleSidedConstants", new Vector4(1, 1, 1, 0));
                }
            }
        }
    }
}
