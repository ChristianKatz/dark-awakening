﻿using UnityEditor;
using Boxophobic.Utils;

namespace TheVegetationEngine
{
    [InitializeOnLoad]
    class TVEListener : AssetPostprocessor
    {
        static string boxophobicFolder = "Assets/BOXOPHOBIC";

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            boxophobicFolder = BoxophobicUtils.GetBoxophobicFolder();

            SwitchDefineSymbols();
        }

        static void SwitchDefineSymbols()
        {
            var defineSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);

            // Cleanup
            defineSymbols = defineSymbols.Replace("THE_VEGETATION_ENGINE", "");
            defineSymbols = defineSymbols.Replace("IS_STANDARD_PIPELINE", "");
            defineSymbols = defineSymbols.Replace("IS_UNIVERSAL_PIPELINE", "");
            defineSymbols = defineSymbols.Replace("IS_HD_PIPELINE", "");

            defineSymbols = defineSymbols + ";THE_VEGETATION_ENGINE;";

            var define = "";
            var pipeline = SettingsUtils.LoadSettingsData(boxophobicFolder + "/User/The Vegetation Engine/Pipeline.asset", "");

            if (pipeline.Contains("Standard"))
            {
                define = "IS_STANDARD_PIPELINE";
            }
            else if (pipeline.Contains("Universal"))
            {
                define = "IS_UNIVERSAL_PIPELINE";
            }

            else if (pipeline.Contains("High"))
            {
                define = "IS_HD_PIPELINE";
            }
            else if (pipeline == "")
            {
                define = "IS_STANDARD_PIPELINE";
            }

            defineSymbols = defineSymbols + ";" + define + ";";

            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, defineSymbols);
        }
    }
}