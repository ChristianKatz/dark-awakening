﻿// Cristian Pop - https://boxophobic.com/

using UnityEngine;
using UnityEditor;
using Boxophobic.StyledGUI;
using Boxophobic.Utils;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

namespace TheVegetationEngine
{
    public class TVEHub : EditorWindow
    {
        string[] featureVegetationStudio = new string[]
        {
        "           //Vegetation Studio Support",
        "           #include \"XXX/The Vegetation Engine/Core/Library/VS_Indirect.cginc\"",
        "           #pragma instancing_options procedural:setup forwardadd",
        "           #pragma multi_compile GPU_FRUSTUM_ON __",
        };

        string[] featureVegetationStudioHD = new string[]
        {
        "           //Vegetation Studio Support",
        "           #include \"XXX/The Vegetation Engine/Core/Library/VS_IndirectHD.cginc\"",
        "           #pragma instancing_options procedural:setupVSPro forwardadd",
        "           #pragma multi_compile GPU_FRUSTUM_ON __",
        };

        string[] featureGPUInstancer = new string[]
        {
        "           //GPU Instancer Support",
        "           #include \"Assets/GPUInstancer/Shaders/Include/GPUInstancerInclude.cginc\"",
        "           #pragma instancing_options procedural:setupGPUI",
        "           #pragma multi_compile_instancing",
        };

        //        readonly string[] crossfadeOptions =
        //        {
        //        "Off",
        //        "On",
        //        };

        readonly string[] compatibilityOptions =
        {
        "Disabled",
        "GPU Instancer (Instanced Indirect)",
        "Vegetation Studio (Instanced Indirect)",
        };

        readonly string[] switchOptions =
        {
        "Off",
        "Switch to Simple Lit",
        "Switch to Lit",
        "Switch to Advanced Lit",
        };

        string boxophobicFolder = "Assets/BOXOPHOBIC";

        string[] packagePaths;
        string[] packageOptions;
        string[] shaderPaths;
        string[] materialPaths;

        string aciveScene = "";
        string packagesPath;
        string shadersPath;

        int unityMajorVersion;

        int packageIndex;
        int compatibilityIndex;
        int switchIndex;

        int userVersion;
        int internalVersion;
        string savedPackageName = "";

        bool isInstalled;

        GUIStyle stylePopup;

        Color bannerColor;
        string bannerText;
        string helpURL;
        static TVEHub window;
        Vector2 scrollPosition = Vector2.zero;

        [MenuItem("Window/BOXOPHOBIC/The Vegetation Engine/Hub")]
        public static void ShowWindow()
        {
            window = GetWindow<TVEHub>(false, "The Vegetation Engine Hub", true);
            window.minSize = new Vector2(389, 220);
        }

        void OnEnable()
        {
            unityMajorVersion = int.Parse(Application.unityVersion.Substring(0, 4));
            //unityMinorVersion = Application.unityVersion.Substring(5, 1);
            internalVersion = SettingsUtils.LoadSettingsData(boxophobicFolder + "/The Vegetation Engine/Core/Editor/TVEVersion.asset", -99);
            userVersion = SettingsUtils.LoadSettingsData(boxophobicFolder + "/User/The Vegetation Engine/Version.asset", -99);

            string bannerVersion = internalVersion.ToString();
            bannerVersion = bannerVersion.Insert(1, ".");
            bannerVersion = bannerVersion.Insert(3, ".");

            // Check for latest version
            //StartBackgroundTask(StartRequest("https://boxophobic.com/s/thevegetationengine", () =>
            //{
            //    int.TryParse(www.downloadHandler.text, out latestVersion);
            //    Debug.Log("hubhub" + latestVersion);
            //}));

            bannerColor = new Color(0.890f, 0.745f, 0.309f);
            bannerText = "The Vegetation Engine " + bannerVersion;
            helpURL = "https://docs.google.com/document/d/145JOVlJ1tE-WODW45YoJ6Ixg23mFc56EnB_8Tbwloz8/edit#heading=h.pr0qp2u684tr";

            boxophobicFolder = BoxophobicUtils.GetBoxophobicFolder();

            packagesPath = boxophobicFolder + "/The Vegetation Engine/Core/Packages";
            shadersPath = boxophobicFolder + "/The Vegetation Engine/Core/Shaders";

            // Add corect Boxophobic Folder if the folder is moved
            featureVegetationStudio[1] = featureVegetationStudio[1].Replace("XXX", boxophobicFolder);
            featureVegetationStudioHD[1] = featureVegetationStudioHD[1].Replace("XXX", boxophobicFolder);

            GetPackages();
            GetShaders();
            GetMaterials();

            if (File.Exists(boxophobicFolder + "/The Vegetation Engine/Core/Editor/TVEHubAutorun.cs"))
            {
                isInstalled = false;
            }
            else
            {
                isInstalled = true;
                GetLastSettings();
            }
        }

        void OnGUI()
        {
            SetGUIStyles();

            StyledGUI.DrawWindowBanner(bannerColor, bannerText, helpURL);

            GUILayout.BeginHorizontal();
            GUILayout.Space(15);

            GUILayout.BeginVertical();

            scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, false, GUILayout.Width(this.position.width - 28), GUILayout.Height(this.position.height - 80));

            if (isInstalled == false)
            {
                DrawInstallMessage();
            }
            else
            {
                DrawDefaultMessage();
            }

            //int height = 140;

            if (packageOptions[packageIndex].Contains("Universal"))
            {
                DrawUniversalMessage();
                //height += 60;
            }

            if (packageOptions[packageIndex].Contains("High"))
            {
                DrawHDMessage();
                DrawHDDiffusonMessage();
                //height += 120;
            }

            if (compatibilityIndex > 0)
            {
                DrawCompatibilityMessage();
                //height += 60;
            }

            if (switchIndex > 0)
            {
                DrawSwitchMessage();
                //height += 60;
            }

            if (switchIndex > 0)
            {
                DrawSwitchMessage();
                //height += 60;
            }

            //scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, false, GUILayout.Width(this.position.width - 28), GUILayout.Height(this.position.height - height));

            if (isInstalled == false)
            {
                GUI.enabled = false;
            }

            DrawRenderPipelineSelection();
            DrawShaderCompatibility();
            //DrawShaderSwicth();

            GUI.enabled = true;

            if (isInstalled == false)
            {
                DrawInstallButton();
            }
            else
            {
                DrawSetupButtons();
            }

            GUILayout.EndScrollView();

            GUILayout.EndVertical();

            GUILayout.Space(13);
            GUILayout.EndHorizontal();
        }

        void SetGUIStyles()
        {
            stylePopup = new GUIStyle(EditorStyles.popup)
            {
                alignment = TextAnchor.MiddleCenter
            };
        }

        void DrawInstallMessage()
        {
            EditorGUILayout.HelpBox("The Vegetation Engine need to be set up to used the current version! The current Scene will be saved before updating!", MessageType.Info, true);
        }

        void DrawDefaultMessage()
        {
            EditorGUILayout.HelpBox("Click the Setup Pipeline and then the Setup Shaders buttons to switch to another Render Pipeline. Click the Setup Shaders button if you only want to add or remove Shader Compatibility support. The current Scene will be saved before importing the new package!", MessageType.Info, true);
        }

        void DrawUniversalMessage()
        {
            EditorGUILayout.HelpBox("Universal Render Pipeline is in Preview. Some features might not work as expected! Due to different rendering architecture, the shading could look diferent and adjustments might be needed!", MessageType.Warning, true);
        }

        void DrawHDMessage()
        {
            EditorGUILayout.HelpBox("High Definition Render Pipeline is in Preview. Some features might not work as expected! Due to different rendering architecture, the shading could look diferent and adjustments might be needed!", MessageType.Warning, true);
        }

        void DrawHDDiffusonMessage()
        {
            EditorGUILayout.HelpBox("The Diffusion Profile will be missing for all Leaf Advanced Lit materials existing in the project! Diffusion Profiles are not directly supported. Please refer to the documentation for more information.", MessageType.Warning, true);
        }

        void DrawCompatibilityMessage()
        {
            EditorGUILayout.HelpBox("Please note that Vegetation Studio and GPU Instancer Instanced Indirect feature might not work with all Render Pipelines! Check the product documentation for more details.", MessageType.Warning, true);
        }

        void DrawSwitchMessage()
        {
            EditorGUILayout.HelpBox("The Shader Switch option will swicth all project materials to the selected lighting model and it is not reversible! Properties like Subsurface will look different with diferent lighting models and manualy adjustment is required!", MessageType.Warning, true);
        }

        void DrawRenderPipelineSelection()
        {
            GUILayout.Space(10);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Render Pipeline", ""));
            packageIndex = EditorGUILayout.Popup(packageIndex, packageOptions, stylePopup);
            GUILayout.EndHorizontal();
        }

        //void DrawShaderLODCrossFade()
        //{
        //    GUILayout.Space(10);

        //    GUILayout.BeginHorizontal();
        //    EditorGUILayout.LabelField(new GUIContent("Shader LOD Fade", ""));
        //    crossfadeIndex = EditorGUILayout.Popup(crossfadeIndex, crossfadeOptions, stylePopup);
        //    GUILayout.EndHorizontal();
        //}

        void DrawShaderCompatibility()
        {
            //GUILayout.Space(10);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Shader Compatibility", ""));

            compatibilityIndex = EditorGUILayout.Popup(compatibilityIndex, compatibilityOptions, stylePopup);

            GUILayout.EndHorizontal();
        }

        void DrawShaderSwicth()
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Shader Switch", ""));

            switchIndex = EditorGUILayout.Popup(switchIndex, switchOptions, stylePopup);

            GUILayout.EndHorizontal();
        }

        void DrawInstallButton()
        {
            GUILayout.Space(20);

            GUILayout.BeginHorizontal();
            //GUILayout.Label("");

            if (GUILayout.Button("Install The Vegetation Engine"))
            {
                SettingsUtils.SaveSettingsData(boxophobicFolder + "/User/The Vegetation Engine/Version.asset", internalVersion);

                SaveScene();
                ReopenScene();

                if (userVersion < 100)
                {
                    UpdateTo099();
                    UpdateTo100();
                }

                if (userVersion < 110)
                {
                    UpdateTo110();
                }

                if (unityMajorVersion >= 2019)
                {
                    UpdateShaders();
                }

                FileUtil.DeleteFileOrDirectory(boxophobicFolder + "/The Vegetation Engine/Core/Editor/TVEHubAutorun.cs");
                FileUtil.DeleteFileOrDirectory(boxophobicFolder + "/User/The Vegetation Engine/Pipeline.asset");
                FileUtil.DeleteFileOrDirectory(boxophobicFolder + "/User/The Vegetation Engine/Compatibility.asset");
                AssetDatabase.Refresh();

                Debug.Log("[The Vegetation Engine] " + "The Vegetation Engine is installed for the Standard Pipeline.");

                isInstalled = true;

                GUIUtility.ExitGUI();
            }

            //GUILayout.Label("");
            GUILayout.EndHorizontal();
        }

        void DrawSetupButtons()
        {
            GUILayout.Space(20);

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Setup Pipeline"/*, GUILayout.Width(160)*/))
            {
                SettingsUtils.SaveSettingsData(boxophobicFolder + "/User/The Vegetation Engine/Pipeline.asset", packageOptions[packageIndex]);

                ImportPackage();

                GUIUtility.ExitGUI();
            }

            if (GUILayout.Button("Setup Shaders"/*, GUILayout.Width(160)*/))
            {
                SettingsUtils.SaveSettingsData(boxophobicFolder + "/User/The Vegetation Engine/Compatibility.asset", compatibilityIndex);

                SaveScene();
                ReopenScene();

                if (unityMajorVersion >= 2019 || compatibilityIndex > 0)
                {
                    UpdateShaders();
                }

                if (switchIndex > 0)
                {
                    SetMaterialsSwitchShaders();
                }

                SetMaterialsRenderSettings();

                FileUtil.DeleteFileOrDirectory(boxophobicFolder + "/The Vegetation Engine/Core/Editor/TVEHubAutorun.cs");
                AssetDatabase.Refresh();

                GUIUtility.ExitGUI();
            }

            GUILayout.EndHorizontal();
        }

        void GetPackages()
        {
            packagePaths = Directory.GetFiles(packagesPath, "*.unitypackage", SearchOption.TopDirectoryOnly);

            packageOptions = new string[packagePaths.Length];

            for (int i = 0; i < packageOptions.Length; i++)
            {
                packageOptions[i] = Path.GetFileNameWithoutExtension(packagePaths[i].Replace("Built-in Pipeline", "Standard"));
            }
        }

        void GetShaders()
        {
            shaderPaths = Directory.GetFiles(shadersPath, "*.shader", SearchOption.AllDirectories);
        }

        void GetMaterials()
        {
            materialPaths = Directory.GetFiles("Assets/", "*.mat", SearchOption.AllDirectories);
        }

        void GetLastSettings()
        {
            compatibilityIndex = SettingsUtils.LoadSettingsData(boxophobicFolder + "/User/The Vegetation Engine/Compatibility.asset", 0);

            savedPackageName = SettingsUtils.LoadSettingsData(boxophobicFolder + "/User/The Vegetation Engine/Pipeline.asset", "");

            for (int i = 0; i < packageOptions.Length; i++)
            {
                if (packageOptions[i] == savedPackageName)
                {
                    packageIndex = i;
                }
            }
        }

        void SaveScene()
        {
            if (SceneManager.GetActiveScene() != null || SceneManager.GetActiveScene().name != "")
            {
                if (SceneManager.GetActiveScene().isDirty)
                {
                    EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                }

                aciveScene = SceneManager.GetActiveScene().path;
                EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);
            }
        }

        void ReopenScene()
        {
            if (aciveScene != "")
            {
                EditorSceneManager.OpenScene(aciveScene);
            }
        }

        void ImportPackage()
        {
            AssetDatabase.ImportPackage(packagePaths[packageIndex], false);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            Debug.Log("[The Vegetation Engine] " + packageOptions[packageIndex] + " package imported into your project!");
        }

        void UpdateShaders()
        {
            for (int i = 0; i < shaderPaths.Length; i++)
            {
                if (shaderPaths[i].Contains("Elements") == false && shaderPaths[i].Contains("Helpers") == false)
                {
                    // Auto generated GPUI Shaders need to be removed to avoid issues
                    if (shaderPaths[i].Contains("GPUI"))
                    {
                        FileUtil.DeleteFileOrDirectory(shaderPaths[i]);
                        AssetDatabase.Refresh();
                    }
                    else
                    {
                        InjectShaderFeature(shaderPaths[i]);
                    }
                }
            }

            if (unityMajorVersion >= 2019)
            {
                Debug.Log("[The Vegetation Engine] " + "Local keywords support added to The Vegetation Engine Shaders.");
            }

            if (compatibilityIndex > 0)
            {
                Debug.Log("[The Vegetation Engine] " + compatibilityOptions[compatibilityIndex] + " support added to The Vegetation Engine Shaders.");
            }

            //AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        void InjectShaderFeature(string shaderAssetPath)
        {
            StreamReader reader = new StreamReader(shaderAssetPath);

            List<string> lines = new List<string>();

            while (!reader.EndOfStream)
            {
                lines.Add(reader.ReadLine());
            }

            reader.Close();

            // Delete Features before adding new ones
            int count = lines.Count;

            for (int i = 0; i < count; i++)
            {
                if (lines[i].Contains("FEATURES BEGIN"))
                {
                    int c = 0;
                    int j = i + 1;

                    while (lines[j].Contains("FEATURES END") == false)
                    {
                        j++;
                        c++;
                    }

                    lines.RemoveRange(i + 1, c);
                    count = count - c;
                }
            }

        
            var pipeline = SettingsUtils.LoadSettingsData(boxophobicFolder + "/User/The Vegetation Engine/Pipeline.asset", "Standard");

            // Delete GPUI added lines    
            count = lines.Count;

            if (pipeline.Contains("Standard"))
            {
                for (int i = 0; i < count; i++)
                {
                    if (lines[i].StartsWith("#"))
                    {
                        lines.RemoveRange(i + 1, 4);
                        count = count - 4;

                        i--;
                    }

                    if (lines[i].Contains("#pragma target 3.0"))
                    {
                        if (lines[i + 1].Contains("multi_compile_instancing") == false)
                        {
                            lines.Insert(i + 1, "		#pragma multi_compile_instancing");
                        }
                    }
                }
            }
            else if (pipeline.Contains("Universal"))
            {
                for (int i = 0; i < count; i++)
                {
                    if (lines[i].StartsWith("#"))
                    {
                        lines.RemoveRange(i, 3);
                        count = count - 3;

                        i--;
                    }

                    if (lines[i].Contains("HLSLPROGRAM"))
                    {
                        if (lines[i + 1].Contains("multi_compile_instancing") == false)
                        {
                            lines.Insert(i + 1, "		    #pragma multi_compile_instancing");
                        }
                    }
                }
            }
            else if (pipeline.Contains("High"))
            {
                for (int i = 0; i < count; i++)
                {
                    if (lines[i].StartsWith("#"))
                    {
                        lines.RemoveRange(i, 3);
                        count = count - 3;

                        i--;
                    }

                    if (lines[i].Contains("HLSLINCLUDE"))
                    {
                        if (lines[i + 3].Contains("multi_compile_instancing") == false)
                        {
                            lines.Insert(i + 3, "		    #pragma multi_compile_instancing");
                        }
                    }
                }
            }

            for (int i = 0; i < count; i++)
            {
                if (lines[i].Contains("GPUInstancerInclude.cginc"))
                {
                    if (pipeline.Contains("Standard"))
                    {
                        Debug.Log("here");
                        lines.RemoveAt(i - 1);
                        lines.RemoveAt(i);
                        lines.RemoveAt(i + 1);
                    }

                    count = count - 3;
                }
            }

            //Inject 3rd Party Support
            if (compatibilityIndex == 1)
            {
                for (int i = 0; i < lines.Count; i++)
                {
                    if (lines[i].Contains("FEATURES BEGIN"))
                    {
                        lines.InsertRange(i + 1, featureGPUInstancer);
                    }
                }
            }

            else if (compatibilityIndex == 2)
            {
                for (int i = 0; i < lines.Count; i++)
                {
                    if (lines[i].Contains("FEATURES BEGIN"))
                    {
                        if (pipeline.Contains("High"))
                        {
                            lines.InsertRange(i + 1, featureVegetationStudioHD);
                        }
                        else
                        {
                            lines.InsertRange(i + 1, featureVegetationStudio);
                        }
                    }
                }
            }

            // Set Keywords to local
            if (unityMajorVersion >= 2019)
            {
                for (int i = 0; i < lines.Count; i++)
                {
                    //if (lines[i].Contains("multi_compile "))
                    //{
                    //    // Skip Instancing pragma and global keywords
                    //    if (lines[i].Contains("multi_compile TVE") == false)
                    //    {
                    //        lines[i] = lines[i].Replace("multi_compile ", "multi_compile_local ");
                    //    }
                    //}
                    //else if (lines[i].Contains("shader_feature "))
                    //{
                    //    lines[i] = lines[i].Replace("shader_feature ", "shader_feature_local ");
                    //}

                    // Disable Universal define set by ASE
                    if (lines[i].Contains("_ALPHATEST_ON 1"))
                    {
                        lines[i] = lines[i].Replace("_ALPHATEST_ON 1", "TVE_DISABLE_ALPHATEST_ON 1");
                    }

                    // Set keyword to local
                    if (lines[i].Contains("shader_feature _ALPHATEST_ON"))
                    {
                        lines[i] = lines[i].Replace("shader_feature _ALPHATEST_ON", "shader_feature_local _ALPHATEST_ON");
                    }

                    // Set keyword to local
                    if (lines[i].Contains("shader_feature _DETAIL_OFF"))
                    {
                        lines[i] = lines[i].Replace("shader_feature _DETAIL_OFF", "shader_feature_local _DETAIL_OFF");
                    }

                    // Hide _SpecGloss Color for Standard RP
                    if (lines[i].Contains("_SpecColor"))
                    {
                        lines[i] = lines[i].Replace("_SpecColor", "[HideInInspector] _SpecColor");
                    }
                }
            }

            StreamWriter writer = new StreamWriter(shaderAssetPath);

            for (int i = 0; i < lines.Count; i++)
            {
                writer.WriteLine(lines[i]);
            }

            writer.Close();

            lines = new List<string>();

            AssetDatabase.ImportAsset(shaderAssetPath);
        }

        void SetMaterialsRenderSettings()
        {
            for (int i = 0; i < materialPaths.Length; i++)
            {
                var material = AssetDatabase.LoadAssetAtPath<Material>(materialPaths[i]);
                var shaderName = material.shader.name;

                if (shaderName.Contains("The Vegetation Engine"))
                {
                    TVEShaderUtils.SetRenderSettings(material);
                }
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            Debug.Log("[The Vegetation Engine] " + "Material render settings updated!");
        }

        void SetMaterialsSwitchShaders()
        {
            string shaderSwitch = "Simple Lit";

            if (switchIndex == 2)
            {
                shaderSwitch = "Lit";
            }
            if (switchIndex == 3)
            {
                shaderSwitch = "Advanced Lit";
            }

            for (int i = 0; i < materialPaths.Length; i++)
            {
                if (materialPaths[i].Contains("TVE Material"))
                {
                    var material = AssetDatabase.LoadAssetAtPath<Material>(materialPaths[i]);
                    var shaderName = material.shader.name;

                    if (shaderName.Contains("Simple Lit"))
                    {
                        shaderName = shaderName.Replace("Simple Lit", "XXX");
                    }
                    else if (shaderName.Contains("Advanced Lit"))
                    {
                        shaderName = shaderName.Replace("Advanced Lit", "XXX");
                    }
                    else
                    {
                        shaderName = shaderName.Replace("Lit", "XXX");
                    }

                    material.shader = Shader.Find(shaderName.Replace("XXX", shaderSwitch));
                }
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            Debug.Log("[The Vegetation Engine] " + shaderSwitch + " shaders assigned to The Vegetation Engine Materials.");
        }

        void UpdateTo099()
        {
            var count = 0;

            for (int i = 0; i < materialPaths.Length; i++)
            {
                if (materialPaths[i].Contains("Element - "))
                {
                    var material = AssetDatabase.LoadAssetAtPath<Material>(materialPaths[i]);
                    var shaderName = material.shader.name;

                    if (material.GetFloat("_Intensity") >= 0)
                    {
                        if (shaderName.Contains("Color"))
                        {
                            material.SetColor("_MainColor", material.GetColor("_Simple"));
                            material.SetColor("_WinterColor", material.GetColor("_Winter"));
                            material.SetColor("_SpringColor", material.GetColor("_Spring"));
                            material.SetColor("_SummerColor", material.GetColor("_Summer"));
                            material.SetColor("_AutumnColor", material.GetColor("_Autumn"));
                        }
                        else if (shaderName.Contains("Color HDR"))
                        {
                            material.SetColor("_MainColorHDR", material.GetColor("_Simple"));
                        }
                        else if (shaderName.Contains("Leaves") || shaderName.Contains("Overlay") || shaderName.Contains("Size"))
                        {
                            material.SetFloat("_MainValue", material.GetFloat("_Simple"));
                            material.SetFloat("_WinterValue", material.GetFloat("_Winter"));
                            material.SetFloat("_SpringValue", material.GetFloat("_Spring"));
                            material.SetFloat("_SummerValue", material.GetFloat("_Summer"));
                            material.SetFloat("_AutumnValue", material.GetFloat("_Autumn"));
                        }
                        else if (shaderName.Contains("Wetness"))
                        {
                            material.SetFloat("_MainValue", material.GetFloat("_Simple"));
                        }

                        if (material.HasProperty("_SeasonMode"))
                        {
                            material.SetInt("_ElementMode", material.GetInt("_SeasonMode"));
                        }

                        material.SetFloat("_ElementIntensity", material.GetFloat("_Intensity"));

                        // Set Old _Intensity to -1 to avoid updating multiple times
                        material.SetFloat("_Intensity", -1);

                        count++;
                    }
                }
            }

            if (count > 0)
            {
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                Debug.Log("[The Vegetation Engine] " + "Element materials updated!");
            }
        }

        void UpdateTo100()
        {
            for (int i = 0; i < materialPaths.Length; i++)
            {
                var material = AssetDatabase.LoadAssetAtPath<Material>(materialPaths[i]);

                if (material.shader.name.Contains("The Vegetation Engine"))
                {
                    material.SetInt("_IsVersion", 100);

                    material.SetInt("_IsTVEShader", 1);
                    material.SetInt("_IsTVEElement", 1);

                    material.SetInt("_IsStandardPipeline", 1);
                    material.SetInt("_IsUniversalPipeline", 1);
                    material.SetInt("_IsHDPipeline", 1);

                    material.SetInt("_IsLitShader", 1);

                    material.SetInt("_IsStandardShader", 1);
                    material.SetInt("_IsSubsurfaceShader", 1);
                    material.SetInt("_IsDisplacementShader", 1);
                    material.SetInt("_IsTessellationShader", 1);

                    material.SetInt("_IsBillboardShader", 1);
                    material.SetInt("_IsVegetationShader", 1);
                }
            }
        }

        void UpdateTo110()
        {
            for (int i = 0; i < materialPaths.Length; i++)
            {
                var material = AssetDatabase.LoadAssetAtPath<Material>(materialPaths[i]);

                if (material.shader.name.Contains("The Vegetation Engine"))
                {
                    material.SetInt("_IsVersion", 110);

                    if (material.HasProperty("_DetailMaskValue"))
                    {
                        material.SetFloat("_DetailMeshValue", material.GetFloat("_DetailMaskValue"));
                        material.SetFloat("_DetailMaskValue", material.GetFloat("_MainMaskValue"));
                    }

                    if (material.HasProperty("_VertexOcclusion"))
                    {
                        material.SetFloat("_ObjectOcclusionValue", material.GetFloat("_VertexOcclusion"));
                    }

                    if (material.HasProperty("_SubsurfaceMode"))
                    {
                        if (material.GetInt("_SubsurfaceMode") == 0)
                        {
                            material.SetFloat("_SubsurfaceMinValue", 0);
                            material.SetFloat("_SubsurfaceMaxValue", 1);
                        }
                        else
                        {
                            material.SetFloat("_SubsurfaceMinValue", 1);
                            material.SetFloat("_SubsurfaceMaxValue", 0);
                        }
                    }

                    if (material.HasProperty("_ObjectThicknessValue"))
                    {
                        material.SetFloat("_SubsurfaceValue", 1 - material.GetFloat("_ObjectThicknessValue"));
                    }
                }
            }
        }

        // Check for latest version
        //UnityWebRequest www;

        //IEnumerator StartRequest(string url, Action success = null)
        //{
        //    using (www = UnityWebRequest.Get(url))
        //    {
        //        yield return www.Send();

        //        while (www.isDone == false)
        //            yield return null;

        //        if (success != null)
        //            success();
        //    }
        //}

        //public static void StartBackgroundTask(IEnumerator update, Action end = null)
        //{
        //    EditorApplication.CallbackFunction closureCallback = null;

        //    closureCallback = () =>
        //    {
        //        try
        //        {
        //            if (update.MoveNext() == false)
        //            {
        //                if (end != null)
        //                    end();
        //                EditorApplication.update -= closureCallback;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            if (end != null)
        //                end();
        //            Debug.LogException(ex);
        //            EditorApplication.update -= closureCallback;
        //        }
        //    };

        //    EditorApplication.update += closureCallback;
        //}
    }
}
