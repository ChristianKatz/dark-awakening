﻿// Cristian Pop - https://boxophobic.com/

namespace TheVegetationEngine
{
    [System.Serializable]
    public class TVEMaterialData
    {
        public string prop;
        public float val;
        public string name;
        public float min;
        public float max;
        public bool snap;
        public bool space;

        public TVEMaterialData(string prop, float val, string name, int min, int max, bool snap, bool space)
        {
            this.prop = prop;
            this.val = val;
            this.name = name;
            this.min = min;
            this.max = max;
            this.snap = snap;
            this.space = space;
        }
    }
}