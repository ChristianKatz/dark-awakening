// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BOXOPHOBIC/The Vegetation Engine/Elements/Overlay Element"
{
	Properties
	{
		[StyledBanner(Overaly Element)]_Banner1("Banner", Float) = 0
		[HideInInspector]_IsVersion("_IsVersion", Float) = 110
		[HideInInspector]_IsStandardPipeline("_IsStandardPipeline", Float) = 1
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector]_IsTVEElement("_IsTVEElement", Float) = 1
		_ElementIntensity("Intensity", Range( 0 , 1)) = 1
		[StyledCategory(Element)]_ElementCat("[ Element Cat ]", Float) = 0
		[NoScaleOffset]_MainTex("Texture", 2D) = "white" {}
		_MainUVs("Texture UVs", Vector) = (1,1,0,0)
		[Enum(Main,0,Seasons,1)]_ElementMode("Element Mode", Float) = 0
		[StyledInteractive(_ElementMode, 0)]_SeasonMode_Simple("# SeasonMode_Simple", Float) = 0
		[Space(10)]_MainValue("Main", Range( 0 , 1)) = 1
		[StyledInteractive(_ElementMode, 1)]_SeasonMode_Seasons("# SeasonMode_Seasons", Float) = 0
		[Space(10)]_WinterValue("Winter", Range( 0 , 1)) = 1
		_SpringValue("Spring", Range( 0 , 1)) = 1
		_SummerValue("Summer", Range( 0 , 1)) = 1
		_AutumnValue("Autumn", Range( 0 , 1)) = 1
		[StyledCategory(Advanced)]_AdvancedCat("[ Advanced Cat]", Float) = 1
		[IntRange]__priority("Priority", Range( -50 , 50)) = 0
		[HideInInspector]_Intensity("Intensity", Range( 0 , 1)) = 1
		[HideInInspector][Enum(Simple,0,Seasons,1)]_SeasonMode("Size Mode", Float) = 0
		[HideInInspector][Space(10)]_Simple("Simple Amount", Float) = 1
		[HideInInspector][Space(10)]_Winter("Winter Amount", Float) = 1
		[HideInInspector]_Summer("Summer Amount", Float) = 1
		[HideInInspector]_Spring("Spring Amount", Float) = 1
		[HideInInspector]_Autumn("Autumn Amount", Float) = 1

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Transparent" "Queue"="Transparent" "PreviewType"="Plane" }
	LOD 0

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Back
		ColorMask B
		ZWrite Off
		ZTest LEqual
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
#endif
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_color : COLOR;
			};

			uniform half _Intensity;
			uniform half _SeasonMode;
			uniform half _IsVersion;
			uniform half __priority;
			uniform half _IsTVEElement;
			uniform half _ElementCat;
			uniform half _AdvancedCat;
			uniform half _SeasonMode_Seasons;
			uniform half _SeasonMode_Simple;
			uniform half _IsTVEShader;
			uniform half _Winter;
			uniform half _Simple;
			uniform half _Autumn;
			uniform half _Summer;
			uniform half _Spring;
			uniform half _Banner1;
			uniform half _IsStandardPipeline;
			uniform half _MainValue;
			uniform sampler2D _MainTex;
			uniform half4 _MainUVs;
			uniform half4 TVE_SeasonOptions;
			uniform half _WinterValue;
			uniform half _SpringValue;
			uniform half TVE_SeasonLerp;
			uniform half _SummerValue;
			uniform half _AutumnValue;
			uniform half _ElementMode;
			uniform half _ElementIntensity;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				half3 temp_cast_0 = (( _IsStandardPipeline * 0.0 )).xxx;
				
				o.ase_texcoord1.xy = v.ase_texcoord.xy;
				o.ase_color = v.color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.zw = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = temp_cast_0;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);

#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
#endif
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
#endif
				half Value_Main157_g36 = _MainValue;
				half4 tex2DNode17_g36 = tex2D( _MainTex, ( ( i.ase_texcoord1.xy * (_MainUVs).xy ) + (_MainUVs).zw ) );
				half MainTex_R73_g36 = tex2DNode17_g36.r;
				half TVE_SeasonOptions_X50_g36 = TVE_SeasonOptions.x;
				half Value_Winter158_g36 = _WinterValue;
				half Value_Spring159_g36 = _SpringValue;
				half TVE_SeasonLerp54_g36 = TVE_SeasonLerp;
				half lerpResult168_g36 = lerp( Value_Winter158_g36 , Value_Spring159_g36 , TVE_SeasonLerp54_g36);
				half TVE_SeasonOptions_Y51_g36 = TVE_SeasonOptions.y;
				half Value_Summer160_g36 = _SummerValue;
				half lerpResult167_g36 = lerp( Value_Spring159_g36 , Value_Summer160_g36 , TVE_SeasonLerp54_g36);
				half TVE_SeasonOptions_Z52_g36 = TVE_SeasonOptions.z;
				half Value_Autumn161_g36 = _AutumnValue;
				half lerpResult166_g36 = lerp( Value_Summer160_g36 , Value_Autumn161_g36 , TVE_SeasonLerp54_g36);
				half TVE_SeasonOptions_W53_g36 = TVE_SeasonOptions.w;
				half lerpResult165_g36 = lerp( Value_Autumn161_g36 , Value_Winter158_g36 , TVE_SeasonLerp54_g36);
				half Element_Mode55_g36 = _ElementMode;
				half lerpResult181_g36 = lerp( ( Value_Main157_g36 * MainTex_R73_g36 ) , ( MainTex_R73_g36 * ( ( TVE_SeasonOptions_X50_g36 * lerpResult168_g36 ) + ( TVE_SeasonOptions_Y51_g36 * lerpResult167_g36 ) + ( TVE_SeasonOptions_Z52_g36 * lerpResult166_g36 ) + ( TVE_SeasonOptions_W53_g36 * lerpResult165_g36 ) ) ) , Element_Mode55_g36);
				half Base_Extras213_g36 = lerpResult181_g36;
				half Element_Intensity56_g36 = ( _ElementIntensity * i.ase_color.a );
				half lerpResult233_g36 = lerp( 1.0 , Base_Extras213_g36 , Element_Intensity56_g36);
				half4 appendResult236_g36 = (half4(0.0 , 0.0 , lerpResult233_g36 , MainTex_R73_g36));
				half4 Final_Overlay_RGBA237_g36 = appendResult236_g36;
				
				
				finalColor = Final_Overlay_RGBA237_g36;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "TVEShaderElementGUI"
	
	
}
/*ASEBEGIN
Version=17802
1927;7;1906;1014;1151.42;1723.979;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;106;0,-1664;Inherit;False;Property;_Summer;Summer Amount;35;1;[HideInInspector];Create;False;0;0;True;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;105;208,-1664;Inherit;False;Property;_Autumn;Autumn Amount;37;1;[HideInInspector];Create;False;0;0;True;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;108;-640,-1280;Half;False;Property;_Banner1;Banner;0;0;Create;True;0;0;True;1;StyledBanner(Overaly Element);0;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;103;-192,-1664;Inherit;False;Property;_Spring;Spring Amount;36;1;[HideInInspector];Create;False;0;0;True;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;104;-640,-1664;Inherit;False;Property;_Simple;Simple Amount;33;1;[HideInInspector];Create;False;0;0;True;1;Space(10);1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;100;-640,-1536;Inherit;False;Property;_SeasonMode;Size Mode;32;2;[HideInInspector];[Enum];Create;False;2;Simple;0;Seasons;1;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;102;-480,-1536;Inherit;False;Property;_Intensity;Intensity;31;1;[HideInInspector];Create;False;0;0;True;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;101;-384,-1664;Inherit;False;Property;_Winter;Winter Amount;34;1;[HideInInspector];Create;False;0;0;True;1;Space(10);1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;109;-640,-1152;Inherit;False;Base Element;1;;36;0e972c73cae2ee54ea51acc9738801d0;3,145,4,148,4,146,4;0;4;FLOAT4;0;FLOAT3;85;FLOAT;86;FLOAT;339
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;-256,-1152;Half;False;True;-1;2;TVEShaderElementGUI;0;1;BOXOPHOBIC/The Vegetation Engine/Elements/Overlay Element;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;True;False;True;0;False;-1;True;False;False;True;False;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;0;False;-1;True;False;0;False;-1;0;False;-1;True;3;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;PreviewType=Plane;True;2;0;False;False;False;False;False;False;False;False;False;True;1;LightMode=ForwardBase;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;0
Node;AmplifyShaderEditor.CommentaryNode;94;-640,-1792;Inherit;False;1025.136;100;Internal;0;;1,0.252,0,1;0;0
WireConnection;0;0;109;0
WireConnection;0;1;109;339
ASEEND*/
//CHKSM=5089BF0A92DAF86F9895C77DE43A1298EA7F254E