// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BOXOPHOBIC/The Vegetation Engine/Elements/Colors HDR Element"
{
	Properties
	{
		[StyledBanner(Color HDR Element)]_Banner1("Banner", Float) = 0
		[HideInInspector]_IsVersion("_IsVersion", Float) = 110
		[HideInInspector]_IsStandardPipeline("_IsStandardPipeline", Float) = 1
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector]_IsTVEElement("_IsTVEElement", Float) = 1
		_ElementIntensity("Intensity", Range( 0 , 1)) = 1
		[StyledCategory(Element)]_ElementCat("[ Element Cat ]", Float) = 0
		[NoScaleOffset]_MainTex("Texture", 2D) = "white" {}
		_MainUVs("Texture UVs", Vector) = (1,1,0,0)
		[StyledInteractive(_ElementMode, 0)]_SeasonMode_Simple("# SeasonMode_Simple", Float) = 0
		[HDR]_MainColorHDR("Color HDR", Color) = (1,0.5,0,1)
		[StyledInteractive(_ElementMode, 1)]_SeasonMode_Seasons("# SeasonMode_Seasons", Float) = 0
		[StyledCategory(Advanced)]_AdvancedCat("[ Advanced Cat]", Float) = 1
		[IntRange]__priority("Priority", Range( -50 , 50)) = 0
		[HideInInspector]_Intensity("Intensity", Range( 0 , 1)) = 1
		[HideInInspector][HDR]_Simple("Color", Color) = (1,0.5,0,1)

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Transparent" "Queue"="Transparent" "PreviewType"="Plane" }
	LOD 0

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Back
		ColorMask RGB
		ZWrite Off
		ZTest LEqual
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
#endif
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_color : COLOR;
			};

			uniform half4 _Simple;
			uniform half _IsVersion;
			uniform half __priority;
			uniform half _IsTVEElement;
			uniform half _ElementCat;
			uniform half _AdvancedCat;
			uniform half _SeasonMode_Seasons;
			uniform half _SeasonMode_Simple;
			uniform half _IsTVEShader;
			uniform half _Intensity;
			uniform half _Banner1;
			uniform half _IsStandardPipeline;
			uniform half4 _MainColorHDR;
			uniform sampler2D _MainTex;
			uniform half4 _MainUVs;
			uniform half _ElementIntensity;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				half3 temp_cast_0 = (( _IsStandardPipeline * 0.0 )).xxx;
				
				o.ase_texcoord1.xy = v.ase_texcoord.xy;
				o.ase_color = v.color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.zw = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = temp_cast_0;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);

#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
#endif
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
#endif
				half4 tex2DNode17_g36 = tex2D( _MainTex, ( ( i.ase_texcoord1.xy * (_MainUVs).xy ) + (_MainUVs).zw ) );
				half3 MainTex_RGB19_g36 = (tex2DNode17_g36).rgb;
				half3 temp_output_89_0_g36 = ( (_MainColorHDR).rgb * MainTex_RGB19_g36 );
				half MainTex_A74_g36 = tex2DNode17_g36.a;
				half Element_Intensity56_g36 = ( _ElementIntensity * i.ase_color.a );
				half temp_output_104_0_g36 = ( MainTex_A74_g36 * Element_Intensity56_g36 );
				half4 appendResult108_g36 = (half4(temp_output_89_0_g36 , temp_output_104_0_g36));
				float4 Final_ColorsHDR_RGBA206_g36 = appendResult108_g36;
				
				
				finalColor = Final_ColorsHDR_RGBA206_g36;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "TVEShaderElementGUI"
	
	
}
/*ASEBEGIN
Version=17802
1927;7;1906;1014;1074.908;1764.261;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;94;-384,-1536;Inherit;False;Property;_Intensity;Intensity;31;1;[HideInInspector];Create;True;0;0;True;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;96;-640,-1280;Half;False;Property;_Banner1;Banner;0;0;Create;True;0;0;True;1;StyledBanner(Color HDR Element);0;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;22;-640,-1536;Half;False;Property;_Simple;Color;32;2;[HideInInspector];[HDR];Create;False;0;0;True;0;1,0.5,0,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;97;-640,-1152;Inherit;False;Base Element;1;;36;0e972c73cae2ee54ea51acc9738801d0;3,145,1,148,1,146,1;0;4;FLOAT4;0;FLOAT3;85;FLOAT;86;FLOAT;339
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;-368,-1152;Half;False;True;-1;2;TVEShaderElementGUI;0;1;BOXOPHOBIC/The Vegetation Engine/Elements/Colors HDR Element;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;True;False;True;0;False;-1;True;True;True;True;False;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;0;False;-1;True;False;0;False;-1;0;False;-1;True;3;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;PreviewType=Plane;True;2;0;False;False;False;False;False;False;False;False;False;True;1;LightMode=ForwardBase;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;0
Node;AmplifyShaderEditor.CommentaryNode;83;-640,-1664;Inherit;False;1025.136;100;Legacy;0;;1,0.252,0,1;0;0
WireConnection;0;0;97;0
WireConnection;0;1;97;339
ASEEND*/
//CHKSM=7D7C960B180EDF60D0D2E89F2412DFA38F96C137