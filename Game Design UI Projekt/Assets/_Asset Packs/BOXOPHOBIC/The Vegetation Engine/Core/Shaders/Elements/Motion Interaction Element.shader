// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BOXOPHOBIC/The Vegetation Engine/Elements/Motion Interaction Element"
{
	Properties
	{
		[StyledBanner(Motion Interaction Element)]_Banner("Banner", Float) = 0
		[HideInInspector]_IsVersion("_IsVersion", Float) = 110
		[HideInInspector]_IsStandardPipeline("_IsStandardPipeline", Float) = 1
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector]_IsTVEElement("_IsTVEElement", Float) = 1
		_ElementIntensity("Intensity", Range( 0 , 1)) = 1
		[StyledCategory(Element)]_ElementCat("[ Element Cat ]", Float) = 0
		[NoScaleOffset]_MainTex("Texture", 2D) = "white" {}
		_MainUVs("Texture UVs", Vector) = (1,1,0,0)
		[StyledInteractive(_ElementMode, 0)]_SeasonMode_Simple("# SeasonMode_Simple", Float) = 0
		[StyledInteractive(_ElementMode, 1)]_SeasonMode_Seasons("# SeasonMode_Seasons", Float) = 0
		[StyledCategory(Advanced)]_AdvancedCat("[ Advanced Cat]", Float) = 1
		[IntRange]__priority("Priority", Range( -50 , 50)) = 0
		[HideInInspector]_Intensity("Intensity", Range( 0 , 1)) = 1

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Transparent" "Queue"="Transparent" "PreviewType"="Plane" }
	LOD 0

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Back
		ColorMask RGBA
		ZWrite Off
		ZTest LEqual
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR


			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
#endif
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_color : COLOR;
			};

			uniform half _IsVersion;
			uniform half __priority;
			uniform half _IsTVEElement;
			uniform half _ElementCat;
			uniform half _AdvancedCat;
			uniform half _SeasonMode_Seasons;
			uniform half _SeasonMode_Simple;
			uniform half _IsTVEShader;
			uniform half _Banner;
			uniform float _Intensity;
			uniform half _IsStandardPipeline;
			uniform sampler2D _MainTex;
			uniform half4 _MainUVs;
			uniform float _ElementIntensity;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				float3 temp_cast_0 = (( _IsStandardPipeline * 0.0 )).xxx;
				
				o.ase_texcoord1.xy = v.ase_texcoord.xy;
				o.ase_color = v.color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.zw = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = temp_cast_0;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);

#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
#endif
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
#endif
				float4 tex2DNode17_g97 = tex2D( _MainTex, ( ( i.ase_texcoord1.xy * (_MainUVs).xy ) + (_MainUVs).zw ) );
				half MainTex_R73_g97 = tex2DNode17_g97.r;
				half MainTex_G265_g97 = tex2DNode17_g97.g;
				float3 appendResult274_g97 = (float3(-(MainTex_R73_g97*2.0 + -1.0) , 0.0 , -(MainTex_G265_g97*2.0 + -1.0)));
				float3 temp_output_275_0_g97 = mul( unity_ObjectToWorld, float4( appendResult274_g97 , 0.0 ) ).xyz;
				float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
				float temp_output_7_0_g100 = -ase_objectScale.x;
				half normalX_WS284_g97 = ( ( temp_output_275_0_g97.x - temp_output_7_0_g100 ) / ( ase_objectScale.x - temp_output_7_0_g100 ) );
				float lerpResult353_g97 = lerp( ( 1.0 - normalX_WS284_g97 ) , normalX_WS284_g97 , i.ase_color.r);
				float temp_output_7_0_g101 = -ase_objectScale.z;
				half normalZ_WS285_g97 = ( ( temp_output_275_0_g97.z - temp_output_7_0_g101 ) / ( ase_objectScale.z - temp_output_7_0_g101 ) );
				float lerpResult354_g97 = lerp( ( 1.0 - normalZ_WS285_g97 ) , normalZ_WS285_g97 , i.ase_color.r);
				half MainTex_B266_g97 = tex2DNode17_g97.b;
				float3 appendResult292_g97 = (float3(lerpResult353_g97 , lerpResult354_g97 , MainTex_B266_g97));
				float3 gammaToLinear348_g97 = GammaToLinearSpace( appendResult292_g97 );
				#ifdef UNITY_COLORSPACE_GAMMA
				float3 staticSwitch347_g97 = gammaToLinear348_g97;
				#else
				float3 staticSwitch347_g97 = gammaToLinear348_g97;
				#endif
				half Element_Intensity56_g97 = ( _ElementIntensity * i.ase_color.a );
				float temp_output_293_0_g97 = ( MainTex_B266_g97 * Element_Intensity56_g97 );
				float4 appendResult295_g97 = (float4(staticSwitch347_g97 , temp_output_293_0_g97));
				half4 Final_MotionInteraction_RGBA304_g97 = appendResult295_g97;
				
				
				finalColor = Final_MotionInteraction_RGBA304_g97;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "TVEShaderElementGUI"
	
	
}
/*ASEBEGIN
Version=17802
1927;7;1906;1014;2540.358;2343.591;2.71204;True;False
Node;AmplifyShaderEditor.RangedFloatNode;10;-640,-1408;Inherit;False;Property;_Intensity;Intensity;31;1;[HideInInspector];Create;True;0;0;True;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;95;-640,-1152;Half;False;Property;_Banner;Banner;0;0;Create;True;0;0;True;1;StyledBanner(Motion Interaction Element);0;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;113;-640,-1024;Inherit;False;Base Element;1;;97;0e972c73cae2ee54ea51acc9738801d0;3,145,6,148,6,146,6;0;4;FLOAT4;0;FLOAT3;85;FLOAT;86;FLOAT;339
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;-256,-1024;Float;False;True;-1;2;TVEShaderElementGUI;0;1;BOXOPHOBIC/The Vegetation Engine/Elements/Motion Interaction Element;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;True;False;True;0;False;-1;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;0;False;-1;True;False;0;False;-1;0;False;-1;True;3;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;PreviewType=Plane;True;2;0;False;False;False;False;False;False;False;False;False;True;1;LightMode=ForwardBase;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;0
Node;AmplifyShaderEditor.CommentaryNode;81;-640,-1536;Inherit;False;1025.136;100;Internal;0;;1,0.252,0,1;0;0
WireConnection;0;0;113;0
WireConnection;0;1;113;339
ASEEND*/
//CHKSM=934531D6063693B9A920B0A5D5B546B8685FB382