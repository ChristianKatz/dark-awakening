// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BOXOPHOBIC/The Vegetation Engine/Elements/Colors Element"
{
	Properties
	{
		[StyledBanner(Color Element)]_Banner("Banner", Float) = 0
		[HideInInspector]_IsVersion("_IsVersion", Float) = 110
		[HideInInspector]_IsStandardPipeline("_IsStandardPipeline", Float) = 1
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector]_IsTVEElement("_IsTVEElement", Float) = 1
		_ElementIntensity("Intensity", Range( 0 , 1)) = 1
		[StyledCategory(Element)]_ElementCat("[ Element Cat ]", Float) = 0
		[NoScaleOffset]_MainTex("Texture", 2D) = "white" {}
		_MainUVs("Texture UVs", Vector) = (1,1,0,0)
		[Enum(Main,0,Seasons,1)]_ElementMode("Element Mode", Float) = 0
		[StyledInteractive(_ElementMode, 0)]_SeasonMode_Simple("# SeasonMode_Simple", Float) = 0
		[Space(10)]_MainColor("Main", Color) = (0.5019608,0.5019608,0.5019608,1)
		[StyledInteractive(_ElementMode, 1)]_SeasonMode_Seasons("# SeasonMode_Seasons", Float) = 0
		[Space(10)]_WinterColor("Winter", Color) = (0.5019608,0.5019608,0.5019608,1)
		_SpringColor("Spring", Color) = (0.5019608,0.5019608,0.5019608,1)
		_SummerColor("Summer", Color) = (0.5019608,0.5019608,0.5019608,1)
		_AutumnColor("Autumn", Color) = (0.5019608,0.5019608,0.5019608,1)
		[StyledCategory(Advanced)]_AdvancedCat("[ Advanced Cat]", Float) = 1
		[IntRange]__priority("Priority", Range( -50 , 50)) = 0
		[HideInInspector]_Intensity("Intensity", Range( 0 , 1)) = 1
		[HideInInspector][Enum(Simple,0,Seasons,1)]_SeasonMode("Color Mode", Float) = 0
		[HideInInspector][Space(10)]_Simple("Simple Color", Color) = (0.5019608,0.5019608,0.5019608,1)
		[HideInInspector][Space(10)]_Winter("Winter Color", Color) = (0.2720588,0.2720588,0.2720588,1)
		[HideInInspector]_Spring("Spring Color", Color) = (0.5779412,0.6397059,0,1)
		[HideInInspector]_Summer("Summer Color", Color) = (0.1908215,0.5220588,0,1)
		[HideInInspector]_Autumn("Autumn Color", Color) = (0.7205882,0.4025354,0,1)

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Transparent" "Queue"="Transparent" "PreviewType"="Plane" }
	LOD 0

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Back
		ColorMask RGB
		ZWrite Off
		ZTest LEqual
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
#endif
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord1 : TEXCOORD1;
				float4 ase_color : COLOR;
			};

			uniform half4 _Winter;
			uniform half4 _Simple;
			uniform half _IsVersion;
			uniform half __priority;
			uniform half _IsTVEElement;
			uniform half _ElementCat;
			uniform half _AdvancedCat;
			uniform half _SeasonMode_Seasons;
			uniform half _SeasonMode_Simple;
			uniform half _IsTVEShader;
			uniform half _Banner;
			uniform half4 _Autumn;
			uniform half _SeasonMode;
			uniform half4 _Spring;
			uniform half4 _Summer;
			uniform half _Intensity;
			uniform half _IsStandardPipeline;
			uniform half4 _MainColor;
			uniform sampler2D _MainTex;
			uniform half4 _MainUVs;
			uniform half4 TVE_SeasonOptions;
			uniform half4 _WinterColor;
			uniform half4 _SpringColor;
			uniform half TVE_SeasonLerp;
			uniform half4 _SummerColor;
			uniform half4 _AutumnColor;
			uniform half _ElementMode;
			uniform half _ElementIntensity;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				half3 temp_cast_0 = (( _IsStandardPipeline * 0.0 )).xxx;
				
				o.ase_texcoord1.xy = v.ase_texcoord.xy;
				o.ase_color = v.color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.zw = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = temp_cast_0;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);

#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
#endif
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
#endif
				half3 Color_Main49_g36 = (_MainColor).rgb;
				half4 tex2DNode17_g36 = tex2D( _MainTex, ( ( i.ase_texcoord1.xy * (_MainUVs).xy ) + (_MainUVs).zw ) );
				half3 MainTex_RGB19_g36 = (tex2DNode17_g36).rgb;
				half TVE_SeasonOptions_X50_g36 = TVE_SeasonOptions.x;
				half3 Color_Winter58_g36 = (_WinterColor).rgb;
				half3 Color_Spring59_g36 = (_SpringColor).rgb;
				half TVE_SeasonLerp54_g36 = TVE_SeasonLerp;
				half3 lerpResult13_g36 = lerp( Color_Winter58_g36 , Color_Spring59_g36 , TVE_SeasonLerp54_g36);
				half TVE_SeasonOptions_Y51_g36 = TVE_SeasonOptions.y;
				half3 Color_Summer60_g36 = (_SummerColor).rgb;
				half3 lerpResult14_g36 = lerp( Color_Spring59_g36 , Color_Summer60_g36 , TVE_SeasonLerp54_g36);
				half TVE_SeasonOptions_Z52_g36 = TVE_SeasonOptions.z;
				half3 Color_Autumn61_g36 = (_AutumnColor).rgb;
				half3 lerpResult15_g36 = lerp( Color_Summer60_g36 , Color_Autumn61_g36 , TVE_SeasonLerp54_g36);
				half TVE_SeasonOptions_W53_g36 = TVE_SeasonOptions.w;
				half3 lerpResult12_g36 = lerp( Color_Autumn61_g36 , Color_Winter58_g36 , TVE_SeasonLerp54_g36);
				half Element_Mode55_g36 = _ElementMode;
				half3 lerpResult30_g36 = lerp( ( Color_Main49_g36 * MainTex_RGB19_g36 ) , ( MainTex_RGB19_g36 * ( ( TVE_SeasonOptions_X50_g36 * lerpResult13_g36 ) + ( TVE_SeasonOptions_Y51_g36 * lerpResult14_g36 ) + ( TVE_SeasonOptions_Z52_g36 * lerpResult15_g36 ) + ( TVE_SeasonOptions_W53_g36 * lerpResult12_g36 ) ) ) , Element_Mode55_g36);
				half MainTex_A74_g36 = tex2DNode17_g36.a;
				half Element_Intensity56_g36 = ( _ElementIntensity * i.ase_color.a );
				half temp_output_34_0_g36 = ( MainTex_A74_g36 * Element_Intensity56_g36 );
				half4 appendResult35_g36 = (half4(lerpResult30_g36 , temp_output_34_0_g36));
				half4 Final_Colors_RGBA143_g36 = appendResult35_g36;
				
				
				finalColor = Final_Colors_RGBA143_g36;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "TVEShaderElementGUI"
	
	
}
/*ASEBEGIN
Version=17802
1927;7;1906;1014;1162.556;1152.789;1;True;False
Node;AmplifyShaderEditor.ColorNode;24;-128,-1024;Half;False;Property;_Spring;Spring Color;35;1;[HideInInspector];Create;False;0;0;True;0;0.5779412,0.6397059,0,1;0.5779411,0.6397059,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;96;-640,-832;Half;False;Property;_SeasonMode;Color Mode;32;2;[HideInInspector];[Enum];Create;False;2;Simple;0;Seasons;1;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;95;-384,-832;Half;False;Property;_Intensity;Intensity;31;1;[HideInInspector];Create;True;0;0;True;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;25;96,-1024;Half;False;Property;_Summer;Summer Color;36;1;[HideInInspector];Create;False;0;0;True;0;0.1908215,0.5220588,0,1;0.1908214,0.5220588,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;26;320,-1024;Half;False;Property;_Autumn;Autumn Color;37;1;[HideInInspector];Create;False;0;0;True;0;0.7205882,0.4025354,0,1;0.7205882,0.4025353,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;22;-640,-1024;Half;False;Property;_Simple;Simple Color;33;1;[HideInInspector];Create;False;0;0;True;1;Space(10);0.5019608,0.5019608,0.5019608,1;0.5019608,0.5019608,0.5019608,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;23;-384,-1024;Half;False;Property;_Winter;Winter Color;34;1;[HideInInspector];Create;False;0;0;True;1;Space(10);0.2720588,0.2720588,0.2720588,1;0.2720588,0.2720588,0.2720588,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;97;-640,-640;Half;False;Property;_Banner;Banner;0;0;Create;True;0;0;True;1;StyledBanner(Color Element);0;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;98;-640,-512;Inherit;False;Base Element;1;;36;0e972c73cae2ee54ea51acc9738801d0;3,145,0,148,0,146,0;0;4;FLOAT4;0;FLOAT3;85;FLOAT;86;FLOAT;339
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;-384,-512;Half;False;True;-1;2;TVEShaderElementGUI;0;1;BOXOPHOBIC/The Vegetation Engine/Elements/Colors Element;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;True;False;True;0;False;-1;True;True;True;True;False;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;0;False;-1;True;False;0;False;-1;0;False;-1;True;3;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;PreviewType=Plane;True;2;0;False;False;False;False;False;False;False;False;False;True;1;LightMode=ForwardBase;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;0
Node;AmplifyShaderEditor.CommentaryNode;93;-640,-1152;Inherit;False;1152.136;100;Legacy;0;;1,0.252,0,1;0;0
WireConnection;0;0;98;0
WireConnection;0;1;98;339
ASEEND*/
//CHKSM=F32D2FB97A662623CBCEB8651ED64C19E8AC4E62