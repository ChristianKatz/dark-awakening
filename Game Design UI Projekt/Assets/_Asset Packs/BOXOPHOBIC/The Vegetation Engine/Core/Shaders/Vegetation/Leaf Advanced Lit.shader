// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BOXOPHOBIC/The Vegetation Engine/Vegetation/Leaf Advanced Lit"
{
	Properties
	{
		[StyledBanner(Leaf Advanced Lit)]_Banner("Banner", Float) = 0
		[StyledMessage(Info, In Standard Render Pipeline the Leaf Advanced Lit shader is always rendered in Forward regardless of the Camera Rendering Path mode., 5, 5 )]_SubsurfaceMessageDeferred("!!! Subsurface Message Deferred !!!", Float) = 0
		[HideInInspector]_IsVersion("_IsVersion", Float) = 110
		[HideInInspector]_IsStandardPipeline("_IsStandardPipeline", Float) = 1
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector][Toggle]_IsUpdated("_IsUpdated", Float) = 0
		[HideInInspector]_VariationMode("_VariationMode", Float) = -1
		[HideInInspector]_MaxBoundsInfo("_MaxBoundsInfo", Vector) = (1,1,1,1)
		[StyledCategory(Render Settings)]_RenderingCat("[ Rendering Cat ]", Float) = 0
		[Enum(Opaque,0,Transparent,1)]__surface("Render Mode", Float) = 0
		[Enum(Both,0,Back,1,Front,2)]__cull("Render Faces", Float) = 0
		[Enum(Flip,0,Mirror,1,None,2)]__normals("Render Normals", Float) = 0
		[StyledInteractive(__surface, 1)]_RenderMode_TransparentInteractive("# RenderMode_TransparentInteractive", Float) = 0
		[Enum(Alpha,0,Premultiply,1)]__blend("Render Blending", Float) = 0
		[Enum(Off,0,On,1)]__zw("Render ZWrite", Float) = 1
		[StyledInteractive(ON)]_RenderMode_ResetInteractive("# RenderMode_ResetInteractive", Float) = 0
		[Toggle][Space(10)]__clip("Alpha Clipping", Float) = 0
		[StyledInteractive(__clip, 1)]_AlphaClipInteractive("# AlphaClipInteractive", Float) = 0
		_Cutoff("Alpha Treshold", Range( 0 , 1)) = 0.5
		[HideInInspector]__cliptreshold("__cliptreshold", Float) = 0
		[StyledCategory(Global Settings)]_GlobalSettingsCat("[ Global Settings Cat ]", Float) = 0
		_GlobalColors("Global Colors", Range( 0 , 1)) = 1
		[HideInInspector]_GlobalOverlayMode("Global Overlay Mode", Float) = 1
		_GlobalOverlay("Global Overlay", Range( 0 , 1)) = 1
		_GlobalWetness("Global Wetness", Range( 0 , 1)) = 1
		_GlobalLeaves("Global Leaves", Range( 0 , 1)) = 1
		_GlobalSize("Global Size", Range( 0 , 1)) = 1
		_GlobalSizeFade("Global Size Fade", Range( 0 , 1)) = 1
		[StyledCategory(Material Shading)]_MaterialShadingCat("[ Material Shading Cat ]", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Subsurface Mask. Use the Min Max sliders to adjust or invert the subsurface effect., _IsLeafShader, 1, 5, 10 )]_MessageSubsurfaceMask("!!! Message Leaf Subsurface Mask !!!", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Subsurface Mask. Use the Min Max sliders to adjust or invert the subsurface effect., _IsCrossShader, 1, 5, 10 )]_MessageCrossSubsurfaceMask("!!! Message Cross Subsurface Mask !!!", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Height Map. Use the Min Max values to adjust or invert the height., _IsTessellationShader, 1, 5, 10 )]_MessageHeightMap("!!! Message Height Map !!!", Float) = 0
		[HDR]_SubsurfaceColor("Subsurface Color", Color) = (0.3315085,0.490566,0,1)
		_SubsurfaceValue("Subsurface Intensity", Range( 0 , 1)) = 1
		_SubsurfaceMinValue("Subsurface Min", Range( 0 , 1)) = 0
		_SubsurfaceMaxValue("Subsurface Max", Range( 0 , 1)) = 1
		[StyledSpace(10)]_MaterialShadingSpaceDrawer("# Material Shading Space", Float) = 0
		_ObjectOcclusionValue("Object Occlusion", Range( 0 , 10)) = 0
		[Space(10)]_OverlayUVTilling("Overlay Tilling", Range( 0 , 10)) = 1
		_OverlayVariation("Overlay Variation", Range( 0 , 1)) = 0
		[StyledCategory(Main Shading)]_MainShadingCat("[ Main Shading Cat ]", Float) = 0
		[Enum(Constant,0,Variation,1)]_MainColorMode("Main Color Mode", Float) = 1
		[Space(10)]_MainColor("Main Color One", Color) = (1,1,1,1)
		_MainColorVariation("Main Color Two", Color) = (1,1,1,1)
		[NoScaleOffset]_MainAlbedoTex("Main Albedo", 2D) = "white" {}
		[NoScaleOffset]_MainNormalTex("Main Normal", 2D) = "bump" {}
		[NoScaleOffset]_MainMaskTex("Main Mask", 2D) = "white" {}
		[Space(10)]_MainUVs("Main UVs", Vector) = (1,1,0,0)
		_MainNormalValue("Main Normal", Range( -8 , 8)) = 1
		_MainMetallicValue("Main Metallic (R)", Range( 0 , 1)) = 0
		_MainOcclusionValue("Main Occlusion (G)", Range( 0 , 1)) = 1
		_MainSmoothnessValue("Main Smoothness (A)", Range( 0 , 1)) = 1
		[StyledCategory(Motion Settings)]_MotionCat("[ Motion Cat ]", Float) = 0
		[HideInInspector][Enum(Off,0,Procedural,1,Hierarchical,2)]_MotionMode("Motion Mode", Float) = 0
		[HideInInspector][Enum(Object Pivot,0,Element Pivot,1)]_PivotMode("Pivot Mode", Float) = 0
		[Toggle]_UseMotion_Main("Motion Main", Float) = 1
		[Toggle]_UseMotion_Leaves("Motion Leaves", Float) = 1
		[HideInInspector][Space(10)]_MotionAmplitude_10("Primary Bending", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector][IntRange]_MotionSpeed_10("Primary Speed", Float) = 2
		[HideInInspector]_MotionScale_10("Primary Elasticity", Float) = 0
		[HideInInspector]_MotionVariation_10("Primary Variation", Float) = 0
		[HideInInspector][Space(10)]_MotionAmplitude_20("Secundary Rolling", Float) = 0
		[HideInInspector]_MotionVertical_20("Secundary Vertical", Float) = 0
		[HideInInspector][IntRange]_MotionSpeed_20("Secundary Speed", Float) = 5
		[HideInInspector]_MotionScale_20("Secundary Elasticity", Float) = 0
		[HideInInspector]_MotionVariation_20("Secundary Variation", Range( 0 , 5)) = 0
		[HideInInspector][Space(10)]_MotionAmplitude_32("Flutter Amplitude", Float) = 0
		[HideInInspector][IntRange]_MotionSpeed_32("Flutter Speed", Float) = 15
		[HideInInspector]_MotionScale_32("Flutter Elasticity", Float) = 100
		[HideInInspector]_MotionVariation_32("Flutter Variation", Float) = 100
		[HideInInspector][Space(10)]_InteractionAmplitude("Interaction Bending", Float) = 0
		[StyledCategory(Advanced)]_AdvancedCat("[ Advanced Cat]", Float) = 0
		[IntRange]__priority("Priority", Range( -50 , 50)) = 0
		[HideInInspector]__src("__src", Float) = 1
		[HideInInspector]__premul("__premul", Float) = 0
		[HideInInspector]_Color("_Color", Color) = (0,0,0,0)
		[HideInInspector]_MainTex("_MainTex", 2D) = "white" {}
		[HideInInspector]_VertexOcclusion("_VertexOcclusion", Float) = 0
		[HideInInspector]_MainMaskValue("_MainMaskValue", Float) = 0
		[HideInInspector][Enum(Translucency,0,Thickness,1)]_SubsurfaceMode("_SubsurfaceMode", Float) = 0
		[HideInInspector]_ObjectThicknessValue("_ObjectThicknessValue", Float) = 0
		[HideInInspector]__normalsoptions("__normalsoptions", Vector) = (1,1,1,0)
		[HideInInspector]_IsLeafShader("_IsLeafShader", Float) = 1
		[HideInInspector]_IsAdvancedShader("_IsAdvancedShader", Float) = 1
		[HideInInspector]_IsLitShader("_IsLitShader", Float) = 1
		[HideInInspector]_Cutoff("_Cutoff", Float) = 0.5
		[HideInInspector][Enum(Both,0,Back,1,Front,2)]__cull("__cull", Float) = 0
		[HideInInspector]__dst("__dst", Float) = 0
		[HideInInspector]__zw("__zw", Float) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" "DisableBatching" = "True" }
		Cull [__cull]
		ZWrite [__zw]
		Offset  0 , 0
		Blend [__src] [__dst]
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "UnityStandardUtils.cginc"
		#include "UnityPBSLighting.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma shader_feature _ALPHATEST_ON
		  
		//FEATURES BEGIN
		//FEATURES END
		    
		#define TVE_DISABLE_UNSAFE_BATCHING
		#pragma exclude_renderers d3d9 d3d11_9x gles 
		#pragma surface surf StandardCustom keepalpha addshadow fullforwardshadows exclude_path:deferred novertexlights nodynlightmap dithercrossfade vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			half ASEVFace : VFACE;
			float3 worldNormal;
			INTERNAL_DATA
			float4 vertexColor : COLOR;
		};

		struct SurfaceOutputStandardCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			half3 Transmission;
		};

		uniform half _IsLeafShader;
		uniform half _IsAdvancedShader;
		uniform float4 _MaxBoundsInfo;
		uniform half _AdvancedCat;
		uniform half __surface;
		uniform half _PivotMode;
		uniform half __zw;
		uniform half _GlobalSettingsCat;
		uniform half _IsTVEShader;
		uniform half _IsUpdated;
		uniform half _MaterialShadingCat;
		uniform half __blend;
		uniform half __src;
		uniform half _Cutoff;
		uniform half __dst;
		uniform half _RenderMode_ResetInteractive;
		uniform half _MessageHeightMap;
		uniform half _MessageCrossSubsurfaceMask;
		uniform float _SubsurfaceMode;
		uniform half _MainMaskValue;
		uniform half _VertexOcclusion;
		uniform half _MainColorMode;
		uniform half _RenderMode_TransparentInteractive;
		uniform half _GlobalOverlayMode;
		uniform half __cull;
		uniform half __clip;
		uniform sampler2D _MainTex;
		uniform half __normals;
		uniform half _VariationMode;
		uniform float4 _Color;
		uniform half _RenderingCat;
		uniform half _IsVersion;
		uniform half __cliptreshold;
		uniform half __priority;
		uniform half _MotionMode;
		uniform half _MainShadingCat;
		uniform half _AlphaClipInteractive;
		uniform half _ObjectThicknessValue;
		uniform half _MessageSubsurfaceMask;
		uniform float _SubsurfaceMessageDeferred;
		uniform half _Banner;
		uniform half _IsLitShader;
		uniform half TVE_SizeFadeEnd;
		uniform half TVE_SizeFadeStart;
		uniform half _GlobalSizeFade;
		uniform half _MotionAmplitude_20;
		uniform half TVE_Amplitude2;
		uniform sampler2D TVE_NoiseTex;
		uniform half TVE_NoiseSpeed;
		uniform half TVE_NoiseSize;
		uniform half TVE_NoiseContrast;
		uniform half _UseMotion_Main;
		uniform half _MotionCat;
		uniform half _MotionSpeed_20;
		uniform half _MotionVariation_20;
		uniform half _MotionScale_20;
		uniform half _MotionAmplitude_10;
		uniform half TVE_Amplitude1;
		uniform sampler2D TVE_MotionTex;
		uniform half4 TVE_MotionCoord;
		uniform half _MotionSpeed_10;
		uniform half _MotionVariation_10;
		uniform half _MotionScale_10;
		uniform float _InteractionAmplitude;
		uniform half _MotionScale_32;
		uniform half _MotionSpeed_32;
		uniform half _MotionVariation_32;
		uniform half _MotionAmplitude_32;
		uniform half _UseMotion_Leaves;
		uniform half _MotionVertical_20;
		uniform sampler2D TVE_ExtrasTex;
		uniform half4 TVE_ExtrasCoord;
		uniform half _GlobalSize;
		uniform half _IsStandardPipeline;
		uniform half _MainNormalValue;
		uniform sampler2D _MainNormalTex;
		uniform half4 _MainUVs;
		uniform half3 __normalsoptions;
		uniform half TVE_OverlayNormalValue;
		uniform sampler2D TVE_OverlayNormalTex;
		uniform half TVE_OverlayUVTilling;
		uniform half _OverlayUVTilling;
		uniform half4 TVE_OverlayDirection;
		uniform half _OverlayVariation;
		uniform half TVE_OverlayIntensity;
		uniform half _GlobalOverlay;
		uniform half4 _MainColorVariation;
		uniform half4 _MainColor;
		uniform sampler2D _MainAlbedoTex;
		uniform sampler2D TVE_ColorsTex;
		uniform half4 TVE_ColorsCoord;
		uniform half _GlobalColors;
		uniform half4 TVE_OverlayColor;
		uniform sampler2D TVE_OverlayAlbedoTex;
		uniform half __premul;
		uniform sampler2D _MainMaskTex;
		uniform half _MainMetallicValue;
		uniform half _MainSmoothnessValue;
		uniform half TVE_OverlaySmoothness;
		uniform float TVE_Wetness;
		uniform half _GlobalWetness;
		uniform half _ObjectOcclusionValue;
		uniform half _MainOcclusionValue;
		uniform half4 _SubsurfaceColor;
		uniform half _SubsurfaceMinValue;
		uniform half _SubsurfaceMaxValue;
		uniform half _MaterialShadingSpaceDrawer;
		uniform half _SubsurfaceValue;
		uniform half _GlobalLeaves;


		half3 ObjectPosition_unity_ObjectToWorld(  )
		{
			return half3(unity_ObjectToWorld[0].w, unity_ObjectToWorld[1].w, unity_ObjectToWorld[2].w );
		}


		float3 DecodeFloatToVector3( int enc )
		{
			  float3 result ;
			  result.x = ((enc >> 0) & 0xFF) / 255.0f;
			  result.y = ((enc >> 8) & 0xFF) / 255.0;
			  result.z = ((enc >>16) & 0xFF) / 255.0;
			  //result.w = ((enc >> 32) & 0xFF) / 255.0;
			  return result;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			half3 localObjectPosition_unity_ObjectToWorld8_g11605 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_731_7_g11594 = localObjectPosition_unity_ObjectToWorld8_g11605;
			half Global_SizeFadeEnd287_g11594 = TVE_SizeFadeEnd;
			float temp_output_7_0_g11639 = Global_SizeFadeEnd287_g11594;
			half Global_SizeFadeStart276_g11594 = TVE_SizeFadeStart;
			half Global_SizeFade694_g11594 = _GlobalSizeFade;
			float lerpResult348_g11594 = lerp( 1.0 , saturate( ( ( distance( _WorldSpaceCameraPos , temp_output_731_7_g11594 ) - temp_output_7_0_g11639 ) / ( Global_SizeFadeStart276_g11594 - temp_output_7_0_g11639 ) ) ) , Global_SizeFade694_g11594);
			float3 ase_vertex3Pos = v.vertex.xyz;
			half3 VertexPos40_g11652 = ase_vertex3Pos;
			float3 appendResult74_g11652 = (float3(0.0 , VertexPos40_g11652.y , 0.0));
			half3 VertexPosRotationAxis50_g11652 = appendResult74_g11652;
			float3 break84_g11652 = VertexPos40_g11652;
			float3 appendResult81_g11652 = (float3(break84_g11652.x , 0.0 , break84_g11652.z));
			half3 VertexPosOtherAxis82_g11652 = appendResult81_g11652;
			float temp_output_7_0_g11596 = UNITY_PI;
			half Bounds_Radius121_g11594 = _MaxBoundsInfo.x;
			#ifdef UNITY_INSTANCING_ENABLED
				float staticSwitch8_g11596 = temp_output_7_0_g11596;
			#else
				float staticSwitch8_g11596 = Bounds_Radius121_g11594;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float staticSwitch10_g11596 = staticSwitch8_g11596;
			#else
				float staticSwitch10_g11596 = temp_output_7_0_g11596;
			#endif
			half Motion_Max_Rolling1137_g11594 = staticSwitch10_g11596;
			half Global_Amplitude_270_g11594 = TVE_Amplitude2;
			int enc15_g11630 = (int)v.texcoord3.x;
			float3 localDecodeFloatToVector315_g11630 = DecodeFloatToVector3( enc15_g11630 );
			float3 break17_g11630 = localDecodeFloatToVector315_g11630;
			half Mesh_Motion_260_g11594 = break17_g11630.y;
			float temp_output_4_0_g11625 = 1.0;
			float mulTime2_g11625 = _Time.y * temp_output_4_0_g11625;
			float2 temp_cast_1 = (TVE_NoiseSpeed).xx;
			half3 localObjectPosition_unity_ObjectToWorld8_g11624 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11623 = localObjectPosition_unity_ObjectToWorld8_g11624;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11623 = temp_output_7_0_g11623;
			#else
				float3 staticSwitch8_g11623 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11623 = staticSwitch8_g11623;
			#else
				float3 staticSwitch10_g11623 = temp_output_7_0_g11623;
			#endif
			float2 panner73_g11622 = ( mulTime2_g11625 * temp_cast_1 + ( (staticSwitch10_g11623).xz * TVE_NoiseSize ));
			float4 temp_cast_2 = (TVE_NoiseContrast).xxxx;
			float4 break142_g11622 = pow( abs( tex2Dlod( TVE_NoiseTex, float4( panner73_g11622, 0, 0.0) ) ) , temp_cast_2 );
			half Global_NoiseTex_R34_g11594 = break142_g11622.r;
			half Global_NoiseTex_G38_g11594 = break142_g11622.g;
			half Motion_UseMain56_g11594 = ( _UseMotion_Main + ( _MotionCat * 0.0 ) );
			half MotionSpeed265_g11594 = _MotionSpeed_20;
			half Input_Speed62_g11609 = MotionSpeed265_g11594;
			float temp_output_4_0_g11612 = Input_Speed62_g11609;
			float mulTime2_g11612 = _Time.y * temp_output_4_0_g11612;
			half3 localObjectPosition_unity_ObjectToWorld8_g11613 = ObjectPosition_unity_ObjectToWorld();
			float3 break9_g11613 = localObjectPosition_unity_ObjectToWorld8_g11613;
			half MotionVariation264_g11594 = _MotionVariation_20;
			float temp_output_218_0_g11609 = MotionVariation264_g11594;
			float Motion_Variation284_g11609 = ( temp_output_218_0_g11609 * v.color.a );
			half MotionScale262_g11594 = _MotionScale_20;
			float Motion_Scale287_g11609 = ( MotionScale262_g11594 * ase_worldPos.x );
			half Motion_Y138_g11594 = ( ( _MotionAmplitude_20 * Motion_Max_Rolling1137_g11594 ) * Global_Amplitude_270_g11594 * Mesh_Motion_260_g11594 * ( Global_NoiseTex_R34_g11594 + Global_NoiseTex_G38_g11594 ) * Motion_UseMain56_g11594 * sin( ( mulTime2_g11612 + ( break9_g11613.x + break9_g11613.z ) + Motion_Variation284_g11609 + Motion_Scale287_g11609 ) ) );
			half Angle44_g11652 = Motion_Y138_g11594;
			float3 temp_output_188_19_g11594 = ( VertexPosRotationAxis50_g11652 + ( VertexPosOtherAxis82_g11652 * cos( Angle44_g11652 ) ) + ( cross( float3(0,1,0) , VertexPosOtherAxis82_g11652 ) * sin( Angle44_g11652 ) ) );
			half3 VertexPos40_g11641 = temp_output_188_19_g11594;
			float3 appendResult74_g11641 = (float3(VertexPos40_g11641.x , 0.0 , 0.0));
			half3 VertexPosRotationAxis50_g11641 = appendResult74_g11641;
			float3 break84_g11641 = VertexPos40_g11641;
			float3 appendResult81_g11641 = (float3(0.0 , break84_g11641.y , break84_g11641.z));
			half3 VertexPosOtherAxis82_g11641 = appendResult81_g11641;
			float temp_output_7_0_g11608 = UNITY_PI;
			half Bounds_Height374_g11594 = _MaxBoundsInfo.y;
			#ifdef UNITY_INSTANCING_ENABLED
				float staticSwitch8_g11608 = temp_output_7_0_g11608;
			#else
				float staticSwitch8_g11608 = ( Bounds_Height374_g11594 * UNITY_PI );
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float staticSwitch10_g11608 = staticSwitch8_g11608;
			#else
				float staticSwitch10_g11608 = temp_output_7_0_g11608;
			#endif
			half Motion_Max_Bending1133_g11594 = staticSwitch10_g11608;
			half Global_Amplitude_136_g11594 = TVE_Amplitude1;
			half3 localObjectPosition_unity_ObjectToWorld8_g11616 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11617 = localObjectPosition_unity_ObjectToWorld8_g11616;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11617 = temp_output_7_0_g11617;
			#else
				float3 staticSwitch8_g11617 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11617 = staticSwitch8_g11617;
			#else
				float3 staticSwitch10_g11617 = temp_output_7_0_g11617;
			#endif
			float3 temp_output_18_0_g11615 = (tex2Dlod( TVE_MotionTex, float4( ( (TVE_MotionCoord).xy + ( TVE_MotionCoord.z * (staticSwitch10_g11617).xz ) ), 0, 0.0) )).rgb;
			float3 linearToGamma28_g11615 = LinearToGammaSpace( temp_output_18_0_g11615 );
			float3 break322_g11614 = linearToGamma28_g11615;
			float3 appendResult323_g11614 = (float3(break322_g11614.x , 0.0 , break322_g11614.y));
			float3 temp_output_324_0_g11614 = (appendResult323_g11614*2.0 + -1.0);
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float3 temp_output_7_0_g11618 = ( mul( unity_WorldToObject, float4( temp_output_324_0_g11614 , 0.0 ) ).xyz * ase_objectScale );
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11618 = temp_output_7_0_g11618;
			#else
				float3 staticSwitch8_g11618 = temp_output_324_0_g11614;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11618 = staticSwitch8_g11618;
			#else
				float3 staticSwitch10_g11618 = temp_output_7_0_g11618;
			#endif
			float2 temp_output_1196_320_g11594 = (staticSwitch10_g11618).xz;
			half2 Motion_DirectionOS39_g11594 = temp_output_1196_320_g11594;
			half Input_Speed62_g11626 = _MotionSpeed_10;
			float temp_output_4_0_g11629 = Input_Speed62_g11626;
			float mulTime2_g11629 = _Time.y * temp_output_4_0_g11629;
			half3 localObjectPosition_unity_ObjectToWorld8_g11627 = ObjectPosition_unity_ObjectToWorld();
			float3 break9_g11627 = localObjectPosition_unity_ObjectToWorld8_g11627;
			half Motion_Variation284_g11626 = ( ( _MotionVariation_10 * v.color.a ) + ( break9_g11627.x + break9_g11627.z ) );
			float2 appendResult344_g11626 = (float2(ase_worldPos.x , ase_worldPos.z));
			float2 Motion_Scale287_g11626 = ( _MotionScale_10 * appendResult344_g11626 );
			half2 Sine_MinusOneToOne281_g11626 = sin( ( mulTime2_g11629 + Motion_Variation284_g11626 + Motion_Scale287_g11626 ) );
			float2 temp_cast_5 = (1.0).xx;
			half Input_Turbulence327_g11626 = Global_NoiseTex_R34_g11594;
			float2 lerpResult321_g11626 = lerp( Sine_MinusOneToOne281_g11626 , temp_cast_5 , Input_Turbulence327_g11626);
			half2 Motion_Interaction53_g11594 = ( _InteractionAmplitude * Motion_Max_Bending1133_g11594 * temp_output_1196_320_g11594 );
			half Motion_InteractionMask66_g11594 = break322_g11614.z;
			float2 lerpResult109_g11594 = lerp( ( ( _MotionAmplitude_10 * Motion_Max_Bending1133_g11594 ) * Global_Amplitude_136_g11594 * Global_NoiseTex_R34_g11594 * Motion_DirectionOS39_g11594 * lerpResult321_g11626 ) , Motion_Interaction53_g11594 , Motion_InteractionMask66_g11594);
			half Mesh_Motion_182_g11594 = break17_g11630.x;
			float2 break143_g11594 = ( lerpResult109_g11594 * Mesh_Motion_182_g11594 * Motion_UseMain56_g11594 );
			half Motion_Z190_g11594 = break143_g11594.y;
			half Angle44_g11641 = Motion_Z190_g11594;
			half3 VertexPos40_g11659 = ( VertexPosRotationAxis50_g11641 + ( VertexPosOtherAxis82_g11641 * cos( Angle44_g11641 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g11641 ) * sin( Angle44_g11641 ) ) );
			float3 appendResult74_g11659 = (float3(0.0 , 0.0 , VertexPos40_g11659.z));
			half3 VertexPosRotationAxis50_g11659 = appendResult74_g11659;
			float3 break84_g11659 = VertexPos40_g11659;
			float3 appendResult81_g11659 = (float3(break84_g11659.x , break84_g11659.y , 0.0));
			half3 VertexPosOtherAxis82_g11659 = appendResult81_g11659;
			half Motion_X216_g11594 = break143_g11594.x;
			half Angle44_g11659 = -Motion_X216_g11594;
			float3 temp_output_261_19_g11594 = ( VertexPosRotationAxis50_g11659 + ( VertexPosOtherAxis82_g11659 * cos( Angle44_g11659 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g11659 ) * sin( Angle44_g11659 ) ) );
			half Motion_Scale321_g11656 = ( _MotionScale_32 * 10.0 );
			half Input_Speed62_g11656 = _MotionSpeed_32;
			float temp_output_4_0_g11657 = Input_Speed62_g11656;
			float mulTime2_g11657 = _Time.y * temp_output_4_0_g11657;
			float Motion_Variation330_g11656 = ( _MotionVariation_32 * v.color.a );
			half Input_Amplitude58_g11656 = ( _MotionAmplitude_32 * Bounds_Radius121_g11594 * 0.2 );
			float3 ase_vertexNormal = v.normal.xyz;
			half Global_NoiseTex_A139_g11594 = break142_g11622.a;
			half Mesh_Motion_3144_g11594 = break17_g11630.z;
			half Motion_UseLeaves162_g11594 = _UseMotion_Leaves;
			half3 Motion_Flutter263_g11594 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Motion_Scale321_g11656 ) + mulTime2_g11657 + Motion_Variation330_g11656 ) ) * Input_Amplitude58_g11656 * ase_vertexNormal ) * ( Global_NoiseTex_R34_g11594 + Global_NoiseTex_A139_g11594 ) * Mesh_Motion_3144_g11594 * Motion_UseLeaves162_g11594 );
			half Global_NoiseTex_B132_g11594 = break142_g11622.b;
			half Input_Speed62_g11631 = -MotionSpeed265_g11594;
			float temp_output_4_0_g11634 = Input_Speed62_g11631;
			float mulTime2_g11634 = _Time.y * temp_output_4_0_g11634;
			half3 localObjectPosition_unity_ObjectToWorld8_g11635 = ObjectPosition_unity_ObjectToWorld();
			float3 break9_g11635 = localObjectPosition_unity_ObjectToWorld8_g11635;
			float temp_output_218_0_g11631 = -MotionVariation264_g11594;
			float Motion_Variation284_g11631 = ( temp_output_218_0_g11631 * v.color.a );
			float Motion_Scale287_g11631 = ( MotionScale262_g11594 * ase_worldPos.x );
			half Motion_Vertical223_g11594 = ( ( _MotionVertical_20 * Bounds_Radius121_g11594 ) * Global_Amplitude_270_g11594 * Mesh_Motion_260_g11594 * Motion_UseMain56_g11594 * ( Global_NoiseTex_R34_g11594 + Global_NoiseTex_B132_g11594 ) * sin( ( mulTime2_g11634 + ( break9_g11635.x + break9_g11635.z ) + Motion_Variation284_g11631 + Motion_Scale287_g11631 ) ) );
			float3 appendResult282_g11594 = (float3(0.0 , Motion_Vertical223_g11594 , 0.0));
			half3 Vertex_Motion_Standard324_g11594 = ( temp_output_261_19_g11594 + Motion_Flutter263_g11594 + appendResult282_g11594 );
			half3 Vertex_Motion833_g11594 = Vertex_Motion_Standard324_g11594;
			float3 temp_output_689_0_g11594 = ( lerpResult348_g11594 * Vertex_Motion833_g11594 );
			half3 localObjectPosition_unity_ObjectToWorld8_g11620 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11621 = localObjectPosition_unity_ObjectToWorld8_g11620;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11621 = temp_output_7_0_g11621;
			#else
				float3 staticSwitch8_g11621 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11621 = staticSwitch8_g11621;
			#else
				float3 staticSwitch10_g11621 = temp_output_7_0_g11621;
			#endif
			float4 tex2DNode7_g11619 = tex2Dlod( TVE_ExtrasTex, float4( ( (TVE_ExtrasCoord).xy + ( TVE_ExtrasCoord.z * (staticSwitch10_g11621).xz ) ), 0, 0.0) );
			float3 linearToGamma9_g11619 = LinearToGammaSpace( tex2DNode7_g11619.rgb );
			#ifdef UNITY_COLORSPACE_GAMMA
				float3 staticSwitch13_g11619 = (tex2DNode7_g11619).rgb;
			#else
				float3 staticSwitch13_g11619 = linearToGamma9_g11619;
			#endif
			float3 break20_g11619 = staticSwitch13_g11619;
			half Global_ExtrasTex_G305_g11594 = break20_g11619.y;
			half Global_Size693_g11594 = _GlobalSize;
			float lerpResult346_g11594 = lerp( 1.0 , Global_ExtrasTex_G305_g11594 , Global_Size693_g11594);
			half3 Vertex_Position601_g11594 = ( temp_output_689_0_g11594 * lerpResult346_g11594 );
			float3 temp_output_7_0_g11658 = Vertex_Position601_g11594;
			float3 appendResult1108_g11594 = (float3(Motion_X216_g11594 , Motion_Vertical223_g11594 , Motion_Z190_g11594));
			float3 appendResult1120_g11594 = (float3(Motion_Y138_g11594 , 0.0 , -Motion_Y138_g11594));
			half3 Vertex_Motion_Standard_Batched1110_g11594 = ( ase_vertex3Pos + appendResult1108_g11594 + appendResult1120_g11594 + Motion_Flutter263_g11594 );
			half3 Vertex_Motion_Batched1118_g11594 = Vertex_Motion_Standard_Batched1110_g11594;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11658 = temp_output_7_0_g11658;
			#else
				float3 staticSwitch8_g11658 = Vertex_Motion_Batched1118_g11594;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11658 = staticSwitch8_g11658;
			#else
				float3 staticSwitch10_g11658 = temp_output_7_0_g11658;
			#endif
			half3 Final_Vertex890_g11594 = ( staticSwitch10_g11658 + ( _IsStandardPipeline * 0.0 ) );
			v.vertex.xyz = Final_Vertex890_g11594;
		}

		inline half4 LightingStandardCustom(SurfaceOutputStandardCustom s, half3 viewDir, UnityGI gi )
		{
			half3 transmission = max(0 , -dot(s.Normal, gi.light.dir)) * gi.light.color * s.Transmission;
			half4 d = half4(s.Albedo * transmission , 0);

			SurfaceOutputStandard r;
			r.Albedo = s.Albedo;
			r.Normal = s.Normal;
			r.Emission = s.Emission;
			r.Metallic = s.Metallic;
			r.Smoothness = s.Smoothness;
			r.Occlusion = s.Occlusion;
			r.Alpha = s.Alpha;
			return LightingStandard (r, viewDir, gi) + d;
		}

		inline void LightingStandardCustom_GI(SurfaceOutputStandardCustom s, UnityGIInput data, inout UnityGI gi )
		{
			#if defined(UNITY_PASS_DEFERRED) && UNITY_ENABLE_REFLECTION_BUFFERS
				gi = UnityGlobalIllumination(data, s.Occlusion, s.Normal);
			#else
				UNITY_GLOSSY_ENV_FROM_SURFACE( g, s, data );
				gi = UnityGlobalIllumination( data, s.Occlusion, s.Normal, g );
			#endif
		}

		void surf( Input i , inout SurfaceOutputStandardCustom o )
		{
			half2 Main_UVs15_g11594 = ( ( i.uv_texcoord * (_MainUVs).xy ) + (_MainUVs).zw );
			half3 Main_Normal137_g11594 = UnpackScaleNormal( tex2D( _MainNormalTex, Main_UVs15_g11594 ), _MainNormalValue );
			float3 temp_output_13_0_g11654 = Main_Normal137_g11594;
			float3 switchResult12_g11654 = (((i.ASEVFace>0)?(temp_output_13_0_g11654):(( temp_output_13_0_g11654 * __normalsoptions ))));
			half3 Blend_Normal312_g11594 = switchResult12_g11654;
			half3 localObjectPosition_unity_ObjectToWorld8_g11650 = ObjectPosition_unity_ObjectToWorld();
			float2 temp_output_38_0_g11648 = ( ( i.uv_texcoord + (localObjectPosition_unity_ObjectToWorld8_g11650).xz ) * TVE_OverlayUVTilling * _OverlayUVTilling );
			float3 temp_output_13_0_g11649 = UnpackScaleNormal( tex2D( TVE_OverlayNormalTex, temp_output_38_0_g11648 ), TVE_OverlayNormalValue );
			float3 switchResult12_g11649 = (((i.ASEVFace>0)?(temp_output_13_0_g11649):(( temp_output_13_0_g11649 * __normalsoptions ))));
			half3 Global_OverlayNormal313_g11594 = switchResult12_g11649;
			half3 Global_OverlayDirection159_g11594 = (TVE_OverlayDirection).xyz;
			half3 Blend_NormalRaw1051_g11594 = Main_Normal137_g11594;
			float3 switchResult1063_g11594 = (((i.ASEVFace>0)?(Blend_NormalRaw1051_g11594):(( Blend_NormalRaw1051_g11594 * half3(-1,-1,-1) ))));
			float dotResult218_g11594 = dot( Global_OverlayDirection159_g11594 , (WorldNormalVector( i , switchResult1063_g11594 )) );
			half Mesh_Variation16_g11594 = i.vertexColor.a;
			float lerpResult1065_g11594 = lerp( 1.0 , Mesh_Variation16_g11594 , _OverlayVariation);
			half Global_OverlayIntensity154_g11594 = TVE_OverlayIntensity;
			half3 localObjectPosition_unity_ObjectToWorld8_g11620 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11621 = localObjectPosition_unity_ObjectToWorld8_g11620;
			float3 ase_worldPos = i.worldPos;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11621 = temp_output_7_0_g11621;
			#else
				float3 staticSwitch8_g11621 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11621 = staticSwitch8_g11621;
			#else
				float3 staticSwitch10_g11621 = temp_output_7_0_g11621;
			#endif
			float4 tex2DNode7_g11619 = tex2D( TVE_ExtrasTex, ( (TVE_ExtrasCoord).xy + ( TVE_ExtrasCoord.z * (staticSwitch10_g11621).xz ) ) );
			float3 linearToGamma9_g11619 = LinearToGammaSpace( tex2DNode7_g11619.rgb );
			#ifdef UNITY_COLORSPACE_GAMMA
				float3 staticSwitch13_g11619 = (tex2DNode7_g11619).rgb;
			#else
				float3 staticSwitch13_g11619 = linearToGamma9_g11619;
			#endif
			float3 break20_g11619 = staticSwitch13_g11619;
			half Global_ExtrasTex_B156_g11594 = break20_g11619.z;
			float temp_output_1025_0_g11594 = ( Global_OverlayIntensity154_g11594 * _GlobalOverlay * Global_ExtrasTex_B156_g11594 );
			half Mask_Overlay269_g11594 = saturate( ( saturate( dotResult218_g11594 ) - ( 1.0 - ( lerpResult1065_g11594 * temp_output_1025_0_g11594 ) ) ) );
			float3 lerpResult349_g11594 = lerp( Blend_Normal312_g11594 , Global_OverlayNormal313_g11594 , Mask_Overlay269_g11594);
			half3 Final_Normal366_g11594 = lerpResult349_g11594;
			o.Normal = Final_Normal366_g11594;
			float4 lerpResult26_g11594 = lerp( _MainColorVariation , _MainColor , Mesh_Variation16_g11594);
			float4 tex2DNode29_g11594 = tex2D( _MainAlbedoTex, Main_UVs15_g11594 );
			float4 temp_output_51_0_g11594 = ( lerpResult26_g11594 * tex2DNode29_g11594 );
			half3 Main_AlbedoRaw99_g11594 = (temp_output_51_0_g11594).rgb;
			float3 temp_cast_2 = (1.0).xxx;
			half3 localObjectPosition_unity_ObjectToWorld8_g11644 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11645 = localObjectPosition_unity_ObjectToWorld8_g11644;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11645 = temp_output_7_0_g11645;
			#else
				float3 staticSwitch8_g11645 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11645 = staticSwitch8_g11645;
			#else
				float3 staticSwitch10_g11645 = temp_output_7_0_g11645;
			#endif
			#ifdef UNITY_COLORSPACE_GAMMA
				float staticSwitch1_g11643 = 2.0;
			#else
				float staticSwitch1_g11643 = 4.594794;
			#endif
			float3 lerpResult108_g11594 = lerp( temp_cast_2 , ( (tex2D( TVE_ColorsTex, ( (TVE_ColorsCoord).xy + ( TVE_ColorsCoord.z * (staticSwitch10_g11645).xz ) ) )).rgb * staticSwitch1_g11643 ) , _GlobalColors);
			half3 Main_AlbedoColored863_g11594 = ( Main_AlbedoRaw99_g11594 * lerpResult108_g11594 );
			half3 Main_Albedo149_g11594 = Main_AlbedoColored863_g11594;
			half3 Blend_Albedo265_g11594 = Main_Albedo149_g11594;
			float4 tex2DNode30_g11648 = tex2D( TVE_OverlayAlbedoTex, temp_output_38_0_g11648 );
			half3 Global_OverlayAlbedo277_g11594 = ( (TVE_OverlayColor).rgb * (tex2DNode30_g11648).rgb );
			float3 lerpResult336_g11594 = lerp( Blend_Albedo265_g11594 , Global_OverlayAlbedo277_g11594 , Mask_Overlay269_g11594);
			half3 Final_Albedo359_g11594 = lerpResult336_g11594;
			half Main_Alpha316_g11594 = (temp_output_51_0_g11594).a;
			float lerpResult354_g11594 = lerp( 1.0 , Main_Alpha316_g11594 , __premul);
			half Final_Premultiply355_g11594 = lerpResult354_g11594;
			o.Albedo = ( Final_Albedo359_g11594 * Final_Premultiply355_g11594 );
			float4 tex2DNode35_g11594 = tex2D( _MainMaskTex, Main_UVs15_g11594 );
			half Main_Metallic237_g11594 = ( tex2DNode35_g11594.r * _MainMetallicValue );
			half Blend_Metallic306_g11594 = Main_Metallic237_g11594;
			float lerpResult342_g11594 = lerp( Blend_Metallic306_g11594 , 0.0 , Mask_Overlay269_g11594);
			half Final_Metallic367_g11594 = lerpResult342_g11594;
			o.Metallic = Final_Metallic367_g11594;
			half Main_Smoothness227_g11594 = ( tex2DNode35_g11594.a * _MainSmoothnessValue );
			half Blend_Smoothness314_g11594 = Main_Smoothness227_g11594;
			half Global_OverlaySmoothness311_g11594 = TVE_OverlaySmoothness;
			float lerpResult343_g11594 = lerp( Blend_Smoothness314_g11594 , Global_OverlaySmoothness311_g11594 , Mask_Overlay269_g11594);
			half Final_Smoothness371_g11594 = lerpResult343_g11594;
			half Global_Wetness1016_g11594 = ( TVE_Wetness * _GlobalWetness );
			half Global_ExtrasTex_A1033_g11594 = tex2DNode7_g11619.a;
			float lerpResult1037_g11594 = lerp( Final_Smoothness371_g11594 , saturate( ( Final_Smoothness371_g11594 + Global_Wetness1016_g11594 ) ) , Global_ExtrasTex_A1033_g11594);
			o.Smoothness = lerpResult1037_g11594;
			half Mesh_Occlusion318_g11594 = i.vertexColor.g;
			float saferPower1201_g11594 = max( Mesh_Occlusion318_g11594 , 0.0001 );
			half Vertex_Occlusion648_g11594 = pow( saferPower1201_g11594 , _ObjectOcclusionValue );
			float lerpResult240_g11594 = lerp( 1.0 , tex2DNode35_g11594.g , _MainOcclusionValue);
			half Main_Occlusion247_g11594 = lerpResult240_g11594;
			half Blend_Occlusion323_g11594 = Main_Occlusion247_g11594;
			o.Occlusion = ( Vertex_Occlusion648_g11594 * Blend_Occlusion323_g11594 );
			half Main_Mask57_g11594 = tex2DNode35_g11594.b;
			float temp_output_7_0_g11637 = _SubsurfaceMinValue;
			half AutoRegister_MaterialShadingSpace1208_g11594 = _MaterialShadingSpaceDrawer;
			float temp_output_1345_0_g11594 = saturate( ( ( Main_Mask57_g11594 - temp_output_7_0_g11637 ) / ( ( _SubsurfaceMaxValue + AutoRegister_MaterialShadingSpace1208_g11594 ) - temp_output_7_0_g11637 ) ) );
			half3 Subsurface_Color884_g11594 = ( (_SubsurfaceColor).rgb * ( temp_output_1345_0_g11594 * _SubsurfaceValue ) );
			half3 Subsurface_StandardPipeline1262_g11594 = Subsurface_Color884_g11594;
			o.Transmission = Subsurface_StandardPipeline1262_g11594;
			o.Alpha = Main_Alpha316_g11594;
			half Main_AlphaRaw1203_g11594 = tex2DNode29_g11594.a;
			half Global_ExtrasTex_R174_g11594 = break20_g11619.x;
			float lerpResult293_g11594 = lerp( 1.0 , ceil( ( pow( Mesh_Variation16_g11594 , 2.0 ) - ( 1.0 - Global_ExtrasTex_R174_g11594 ) ) ) , _GlobalLeaves);
			half Mask_Leaves315_g11594 = lerpResult293_g11594;
			half Alpha5_g11660 = ( Main_AlphaRaw1203_g11594 * Mask_Leaves315_g11594 );
			#ifdef _ALPHATEST_ON
				float staticSwitch2_g11660 = Alpha5_g11660;
			#else
				float staticSwitch2_g11660 = 1.0;
			#endif
			half Final_Clip914_g11594 = staticSwitch2_g11660;
			clip( Final_Clip914_g11594 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "TVEShaderCoreGUI"
}
/*ASEBEGIN
Version=17802
1927;1;1906;1020;2550.442;1049.836;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;358;-1890,-512;Inherit;False;Property;_SubsurfaceMessageUniversal;!!! Subsurface Message Universal !!!;3;0;Create;False;0;0;False;1;StyledMessage(Info, In Universal Render Pipeline the Subsurface only work for the Directional light. Other light types are not supported., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-2176,-768;Half;False;Property;_Banner;Banner;0;0;Create;True;0;0;True;1;StyledBanner(Leaf Advanced Lit);0;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-1888,-640;Half;False;Property;__src;__src;156;1;[HideInInspector];Fetch;False;0;0;True;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;271;-1584,-768;Half;False;Property;_IsLitShader;_IsLitShader;153;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;357;-2178,-512;Inherit;False;Property;_SubsurfaceMessageStandard;!!! Subsurface Message !!!;1;0;Create;False;0;0;False;1;StyledMessage(Info, Subsurface shading requires Directional light and Ambient light higher than 0 to work. Other light types are not supported. Switch to Advanced Lit shaders for high quality translucency., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1632,-640;Half;False;Property;__zw;__zw;158;1;[HideInInspector];Fetch;False;2;Opaque;0;Transparent;1;0;True;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-2016,-640;Half;False;Property;__cull;__cull;155;2;[HideInInspector];[Enum];Fetch;False;3;Both;0;Back;1;Front;2;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;168;-2032,-768;Half;False;Property;_IsLeafShader;_IsLeafShader;151;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-1760,-640;Half;False;Property;__dst;__dst;157;1;[HideInInspector];Fetch;False;2;Opaque;0;Transparent;1;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-2176,-640;Half;False;Property;_Cutoff;_Cutoff;154;1;[HideInInspector];Fetch;False;4;Alpha;0;Premultiply;1;Additive;2;Multiply;3;0;True;0;0.5;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;81;-1824,-768;Half;False;Property;_IsAdvancedShader;_IsAdvancedShader;152;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;361;-1280,-512;Inherit;False;Property;_SubsurfaceMessageDeferred;!!! Subsurface Message Deferred !!!;2;0;Create;False;0;0;True;1;StyledMessage(Info, In Standard Render Pipeline the Leaf Advanced Lit shader is always rendered in Forward regardless of the Camera Rendering Path mode., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;359;-1586,-512;Inherit;False;Property;_SubsurfaceMessageHD;!!! Subsurface Message HD !!!;4;0;Create;False;0;0;False;1;StyledMessage(Info, Diffusion Profiles are not directly supported. You need to create a HDRP Lit material and assign a Diffusion Profile then drag it to the Subsurface Diffusion Material slot., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;369;-2176,-128;Inherit;False;Base;5;;11594;856f7164d1c579d43a5cf4968a75ca43;20,1034,1,860,1,1000,1,995,1,847,2,1114,2,850,1,853,0,1271,1,1298,1,1300,1,866,0,844,2,883,0,879,0,881,0,878,0,888,0,893,1,916,0;0;16;FLOAT3;0;FLOAT3;528;FLOAT;529;FLOAT;530;FLOAT;531;FLOAT;1235;FLOAT3;1230;FLOAT;1290;FLOAT;721;FLOAT;532;FLOAT;653;FLOAT;892;FLOAT3;534;FLOAT4;535;FLOAT;629;FLOAT;722
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-1376,-128;Float;False;True;-1;2;TVEShaderCoreGUI;0;0;Standard;BOXOPHOBIC/The Vegetation Engine/Vegetation/Leaf Advanced Lit;False;False;False;False;False;True;False;True;False;False;False;False;True;True;True;False;True;False;False;False;True;Back;0;True;17;0;False;-1;True;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Opaque;;Geometry;ForwardOnly;11;d3d11;glcore;gles3;metal;vulkan;xbox360;xboxone;ps4;psp2;n3ds;wiiu;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;1;0;True;20;0;True;7;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Absolute;0;;-1;-1;-1;-1;0;False;0;0;True;10;-1;0;True;21;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;37;-2176,-896;Inherit;False;1023.392;100;Internal;0;;1,0.252,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;33;-2176,-256;Inherit;False;1024.392;100;Final;0;;0.3439424,0.5960785,0,1;0;0
WireConnection;0;0;369;0
WireConnection;0;1;369;528
WireConnection;0;3;369;529
WireConnection;0;4;369;530
WireConnection;0;5;369;531
WireConnection;0;6;369;1230
WireConnection;0;9;369;532
WireConnection;0;10;369;653
WireConnection;0;11;369;534
ASEEND*/
//CHKSM=08F2DDCCB41B4E2D7AC9ED89677B370B2F2ED230