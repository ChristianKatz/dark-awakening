// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BOXOPHOBIC/The Vegetation Engine/Vegetation/Leaf Standard Lit"
{
	Properties
	{
		[StyledBanner(Leaf Standard Lit)]_Banner("Banner", Float) = 0
		[StyledMessage(Info, Subsurface shading requires Directional light and Ambient light higher than 0 to work. Other light types are not supported. Switch to Advanced Lit shaders for high quality translucency., 5, 5 )]_SubsurfaceMessageStandard1("!!! Subsurface Message !!!", Float) = 0
		[HideInInspector]_IsVersion("_IsVersion", Float) = 110
		[HideInInspector]_IsStandardPipeline("_IsStandardPipeline", Float) = 1
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector][Toggle]_IsUpdated("_IsUpdated", Float) = 0
		[HideInInspector]_VariationMode("_VariationMode", Float) = -1
		[HideInInspector]_MaxBoundsInfo("_MaxBoundsInfo", Vector) = (1,1,1,1)
		[StyledCategory(Render Settings)]_RenderingCat("[ Rendering Cat ]", Float) = 0
		[Enum(Opaque,0,Transparent,1)]__surface("Render Mode", Float) = 0
		[Enum(Both,0,Back,1,Front,2)]__cull("Render Faces", Float) = 0
		[Enum(Flip,0,Mirror,1,None,2)]__normals("Render Normals", Float) = 0
		[StyledInteractive(__surface, 1)]_RenderMode_TransparentInteractive("# RenderMode_TransparentInteractive", Float) = 0
		[Enum(Alpha,0,Premultiply,1)]__blend("Render Blending", Float) = 0
		[Enum(Off,0,On,1)]__zw("Render ZWrite", Float) = 1
		[StyledInteractive(ON)]_RenderMode_ResetInteractive("# RenderMode_ResetInteractive", Float) = 0
		[Toggle][Space(10)]__clip("Alpha Clipping", Float) = 0
		[StyledInteractive(__clip, 1)]_AlphaClipInteractive("# AlphaClipInteractive", Float) = 0
		_Cutoff("Alpha Treshold", Range( 0 , 1)) = 0.5
		[HideInInspector]__cliptreshold("__cliptreshold", Float) = 0
		[StyledCategory(Global Settings)]_GlobalSettingsCat("[ Global Settings Cat ]", Float) = 0
		_GlobalColors("Global Colors", Range( 0 , 1)) = 1
		[HideInInspector]_GlobalOverlayMode("Global Overlay Mode", Float) = 1
		_GlobalOverlay("Global Overlay", Range( 0 , 1)) = 1
		_GlobalWetness("Global Wetness", Range( 0 , 1)) = 1
		_GlobalLeaves("Global Leaves", Range( 0 , 1)) = 1
		_GlobalSize("Global Size", Range( 0 , 1)) = 1
		_GlobalSizeFade("Global Size Fade", Range( 0 , 1)) = 1
		[StyledCategory(Material Shading)]_MaterialShadingCat("[ Material Shading Cat ]", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Subsurface Mask. Use the Min Max sliders to adjust or invert the subsurface effect., _IsLeafShader, 1, 5, 10 )]_MessageSubsurfaceMask("!!! Message Leaf Subsurface Mask !!!", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Subsurface Mask. Use the Min Max sliders to adjust or invert the subsurface effect., _IsCrossShader, 1, 5, 10 )]_MessageCrossSubsurfaceMask("!!! Message Cross Subsurface Mask !!!", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Height Map. Use the Min Max values to adjust or invert the height., _IsTessellationShader, 1, 5, 10 )]_MessageHeightMap("!!! Message Height Map !!!", Float) = 0
		[HDR]_SubsurfaceColor("Subsurface Color", Color) = (0.3315085,0.490566,0,1)
		_SubsurfaceValue("Subsurface Intensity", Range( 0 , 1)) = 1
		_SubsurfaceMinValue("Subsurface Min", Range( 0 , 1)) = 0
		_SubsurfaceMaxValue("Subsurface Max", Range( 0 , 1)) = 1
		[StyledSpace(10)]_MaterialShadingSpaceDrawer("# Material Shading Space", Float) = 0
		_ObjectOcclusionValue("Object Occlusion", Range( 0 , 10)) = 0
		[Space(10)]_OverlayUVTilling("Overlay Tilling", Range( 0 , 10)) = 1
		_OverlayVariation("Overlay Variation", Range( 0 , 1)) = 0
		[StyledCategory(Main Shading)]_MainShadingCat("[ Main Shading Cat ]", Float) = 0
		[Enum(Constant,0,Variation,1)]_MainColorMode("Main Color Mode", Float) = 1
		[Space(10)]_MainColor("Main Color One", Color) = (1,1,1,1)
		_MainColorVariation("Main Color Two", Color) = (1,1,1,1)
		[NoScaleOffset]_MainAlbedoTex("Main Albedo", 2D) = "white" {}
		[NoScaleOffset]_MainNormalTex("Main Normal", 2D) = "bump" {}
		[NoScaleOffset]_MainMaskTex("Main Mask", 2D) = "white" {}
		[Space(10)]_MainUVs("Main UVs", Vector) = (1,1,0,0)
		_MainNormalValue("Main Normal", Range( -8 , 8)) = 1
		_MainMetallicValue("Main Metallic (R)", Range( 0 , 1)) = 0
		_MainOcclusionValue("Main Occlusion (G)", Range( 0 , 1)) = 1
		_MainSmoothnessValue("Main Smoothness (A)", Range( 0 , 1)) = 1
		[StyledCategory(Motion Settings)]_MotionCat("[ Motion Cat ]", Float) = 0
		[HideInInspector][Enum(Off,0,Procedural,1,Hierarchical,2)]_MotionMode("Motion Mode", Float) = 0
		[HideInInspector][Enum(Object Pivot,0,Element Pivot,1)]_PivotMode("Pivot Mode", Float) = 0
		[Toggle]_UseMotion_Main("Motion Main", Float) = 1
		[Toggle]_UseMotion_Leaves("Motion Leaves", Float) = 1
		[HideInInspector][Space(10)]_MotionAmplitude_10("Primary Bending", Float) = 0
		[HideInInspector][IntRange]_MotionSpeed_10("Primary Speed", Float) = 2
		[HideInInspector]_MotionScale_10("Primary Elasticity", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector]_MotionVariation_10("Primary Variation", Float) = 0
		[HideInInspector][Space(10)]_MotionAmplitude_20("Secundary Rolling", Float) = 0
		[HideInInspector]_MotionVertical_20("Secundary Vertical", Float) = 0
		[HideInInspector][IntRange]_MotionSpeed_20("Secundary Speed", Float) = 5
		[HideInInspector]_MotionScale_20("Secundary Elasticity", Float) = 0
		[HideInInspector]_MotionVariation_20("Secundary Variation", Range( 0 , 5)) = 0
		[HideInInspector][Space(10)]_MotionAmplitude_32("Flutter Amplitude", Float) = 0
		[HideInInspector][IntRange]_MotionSpeed_32("Flutter Speed", Float) = 15
		[HideInInspector]_MotionScale_32("Flutter Elasticity", Float) = 100
		[HideInInspector]_MotionVariation_32("Flutter Variation", Float) = 100
		[HideInInspector][Space(10)]_InteractionAmplitude("Interaction Bending", Float) = 0
		[StyledCategory(Advanced)]_AdvancedCat("[ Advanced Cat]", Float) = 0
		[IntRange]__priority("Priority", Range( -50 , 50)) = 0
		[HideInInspector]__premul("__premul", Float) = 0
		[HideInInspector]_Color("_Color", Color) = (0,0,0,0)
		[HideInInspector]_MainTex("_MainTex", 2D) = "white" {}
		[HideInInspector]_VertexOcclusion("_VertexOcclusion", Float) = 0
		[HideInInspector]_MainMaskValue("_MainMaskValue", Float) = 0
		[HideInInspector][Enum(Translucency,0,Thickness,1)]_SubsurfaceMode("_SubsurfaceMode", Float) = 0
		[HideInInspector]_ObjectThicknessValue("_ObjectThicknessValue", Float) = 0
		[HideInInspector]__normalsoptions("__normalsoptions", Vector) = (1,1,1,0)
		[HideInInspector]_IsLeafShader("_IsLeafShader", Float) = 1
		[HideInInspector]_IsStandardShader("_IsStandardShader", Float) = 1
		[HideInInspector]_IsLitShader("_IsLitShader", Float) = 1
		[HideInInspector]_Cutoff("_Cutoff", Float) = 0.5
		[HideInInspector][Enum(Both,0,Back,1,Front,2)]__cull("__cull", Float) = 0
		[HideInInspector]__src("__src", Float) = 1
		[HideInInspector]__dst("__dst", Float) = 0
		[HideInInspector]__zw("__zw", Float) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" "DisableBatching" = "True" }
		Cull [__cull]
		ZWrite [__zw]
		Offset  0 , 0
		Blend [__src] [__dst]
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma shader_feature _ALPHATEST_ON
		  
		//FEATURES BEGIN
		//FEATURES END
		    
		#define TVE_DISABLE_UNSAFE_BATCHING
		#pragma exclude_renderers d3d9 d3d11_9x gles 
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows novertexlights nodynlightmap dithercrossfade vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			half ASEVFace : VFACE;
			float3 worldNormal;
			INTERNAL_DATA
			float4 vertexColor : COLOR;
		};

		uniform half _IsStandardShader;
		uniform half _IsLeafShader;
		uniform float _SubsurfaceMessageStandard1;
		uniform half _IsLitShader;
		uniform half _Banner;
		uniform float4 _MaxBoundsInfo;
		uniform half _AdvancedCat;
		uniform half __surface;
		uniform half _PivotMode;
		uniform half __zw;
		uniform half _GlobalSettingsCat;
		uniform half _IsTVEShader;
		uniform half _IsUpdated;
		uniform half _MaterialShadingCat;
		uniform half __blend;
		uniform half __src;
		uniform half _Cutoff;
		uniform half __dst;
		uniform half _RenderMode_ResetInteractive;
		uniform half _MessageHeightMap;
		uniform half _MessageCrossSubsurfaceMask;
		uniform float _SubsurfaceMode;
		uniform half _MainMaskValue;
		uniform half _VertexOcclusion;
		uniform half _MainColorMode;
		uniform half _RenderMode_TransparentInteractive;
		uniform half _GlobalOverlayMode;
		uniform half __cull;
		uniform half __clip;
		uniform sampler2D _MainTex;
		uniform half __normals;
		uniform half _VariationMode;
		uniform float4 _Color;
		uniform half _RenderingCat;
		uniform half _IsVersion;
		uniform half __cliptreshold;
		uniform half __priority;
		uniform half _MotionMode;
		uniform half _MainShadingCat;
		uniform half _AlphaClipInteractive;
		uniform half _ObjectThicknessValue;
		uniform half _MessageSubsurfaceMask;
		uniform half TVE_SizeFadeEnd;
		uniform half TVE_SizeFadeStart;
		uniform half _GlobalSizeFade;
		uniform half _MotionAmplitude_20;
		uniform half TVE_Amplitude2;
		uniform sampler2D TVE_NoiseTex;
		uniform half TVE_NoiseSpeed;
		uniform half TVE_NoiseSize;
		uniform half TVE_NoiseContrast;
		uniform half _UseMotion_Main;
		uniform half _MotionCat;
		uniform half _MotionSpeed_20;
		uniform half _MotionVariation_20;
		uniform half _MotionScale_20;
		uniform half _MotionAmplitude_10;
		uniform half TVE_Amplitude1;
		uniform sampler2D TVE_MotionTex;
		uniform half4 TVE_MotionCoord;
		uniform half _MotionSpeed_10;
		uniform half _MotionVariation_10;
		uniform half _MotionScale_10;
		uniform float _InteractionAmplitude;
		uniform half _MotionScale_32;
		uniform half _MotionSpeed_32;
		uniform half _MotionVariation_32;
		uniform half _MotionAmplitude_32;
		uniform half _UseMotion_Leaves;
		uniform half _MotionVertical_20;
		uniform sampler2D TVE_ExtrasTex;
		uniform half4 TVE_ExtrasCoord;
		uniform half _GlobalSize;
		uniform half _IsStandardPipeline;
		uniform half _MainNormalValue;
		uniform sampler2D _MainNormalTex;
		uniform half4 _MainUVs;
		uniform half3 __normalsoptions;
		uniform half TVE_OverlayNormalValue;
		uniform sampler2D TVE_OverlayNormalTex;
		uniform half TVE_OverlayUVTilling;
		uniform half _OverlayUVTilling;
		uniform half4 TVE_OverlayDirection;
		uniform half _OverlayVariation;
		uniform half TVE_OverlayIntensity;
		uniform half _GlobalOverlay;
		uniform half4 _MainColorVariation;
		uniform half4 _MainColor;
		uniform sampler2D _MainAlbedoTex;
		uniform sampler2D TVE_ColorsTex;
		uniform half4 TVE_ColorsCoord;
		uniform half _GlobalColors;
		uniform half4 _SubsurfaceColor;
		uniform sampler2D _MainMaskTex;
		uniform half _SubsurfaceMinValue;
		uniform half _SubsurfaceMaxValue;
		uniform half _MaterialShadingSpaceDrawer;
		uniform half _SubsurfaceValue;
		uniform half _ObjectOcclusionValue;
		uniform half4 TVE_OverlayColor;
		uniform sampler2D TVE_OverlayAlbedoTex;
		uniform half __premul;
		uniform half _MainMetallicValue;
		uniform half _MainSmoothnessValue;
		uniform half TVE_OverlaySmoothness;
		uniform float TVE_Wetness;
		uniform half _GlobalWetness;
		uniform half _MainOcclusionValue;
		uniform half _GlobalLeaves;


		half3 ObjectPosition_unity_ObjectToWorld(  )
		{
			return half3(unity_ObjectToWorld[0].w, unity_ObjectToWorld[1].w, unity_ObjectToWorld[2].w );
		}


		float3 DecodeFloatToVector3( int enc )
		{
			  float3 result ;
			  result.x = ((enc >> 0) & 0xFF) / 255.0f;
			  result.y = ((enc >> 8) & 0xFF) / 255.0;
			  result.z = ((enc >>16) & 0xFF) / 255.0;
			  //result.w = ((enc >> 32) & 0xFF) / 255.0;
			  return result;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			half3 localObjectPosition_unity_ObjectToWorld8_g11469 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_731_7_g1 = localObjectPosition_unity_ObjectToWorld8_g11469;
			half Global_SizeFadeEnd287_g1 = TVE_SizeFadeEnd;
			float temp_output_7_0_g11503 = Global_SizeFadeEnd287_g1;
			half Global_SizeFadeStart276_g1 = TVE_SizeFadeStart;
			half Global_SizeFade694_g1 = _GlobalSizeFade;
			float lerpResult348_g1 = lerp( 1.0 , saturate( ( ( distance( _WorldSpaceCameraPos , temp_output_731_7_g1 ) - temp_output_7_0_g11503 ) / ( Global_SizeFadeStart276_g1 - temp_output_7_0_g11503 ) ) ) , Global_SizeFade694_g1);
			float3 ase_vertex3Pos = v.vertex.xyz;
			half3 VertexPos40_g11516 = ase_vertex3Pos;
			float3 appendResult74_g11516 = (float3(0.0 , VertexPos40_g11516.y , 0.0));
			half3 VertexPosRotationAxis50_g11516 = appendResult74_g11516;
			float3 break84_g11516 = VertexPos40_g11516;
			float3 appendResult81_g11516 = (float3(break84_g11516.x , 0.0 , break84_g11516.z));
			half3 VertexPosOtherAxis82_g11516 = appendResult81_g11516;
			float temp_output_7_0_g11460 = UNITY_PI;
			half Bounds_Radius121_g1 = _MaxBoundsInfo.x;
			#ifdef UNITY_INSTANCING_ENABLED
				float staticSwitch8_g11460 = temp_output_7_0_g11460;
			#else
				float staticSwitch8_g11460 = Bounds_Radius121_g1;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float staticSwitch10_g11460 = staticSwitch8_g11460;
			#else
				float staticSwitch10_g11460 = temp_output_7_0_g11460;
			#endif
			half Motion_Max_Rolling1137_g1 = staticSwitch10_g11460;
			half Global_Amplitude_270_g1 = TVE_Amplitude2;
			int enc15_g11494 = (int)v.texcoord3.x;
			float3 localDecodeFloatToVector315_g11494 = DecodeFloatToVector3( enc15_g11494 );
			float3 break17_g11494 = localDecodeFloatToVector315_g11494;
			half Mesh_Motion_260_g1 = break17_g11494.y;
			float temp_output_4_0_g11489 = 1.0;
			float mulTime2_g11489 = _Time.y * temp_output_4_0_g11489;
			float2 temp_cast_1 = (TVE_NoiseSpeed).xx;
			half3 localObjectPosition_unity_ObjectToWorld8_g11488 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11487 = localObjectPosition_unity_ObjectToWorld8_g11488;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11487 = temp_output_7_0_g11487;
			#else
				float3 staticSwitch8_g11487 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11487 = staticSwitch8_g11487;
			#else
				float3 staticSwitch10_g11487 = temp_output_7_0_g11487;
			#endif
			float2 panner73_g11486 = ( mulTime2_g11489 * temp_cast_1 + ( (staticSwitch10_g11487).xz * TVE_NoiseSize ));
			float4 temp_cast_2 = (TVE_NoiseContrast).xxxx;
			float4 break142_g11486 = pow( abs( tex2Dlod( TVE_NoiseTex, float4( panner73_g11486, 0, 0.0) ) ) , temp_cast_2 );
			half Global_NoiseTex_R34_g1 = break142_g11486.r;
			half Global_NoiseTex_G38_g1 = break142_g11486.g;
			half Motion_UseMain56_g1 = ( _UseMotion_Main + ( _MotionCat * 0.0 ) );
			half MotionSpeed265_g1 = _MotionSpeed_20;
			half Input_Speed62_g11473 = MotionSpeed265_g1;
			float temp_output_4_0_g11476 = Input_Speed62_g11473;
			float mulTime2_g11476 = _Time.y * temp_output_4_0_g11476;
			half3 localObjectPosition_unity_ObjectToWorld8_g11477 = ObjectPosition_unity_ObjectToWorld();
			float3 break9_g11477 = localObjectPosition_unity_ObjectToWorld8_g11477;
			half MotionVariation264_g1 = _MotionVariation_20;
			float temp_output_218_0_g11473 = MotionVariation264_g1;
			float Motion_Variation284_g11473 = ( temp_output_218_0_g11473 * v.color.a );
			half MotionScale262_g1 = _MotionScale_20;
			float Motion_Scale287_g11473 = ( MotionScale262_g1 * ase_worldPos.x );
			half Motion_Y138_g1 = ( ( _MotionAmplitude_20 * Motion_Max_Rolling1137_g1 ) * Global_Amplitude_270_g1 * Mesh_Motion_260_g1 * ( Global_NoiseTex_R34_g1 + Global_NoiseTex_G38_g1 ) * Motion_UseMain56_g1 * sin( ( mulTime2_g11476 + ( break9_g11477.x + break9_g11477.z ) + Motion_Variation284_g11473 + Motion_Scale287_g11473 ) ) );
			half Angle44_g11516 = Motion_Y138_g1;
			float3 temp_output_188_19_g1 = ( VertexPosRotationAxis50_g11516 + ( VertexPosOtherAxis82_g11516 * cos( Angle44_g11516 ) ) + ( cross( float3(0,1,0) , VertexPosOtherAxis82_g11516 ) * sin( Angle44_g11516 ) ) );
			half3 VertexPos40_g11505 = temp_output_188_19_g1;
			float3 appendResult74_g11505 = (float3(VertexPos40_g11505.x , 0.0 , 0.0));
			half3 VertexPosRotationAxis50_g11505 = appendResult74_g11505;
			float3 break84_g11505 = VertexPos40_g11505;
			float3 appendResult81_g11505 = (float3(0.0 , break84_g11505.y , break84_g11505.z));
			half3 VertexPosOtherAxis82_g11505 = appendResult81_g11505;
			float temp_output_7_0_g11472 = UNITY_PI;
			half Bounds_Height374_g1 = _MaxBoundsInfo.y;
			#ifdef UNITY_INSTANCING_ENABLED
				float staticSwitch8_g11472 = temp_output_7_0_g11472;
			#else
				float staticSwitch8_g11472 = ( Bounds_Height374_g1 * UNITY_PI );
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float staticSwitch10_g11472 = staticSwitch8_g11472;
			#else
				float staticSwitch10_g11472 = temp_output_7_0_g11472;
			#endif
			half Motion_Max_Bending1133_g1 = staticSwitch10_g11472;
			half Global_Amplitude_136_g1 = TVE_Amplitude1;
			half3 localObjectPosition_unity_ObjectToWorld8_g11480 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11481 = localObjectPosition_unity_ObjectToWorld8_g11480;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11481 = temp_output_7_0_g11481;
			#else
				float3 staticSwitch8_g11481 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11481 = staticSwitch8_g11481;
			#else
				float3 staticSwitch10_g11481 = temp_output_7_0_g11481;
			#endif
			float3 temp_output_18_0_g11479 = (tex2Dlod( TVE_MotionTex, float4( ( (TVE_MotionCoord).xy + ( TVE_MotionCoord.z * (staticSwitch10_g11481).xz ) ), 0, 0.0) )).rgb;
			float3 linearToGamma28_g11479 = LinearToGammaSpace( temp_output_18_0_g11479 );
			float3 break322_g11478 = linearToGamma28_g11479;
			float3 appendResult323_g11478 = (float3(break322_g11478.x , 0.0 , break322_g11478.y));
			float3 temp_output_324_0_g11478 = (appendResult323_g11478*2.0 + -1.0);
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float3 temp_output_7_0_g11482 = ( mul( unity_WorldToObject, float4( temp_output_324_0_g11478 , 0.0 ) ).xyz * ase_objectScale );
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11482 = temp_output_7_0_g11482;
			#else
				float3 staticSwitch8_g11482 = temp_output_324_0_g11478;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11482 = staticSwitch8_g11482;
			#else
				float3 staticSwitch10_g11482 = temp_output_7_0_g11482;
			#endif
			float2 temp_output_1196_320_g1 = (staticSwitch10_g11482).xz;
			half2 Motion_DirectionOS39_g1 = temp_output_1196_320_g1;
			half Input_Speed62_g11490 = _MotionSpeed_10;
			float temp_output_4_0_g11493 = Input_Speed62_g11490;
			float mulTime2_g11493 = _Time.y * temp_output_4_0_g11493;
			half3 localObjectPosition_unity_ObjectToWorld8_g11491 = ObjectPosition_unity_ObjectToWorld();
			float3 break9_g11491 = localObjectPosition_unity_ObjectToWorld8_g11491;
			half Motion_Variation284_g11490 = ( ( _MotionVariation_10 * v.color.a ) + ( break9_g11491.x + break9_g11491.z ) );
			float2 appendResult344_g11490 = (float2(ase_worldPos.x , ase_worldPos.z));
			float2 Motion_Scale287_g11490 = ( _MotionScale_10 * appendResult344_g11490 );
			half2 Sine_MinusOneToOne281_g11490 = sin( ( mulTime2_g11493 + Motion_Variation284_g11490 + Motion_Scale287_g11490 ) );
			float2 temp_cast_5 = (1.0).xx;
			half Input_Turbulence327_g11490 = Global_NoiseTex_R34_g1;
			float2 lerpResult321_g11490 = lerp( Sine_MinusOneToOne281_g11490 , temp_cast_5 , Input_Turbulence327_g11490);
			half2 Motion_Interaction53_g1 = ( _InteractionAmplitude * Motion_Max_Bending1133_g1 * temp_output_1196_320_g1 );
			half Motion_InteractionMask66_g1 = break322_g11478.z;
			float2 lerpResult109_g1 = lerp( ( ( _MotionAmplitude_10 * Motion_Max_Bending1133_g1 ) * Global_Amplitude_136_g1 * Global_NoiseTex_R34_g1 * Motion_DirectionOS39_g1 * lerpResult321_g11490 ) , Motion_Interaction53_g1 , Motion_InteractionMask66_g1);
			half Mesh_Motion_182_g1 = break17_g11494.x;
			float2 break143_g1 = ( lerpResult109_g1 * Mesh_Motion_182_g1 * Motion_UseMain56_g1 );
			half Motion_Z190_g1 = break143_g1.y;
			half Angle44_g11505 = Motion_Z190_g1;
			half3 VertexPos40_g11523 = ( VertexPosRotationAxis50_g11505 + ( VertexPosOtherAxis82_g11505 * cos( Angle44_g11505 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g11505 ) * sin( Angle44_g11505 ) ) );
			float3 appendResult74_g11523 = (float3(0.0 , 0.0 , VertexPos40_g11523.z));
			half3 VertexPosRotationAxis50_g11523 = appendResult74_g11523;
			float3 break84_g11523 = VertexPos40_g11523;
			float3 appendResult81_g11523 = (float3(break84_g11523.x , break84_g11523.y , 0.0));
			half3 VertexPosOtherAxis82_g11523 = appendResult81_g11523;
			half Motion_X216_g1 = break143_g1.x;
			half Angle44_g11523 = -Motion_X216_g1;
			float3 temp_output_261_19_g1 = ( VertexPosRotationAxis50_g11523 + ( VertexPosOtherAxis82_g11523 * cos( Angle44_g11523 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g11523 ) * sin( Angle44_g11523 ) ) );
			half Motion_Scale321_g11520 = ( _MotionScale_32 * 10.0 );
			half Input_Speed62_g11520 = _MotionSpeed_32;
			float temp_output_4_0_g11521 = Input_Speed62_g11520;
			float mulTime2_g11521 = _Time.y * temp_output_4_0_g11521;
			float Motion_Variation330_g11520 = ( _MotionVariation_32 * v.color.a );
			half Input_Amplitude58_g11520 = ( _MotionAmplitude_32 * Bounds_Radius121_g1 * 0.2 );
			float3 ase_vertexNormal = v.normal.xyz;
			half Global_NoiseTex_A139_g1 = break142_g11486.a;
			half Mesh_Motion_3144_g1 = break17_g11494.z;
			half Motion_UseLeaves162_g1 = _UseMotion_Leaves;
			half3 Motion_Flutter263_g1 = ( ( sin( ( ( ( ase_worldPos.x + ase_worldPos.y + ase_worldPos.z ) * Motion_Scale321_g11520 ) + mulTime2_g11521 + Motion_Variation330_g11520 ) ) * Input_Amplitude58_g11520 * ase_vertexNormal ) * ( Global_NoiseTex_R34_g1 + Global_NoiseTex_A139_g1 ) * Mesh_Motion_3144_g1 * Motion_UseLeaves162_g1 );
			half Global_NoiseTex_B132_g1 = break142_g11486.b;
			half Input_Speed62_g11495 = -MotionSpeed265_g1;
			float temp_output_4_0_g11498 = Input_Speed62_g11495;
			float mulTime2_g11498 = _Time.y * temp_output_4_0_g11498;
			half3 localObjectPosition_unity_ObjectToWorld8_g11499 = ObjectPosition_unity_ObjectToWorld();
			float3 break9_g11499 = localObjectPosition_unity_ObjectToWorld8_g11499;
			float temp_output_218_0_g11495 = -MotionVariation264_g1;
			float Motion_Variation284_g11495 = ( temp_output_218_0_g11495 * v.color.a );
			float Motion_Scale287_g11495 = ( MotionScale262_g1 * ase_worldPos.x );
			half Motion_Vertical223_g1 = ( ( _MotionVertical_20 * Bounds_Radius121_g1 ) * Global_Amplitude_270_g1 * Mesh_Motion_260_g1 * Motion_UseMain56_g1 * ( Global_NoiseTex_R34_g1 + Global_NoiseTex_B132_g1 ) * sin( ( mulTime2_g11498 + ( break9_g11499.x + break9_g11499.z ) + Motion_Variation284_g11495 + Motion_Scale287_g11495 ) ) );
			float3 appendResult282_g1 = (float3(0.0 , Motion_Vertical223_g1 , 0.0));
			half3 Vertex_Motion_Standard324_g1 = ( temp_output_261_19_g1 + Motion_Flutter263_g1 + appendResult282_g1 );
			half3 Vertex_Motion833_g1 = Vertex_Motion_Standard324_g1;
			float3 temp_output_689_0_g1 = ( lerpResult348_g1 * Vertex_Motion833_g1 );
			half3 localObjectPosition_unity_ObjectToWorld8_g11484 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11485 = localObjectPosition_unity_ObjectToWorld8_g11484;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11485 = temp_output_7_0_g11485;
			#else
				float3 staticSwitch8_g11485 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11485 = staticSwitch8_g11485;
			#else
				float3 staticSwitch10_g11485 = temp_output_7_0_g11485;
			#endif
			float4 tex2DNode7_g11483 = tex2Dlod( TVE_ExtrasTex, float4( ( (TVE_ExtrasCoord).xy + ( TVE_ExtrasCoord.z * (staticSwitch10_g11485).xz ) ), 0, 0.0) );
			float3 linearToGamma9_g11483 = LinearToGammaSpace( tex2DNode7_g11483.rgb );
			#ifdef UNITY_COLORSPACE_GAMMA
				float3 staticSwitch13_g11483 = (tex2DNode7_g11483).rgb;
			#else
				float3 staticSwitch13_g11483 = linearToGamma9_g11483;
			#endif
			float3 break20_g11483 = staticSwitch13_g11483;
			half Global_ExtrasTex_G305_g1 = break20_g11483.y;
			half Global_Size693_g1 = _GlobalSize;
			float lerpResult346_g1 = lerp( 1.0 , Global_ExtrasTex_G305_g1 , Global_Size693_g1);
			half3 Vertex_Position601_g1 = ( temp_output_689_0_g1 * lerpResult346_g1 );
			float3 temp_output_7_0_g11522 = Vertex_Position601_g1;
			float3 appendResult1108_g1 = (float3(Motion_X216_g1 , Motion_Vertical223_g1 , Motion_Z190_g1));
			float3 appendResult1120_g1 = (float3(Motion_Y138_g1 , 0.0 , -Motion_Y138_g1));
			half3 Vertex_Motion_Standard_Batched1110_g1 = ( ase_vertex3Pos + appendResult1108_g1 + appendResult1120_g1 + Motion_Flutter263_g1 );
			half3 Vertex_Motion_Batched1118_g1 = Vertex_Motion_Standard_Batched1110_g1;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11522 = temp_output_7_0_g11522;
			#else
				float3 staticSwitch8_g11522 = Vertex_Motion_Batched1118_g1;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11522 = staticSwitch8_g11522;
			#else
				float3 staticSwitch10_g11522 = temp_output_7_0_g11522;
			#endif
			half3 Final_Vertex890_g1 = ( staticSwitch10_g11522 + ( _IsStandardPipeline * 0.0 ) );
			v.vertex.xyz = Final_Vertex890_g1;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			half2 Main_UVs15_g1 = ( ( i.uv_texcoord * (_MainUVs).xy ) + (_MainUVs).zw );
			half3 Main_Normal137_g1 = UnpackScaleNormal( tex2D( _MainNormalTex, Main_UVs15_g1 ), _MainNormalValue );
			float3 temp_output_13_0_g11518 = Main_Normal137_g1;
			float3 switchResult12_g11518 = (((i.ASEVFace>0)?(temp_output_13_0_g11518):(( temp_output_13_0_g11518 * __normalsoptions ))));
			half3 Blend_Normal312_g1 = switchResult12_g11518;
			half3 localObjectPosition_unity_ObjectToWorld8_g11514 = ObjectPosition_unity_ObjectToWorld();
			float2 temp_output_38_0_g11512 = ( ( i.uv_texcoord + (localObjectPosition_unity_ObjectToWorld8_g11514).xz ) * TVE_OverlayUVTilling * _OverlayUVTilling );
			float3 temp_output_13_0_g11513 = UnpackScaleNormal( tex2D( TVE_OverlayNormalTex, temp_output_38_0_g11512 ), TVE_OverlayNormalValue );
			float3 switchResult12_g11513 = (((i.ASEVFace>0)?(temp_output_13_0_g11513):(( temp_output_13_0_g11513 * __normalsoptions ))));
			half3 Global_OverlayNormal313_g1 = switchResult12_g11513;
			half3 Global_OverlayDirection159_g1 = (TVE_OverlayDirection).xyz;
			half3 Blend_NormalRaw1051_g1 = Main_Normal137_g1;
			float3 switchResult1063_g1 = (((i.ASEVFace>0)?(Blend_NormalRaw1051_g1):(( Blend_NormalRaw1051_g1 * half3(-1,-1,-1) ))));
			float dotResult218_g1 = dot( Global_OverlayDirection159_g1 , (WorldNormalVector( i , switchResult1063_g1 )) );
			half Mesh_Variation16_g1 = i.vertexColor.a;
			float lerpResult1065_g1 = lerp( 1.0 , Mesh_Variation16_g1 , _OverlayVariation);
			half Global_OverlayIntensity154_g1 = TVE_OverlayIntensity;
			half3 localObjectPosition_unity_ObjectToWorld8_g11484 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11485 = localObjectPosition_unity_ObjectToWorld8_g11484;
			float3 ase_worldPos = i.worldPos;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11485 = temp_output_7_0_g11485;
			#else
				float3 staticSwitch8_g11485 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11485 = staticSwitch8_g11485;
			#else
				float3 staticSwitch10_g11485 = temp_output_7_0_g11485;
			#endif
			float4 tex2DNode7_g11483 = tex2D( TVE_ExtrasTex, ( (TVE_ExtrasCoord).xy + ( TVE_ExtrasCoord.z * (staticSwitch10_g11485).xz ) ) );
			float3 linearToGamma9_g11483 = LinearToGammaSpace( tex2DNode7_g11483.rgb );
			#ifdef UNITY_COLORSPACE_GAMMA
				float3 staticSwitch13_g11483 = (tex2DNode7_g11483).rgb;
			#else
				float3 staticSwitch13_g11483 = linearToGamma9_g11483;
			#endif
			float3 break20_g11483 = staticSwitch13_g11483;
			half Global_ExtrasTex_B156_g1 = break20_g11483.z;
			float temp_output_1025_0_g1 = ( Global_OverlayIntensity154_g1 * _GlobalOverlay * Global_ExtrasTex_B156_g1 );
			half Mask_Overlay269_g1 = saturate( ( saturate( dotResult218_g1 ) - ( 1.0 - ( lerpResult1065_g1 * temp_output_1025_0_g1 ) ) ) );
			float3 lerpResult349_g1 = lerp( Blend_Normal312_g1 , Global_OverlayNormal313_g1 , Mask_Overlay269_g1);
			half3 Final_Normal366_g1 = lerpResult349_g1;
			o.Normal = Final_Normal366_g1;
			float4 lerpResult26_g1 = lerp( _MainColorVariation , _MainColor , Mesh_Variation16_g1);
			float4 tex2DNode29_g1 = tex2D( _MainAlbedoTex, Main_UVs15_g1 );
			float4 temp_output_51_0_g1 = ( lerpResult26_g1 * tex2DNode29_g1 );
			half3 Main_AlbedoRaw99_g1 = (temp_output_51_0_g1).rgb;
			float3 temp_cast_2 = (1.0).xxx;
			half3 localObjectPosition_unity_ObjectToWorld8_g11508 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11509 = localObjectPosition_unity_ObjectToWorld8_g11508;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11509 = temp_output_7_0_g11509;
			#else
				float3 staticSwitch8_g11509 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11509 = staticSwitch8_g11509;
			#else
				float3 staticSwitch10_g11509 = temp_output_7_0_g11509;
			#endif
			#ifdef UNITY_COLORSPACE_GAMMA
				float staticSwitch1_g11507 = 2.0;
			#else
				float staticSwitch1_g11507 = 4.594794;
			#endif
			float3 lerpResult108_g1 = lerp( temp_cast_2 , ( (tex2D( TVE_ColorsTex, ( (TVE_ColorsCoord).xy + ( TVE_ColorsCoord.z * (staticSwitch10_g11509).xz ) ) )).rgb * staticSwitch1_g11507 ) , _GlobalColors);
			half3 Main_AlbedoColored863_g1 = ( Main_AlbedoRaw99_g1 * lerpResult108_g1 );
			float4 tex2DNode35_g1 = tex2D( _MainMaskTex, Main_UVs15_g1 );
			half Main_Mask57_g1 = tex2DNode35_g1.b;
			float temp_output_7_0_g11501 = _SubsurfaceMinValue;
			half AutoRegister_MaterialShadingSpace1208_g1 = _MaterialShadingSpaceDrawer;
			float temp_output_1345_0_g1 = saturate( ( ( Main_Mask57_g1 - temp_output_7_0_g11501 ) / ( ( _SubsurfaceMaxValue + AutoRegister_MaterialShadingSpace1208_g1 ) - temp_output_7_0_g11501 ) ) );
			half3 Subsurface_Color884_g1 = ( (_SubsurfaceColor).rgb * ( temp_output_1345_0_g1 * _SubsurfaceValue ) );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float dotResult785_g1 = dot( -ase_worldlightDir , ase_worldViewDir );
			float temp_output_792_0_g1 = (dotResult785_g1*0.5 + 0.5);
			half Mask_Subsurface_View782_g1 = ( temp_output_792_0_g1 * temp_output_792_0_g1 );
			half Mesh_Occlusion318_g1 = i.vertexColor.g;
			float saferPower1201_g1 = max( Mesh_Occlusion318_g1 , 0.0001 );
			half Vertex_Occlusion648_g1 = pow( saferPower1201_g1 , _ObjectOcclusionValue );
			float temp_output_1199_0_g1 = ( Mask_Subsurface_View782_g1 * Vertex_Occlusion648_g1 );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 temp_output_13_0_g11525 = ase_worldNormal;
			float3 switchResult12_g11525 = (((i.ASEVFace>0)?(temp_output_13_0_g11525):(( temp_output_13_0_g11525 * __normalsoptions ))));
			float dotResult777_g1 = dot( switchResult12_g11525 , -ase_worldlightDir );
			half Mask_Subsurface_Normal870_g1 = saturate( ( dotResult777_g1 + 1.0 ) );
			half3 Main_Albedo149_g1 = ( Main_AlbedoColored863_g1 + ( Subsurface_Color884_g1 * saturate( ase_lightColor.rgb ) * ( temp_output_1199_0_g1 * Mask_Subsurface_Normal870_g1 ) ) );
			half3 Blend_Albedo265_g1 = Main_Albedo149_g1;
			float4 tex2DNode30_g11512 = tex2D( TVE_OverlayAlbedoTex, temp_output_38_0_g11512 );
			half3 Global_OverlayAlbedo277_g1 = ( (TVE_OverlayColor).rgb * (tex2DNode30_g11512).rgb );
			float3 lerpResult336_g1 = lerp( Blend_Albedo265_g1 , Global_OverlayAlbedo277_g1 , Mask_Overlay269_g1);
			half3 Final_Albedo359_g1 = lerpResult336_g1;
			half Main_Alpha316_g1 = (temp_output_51_0_g1).a;
			float lerpResult354_g1 = lerp( 1.0 , Main_Alpha316_g1 , __premul);
			half Final_Premultiply355_g1 = lerpResult354_g1;
			o.Albedo = ( Final_Albedo359_g1 * Final_Premultiply355_g1 );
			half Main_Metallic237_g1 = ( tex2DNode35_g1.r * _MainMetallicValue );
			half Blend_Metallic306_g1 = Main_Metallic237_g1;
			float lerpResult342_g1 = lerp( Blend_Metallic306_g1 , 0.0 , Mask_Overlay269_g1);
			half Final_Metallic367_g1 = lerpResult342_g1;
			o.Metallic = Final_Metallic367_g1;
			half Main_Smoothness227_g1 = ( tex2DNode35_g1.a * _MainSmoothnessValue );
			half Blend_Smoothness314_g1 = Main_Smoothness227_g1;
			half Global_OverlaySmoothness311_g1 = TVE_OverlaySmoothness;
			float lerpResult343_g1 = lerp( Blend_Smoothness314_g1 , Global_OverlaySmoothness311_g1 , Mask_Overlay269_g1);
			half Final_Smoothness371_g1 = lerpResult343_g1;
			half Global_Wetness1016_g1 = ( TVE_Wetness * _GlobalWetness );
			half Global_ExtrasTex_A1033_g1 = tex2DNode7_g11483.a;
			float lerpResult1037_g1 = lerp( Final_Smoothness371_g1 , saturate( ( Final_Smoothness371_g1 + Global_Wetness1016_g1 ) ) , Global_ExtrasTex_A1033_g1);
			o.Smoothness = lerpResult1037_g1;
			float lerpResult240_g1 = lerp( 1.0 , tex2DNode35_g1.g , _MainOcclusionValue);
			half Main_Occlusion247_g1 = lerpResult240_g1;
			half Blend_Occlusion323_g1 = Main_Occlusion247_g1;
			o.Occlusion = ( Vertex_Occlusion648_g1 * Blend_Occlusion323_g1 );
			o.Alpha = Main_Alpha316_g1;
			half Main_AlphaRaw1203_g1 = tex2DNode29_g1.a;
			half Global_ExtrasTex_R174_g1 = break20_g11483.x;
			float lerpResult293_g1 = lerp( 1.0 , ceil( ( pow( Mesh_Variation16_g1 , 2.0 ) - ( 1.0 - Global_ExtrasTex_R174_g1 ) ) ) , _GlobalLeaves);
			half Mask_Leaves315_g1 = lerpResult293_g1;
			half Alpha5_g11524 = ( Main_AlphaRaw1203_g1 * Mask_Leaves315_g1 );
			#ifdef _ALPHATEST_ON
				float staticSwitch2_g11524 = Alpha5_g11524;
			#else
				float staticSwitch2_g11524 = 1.0;
			#endif
			half Final_Clip914_g1 = staticSwitch2_g11524;
			clip( Final_Clip914_g1 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "TVEShaderCoreGUI"
}
/*ASEBEGIN
Version=17802
1927;1;1906;1020;2904.435;754.1281;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;81;-1840,-768;Half;False;Property;_IsStandardShader;_IsStandardShader;151;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-2176,-640;Half;False;Property;_Cutoff;_Cutoff;153;1;[HideInInspector];Fetch;False;4;Alpha;0;Premultiply;1;Additive;2;Multiply;3;0;True;0;0.5;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-1888,-640;Half;False;Property;__src;__src;155;1;[HideInInspector];Fetch;False;0;0;True;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;168;-2032,-768;Half;False;Property;_IsLeafShader;_IsLeafShader;150;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;361;-1584,-512;Inherit;False;Property;_SubsurfaceMessageHD1;!!! Subsurface Message HD !!!;3;0;Create;False;0;0;False;1;StyledMessage(Info, Diffusion Profiles are not directly supported. You need to create an HDRP Lit material and assign a Diffusion Profile then drag it to the Subsurface Diffusion Material slot., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;360;-2176,-512;Inherit;False;Property;_SubsurfaceMessageStandard1;!!! Subsurface Message !!!;1;0;Create;False;0;0;True;1;StyledMessage(Info, Subsurface shading requires Directional light and Ambient light higher than 0 to work. Other light types are not supported. Switch to Advanced Lit shaders for high quality translucency., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;271;-1600,-768;Half;False;Property;_IsLitShader;_IsLitShader;152;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;359;-1888,-512;Inherit;False;Property;_SubsurfaceMessageUniversal1;!!! Subsurface Message Universal !!!;2;0;Create;False;0;0;False;1;StyledMessage(Info, In Universal Render Pipeline the Subsurface only work for the Directional light. Other light types are not supported, 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1632,-640;Half;False;Property;__zw;__zw;157;1;[HideInInspector];Fetch;False;2;Opaque;0;Transparent;1;0;True;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-2016,-640;Half;False;Property;__cull;__cull;154;2;[HideInInspector];[Enum];Fetch;False;3;Both;0;Back;1;Front;2;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-2176,-768;Half;False;Property;_Banner;Banner;0;0;Create;True;0;0;True;1;StyledBanner(Leaf Standard Lit);0;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-1760,-640;Half;False;Property;__dst;__dst;156;1;[HideInInspector];Fetch;False;2;Opaque;0;Transparent;1;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;358;-2176,-128;Inherit;False;Base;4;;1;856f7164d1c579d43a5cf4968a75ca43;20,1034,1,860,1,1000,1,995,1,847,2,1114,2,850,1,853,0,1271,1,1298,1,1300,1,866,1,844,2,883,0,879,0,881,0,878,0,888,0,893,1,916,0;0;16;FLOAT3;0;FLOAT3;528;FLOAT;529;FLOAT;530;FLOAT;531;FLOAT;1235;FLOAT3;1230;FLOAT;1290;FLOAT;721;FLOAT;532;FLOAT;653;FLOAT;892;FLOAT3;534;FLOAT4;535;FLOAT;629;FLOAT;722
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-1376,-128;Float;False;True;-1;2;TVEShaderCoreGUI;0;0;Standard;BOXOPHOBIC/The Vegetation Engine/Vegetation/Leaf Standard Lit;False;False;False;False;False;True;False;True;False;False;False;False;True;True;True;False;True;False;False;False;True;Back;0;True;17;0;False;-1;True;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Opaque;;Geometry;All;11;d3d11;glcore;gles3;metal;vulkan;xbox360;xboxone;ps4;psp2;n3ds;wiiu;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;1;0;True;20;0;True;7;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Absolute;0;;-1;-1;-1;-1;0;False;0;0;True;10;-1;0;True;21;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;37;-2176,-896;Inherit;False;1023.392;100;Internal;0;;1,0.252,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;33;-2176,-256;Inherit;False;1024.392;100;Final;0;;0.3439424,0.5960785,0,1;0;0
WireConnection;0;0;358;0
WireConnection;0;1;358;528
WireConnection;0;3;358;529
WireConnection;0;4;358;530
WireConnection;0;5;358;531
WireConnection;0;9;358;532
WireConnection;0;10;358;653
WireConnection;0;11;358;534
ASEEND*/
//CHKSM=63875A296C0338A299AF8BA8362C1F85AE5816E0