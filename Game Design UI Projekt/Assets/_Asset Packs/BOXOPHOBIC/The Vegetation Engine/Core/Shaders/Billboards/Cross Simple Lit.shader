// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BOXOPHOBIC/The Vegetation Engine/Billboards/Cross Simple Lit"
{
	Properties
	{
		[StyledBanner(Cross Simple Lit)]_Banner("Banner", Float) = 0
		[StyledMessage(Info, Subsurface shading requires Directional light and Ambient light higher than 0 to work. Other light types are not supported., 5, 5 )]_SubsurfaceMessageStandard2("!!! Subsurface Message !!!", Float) = 0
		[HideInInspector]_IsVersion("_IsVersion", Float) = 110
		[HideInInspector]_IsStandardPipeline("_IsStandardPipeline", Float) = 1
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector][Toggle]_IsUpdated("_IsUpdated", Float) = 0
		[HideInInspector]_VariationMode("_VariationMode", Float) = -1
		[HideInInspector]_MaxBoundsInfo("_MaxBoundsInfo", Vector) = (1,1,1,1)
		[StyledCategory(Render Settings)]_RenderingCat("[ Rendering Cat ]", Float) = 0
		[Enum(Opaque,0,Transparent,1)]__surface("Render Mode", Float) = 0
		[Enum(Both,0,Back,1,Front,2)]__cull("Render Faces", Float) = 0
		[Enum(Flip,0,Mirror,1,None,2)]__normals("Render Normals", Float) = 0
		[StyledInteractive(__surface, 1)]_RenderMode_TransparentInteractive("# RenderMode_TransparentInteractive", Float) = 0
		[Enum(Alpha,0,Premultiply,1)]__blend("Render Blending", Float) = 0
		[Enum(Off,0,On,1)]__zw("Render ZWrite", Float) = 1
		[StyledInteractive(ON)]_RenderMode_ResetInteractive("# RenderMode_ResetInteractive", Float) = 0
		[Toggle][Space(10)]__clip("Alpha Clipping", Float) = 0
		[StyledInteractive(__clip, 1)]_AlphaClipInteractive("# AlphaClipInteractive", Float) = 0
		_Cutoff("Alpha Treshold", Range( 0 , 1)) = 0.5
		[HideInInspector]__cliptreshold("__cliptreshold", Float) = 0
		[StyledCategory(Global Settings)]_GlobalSettingsCat("[ Global Settings Cat ]", Float) = 0
		_GlobalColors("Global Colors", Range( 0 , 1)) = 1
		[HideInInspector]_GlobalOverlayMode("Global Overlay Mode", Float) = 1
		_GlobalOverlay("Global Overlay", Range( 0 , 1)) = 1
		_GlobalWetness("Global Wetness", Range( 0 , 1)) = 1
		_GlobalSize("Global Size", Range( 0 , 1)) = 1
		_GlobalSizeFade("Global Size Fade", Range( 0 , 1)) = 1
		[StyledCategory(Material Shading)]_MaterialShadingCat("[ Material Shading Cat ]", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Subsurface Mask. Use the Min Max sliders to adjust or invert the subsurface effect., _IsLeafShader, 1, 5, 10 )]_MessageSubsurfaceMask("!!! Message Leaf Subsurface Mask !!!", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Subsurface Mask. Use the Min Max sliders to adjust or invert the subsurface effect., _IsCrossShader, 1, 5, 10 )]_MessageCrossSubsurfaceMask("!!! Message Cross Subsurface Mask !!!", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Height Map. Use the Min Max values to adjust or invert the height., _IsTessellationShader, 1, 5, 10 )]_MessageHeightMap("!!! Message Height Map !!!", Float) = 0
		[HDR]_SubsurfaceColor("Subsurface Color", Color) = (0.3315085,0.490566,0,1)
		_SubsurfaceValue("Subsurface Intensity", Range( 0 , 1)) = 1
		_SubsurfaceMinValue("Subsurface Min", Range( 0 , 1)) = 0
		_SubsurfaceMaxValue("Subsurface Max", Range( 0 , 1)) = 1
		[StyledSpace(10)]_MaterialShadingSpaceDrawer("# Material Shading Space", Float) = 0
		_ObjectSmoothnessValue("Object Smoothness", Range( 0 , 1)) = 0
		[Space(10)]_OverlayUVTilling("Overlay Tilling", Range( 0 , 10)) = 1
		[StyledCategory(Main Shading)]_MainShadingCat("[ Main Shading Cat ]", Float) = 0
		[Enum(Constant,0,Variation,1)]_MainColorMode("Main Color Mode", Float) = 1
		[Space(10)]_MainColor("Main Color One", Color) = (1,1,1,1)
		_MainColorVariation("Main Color Two", Color) = (1,1,1,1)
		[NoScaleOffset]_MainAlbedoTex("Main Albedo", 2D) = "white" {}
		[NoScaleOffset]_MainNormalTex("Main Normal", 2D) = "bump" {}
		[NoScaleOffset]_MainMaskTex("Main Mask", 2D) = "white" {}
		[Space(10)]_MainUVs("Main UVs", Vector) = (1,1,0,0)
		_MainNormalValue("Main Normal", Range( -8 , 8)) = 1
		[StyledCategory(Motion Settings)]_MotionCat("[ Motion Cat ]", Float) = 0
		[HideInInspector][Enum(Off,0,Procedural,1,Hierarchical,2)]_MotionMode("Motion Mode", Float) = 0
		[HideInInspector][Enum(Object Pivot,0,Element Pivot,1)]_PivotMode("Pivot Mode", Float) = 0
		[Toggle]_UseMotion_Main("Motion Main", Float) = 1
		[HideInInspector][Space(10)]_MotionAmplitude_10("Primary Bending", Float) = 0
		[HideInInspector][IntRange]_MotionSpeed_10("Primary Speed", Float) = 2
		[HideInInspector]_MotionScale_10("Primary Elasticity", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector]_MotionVariation_10("Primary Variation", Float) = 0
		[HideInInspector][Space(10)]_InteractionAmplitude("Interaction Bending", Float) = 0
		[StyledCategory(Advanced)]_AdvancedCat("[ Advanced Cat]", Float) = 0
		[IntRange]__priority("Priority", Range( -50 , 50)) = 0
		[HideInInspector]__src("__src", Float) = 1
		[HideInInspector]__dst("__dst", Float) = 0
		[HideInInspector]__premul("__premul", Float) = 0
		[HideInInspector]_Color("_Color", Color) = (0,0,0,0)
		[HideInInspector]_MainTex("_MainTex", 2D) = "white" {}
		[HideInInspector]_VertexOcclusion("_VertexOcclusion", Float) = 0
		[HideInInspector]_MainMaskValue("_MainMaskValue", Float) = 0
		[HideInInspector][Enum(Translucency,0,Thickness,1)]_SubsurfaceMode("_SubsurfaceMode", Float) = 0
		[HideInInspector]_ObjectThicknessValue("_ObjectThicknessValue", Float) = 0
		[HideInInspector]__normalsoptions("__normalsoptions", Vector) = (1,1,1,0)
		[HideInInspector]_IsSimpleShader("_IsSimpleShader", Float) = 1
		[HideInInspector]_IsLitShader("_IsLitShader", Float) = 1
		[HideInInspector]_IsCrossShader("_IsCrossShader", Float) = 1
		[HideInInspector]_Cutoff("_Cutoff", Float) = 0.5
		[HideInInspector][Enum(Both,0,Back,1,Front,2)]__cull("__cull", Float) = 0
		[HideInInspector]__zw("__zw", Float) = 1
		_SpecColor("Specular Color",Color)=(1,1,1,1)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" "DisableBatching" = "True" }
		Cull [__cull]
		ZWrite [__zw]
		Offset  0 , -1
		Blend [__src] [__dst]
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma shader_feature _ALPHATEST_ON
		  
		//FEATURES BEGIN
		//FEATURES END
		    
		#define TVE_DISABLE_UNSAFE_BATCHING
		#pragma exclude_renderers d3d9 d3d11_9x gles 
		#pragma surface surf BlinnPhong keepalpha addshadow fullforwardshadows novertexlights nodynlightmap vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			half ASEVFace : VFACE;
			float3 worldNormal;
			INTERNAL_DATA
			float4 vertexColor : COLOR;
		};

		uniform float4 _MaxBoundsInfo;
		uniform half _AdvancedCat;
		uniform half __surface;
		uniform half _PivotMode;
		uniform half __zw;
		uniform half _GlobalSettingsCat;
		uniform half _IsTVEShader;
		uniform half _IsUpdated;
		uniform half _MaterialShadingCat;
		uniform half __blend;
		uniform half __src;
		uniform half _Cutoff;
		uniform half __dst;
		uniform half _RenderMode_ResetInteractive;
		uniform half _MessageHeightMap;
		uniform half _MessageCrossSubsurfaceMask;
		uniform float _SubsurfaceMode;
		uniform half _MainMaskValue;
		uniform half _VertexOcclusion;
		uniform half _MainColorMode;
		uniform half _RenderMode_TransparentInteractive;
		uniform half _GlobalOverlayMode;
		uniform half __cull;
		uniform half __clip;
		uniform sampler2D _MainTex;
		uniform half __normals;
		uniform half _VariationMode;
		uniform float4 _Color;
		uniform half _RenderingCat;
		uniform half _IsVersion;
		uniform half __cliptreshold;
		uniform half __priority;
		uniform half _MotionMode;
		uniform half _MainShadingCat;
		uniform half _AlphaClipInteractive;
		uniform half _ObjectThicknessValue;
		uniform half _MessageSubsurfaceMask;
		uniform float _SubsurfaceMessageStandard2;
		uniform half _Banner;
		uniform half _IsSimpleShader;
		uniform half _IsCrossShader;
		uniform half _IsLitShader;
		uniform half TVE_SizeFadeEnd;
		uniform half TVE_SizeFadeStart;
		uniform half _GlobalSizeFade;
		uniform half _MotionAmplitude_10;
		uniform half TVE_Amplitude1;
		uniform sampler2D TVE_NoiseTex;
		uniform half TVE_NoiseSpeed;
		uniform half TVE_NoiseSize;
		uniform half TVE_NoiseContrast;
		uniform sampler2D TVE_MotionTex;
		uniform half4 TVE_MotionCoord;
		uniform half _MotionSpeed_10;
		uniform half _MotionVariation_10;
		uniform half _MotionScale_10;
		uniform float _InteractionAmplitude;
		uniform half _UseMotion_Main;
		uniform half _MotionCat;
		uniform sampler2D TVE_ExtrasTex;
		uniform half4 TVE_ExtrasCoord;
		uniform half _GlobalSize;
		uniform half _IsStandardPipeline;
		uniform half _MainNormalValue;
		uniform sampler2D _MainNormalTex;
		uniform half4 _MainUVs;
		uniform half3 __normalsoptions;
		uniform half TVE_OverlayNormalValue;
		uniform sampler2D TVE_OverlayNormalTex;
		uniform half TVE_OverlayUVTilling;
		uniform half _OverlayUVTilling;
		uniform half TVE_OverlayIntensity;
		uniform half _GlobalOverlay;
		uniform half4 _MainColorVariation;
		uniform half4 _MainColor;
		uniform sampler2D _MainAlbedoTex;
		uniform sampler2D TVE_ColorsTex;
		uniform half4 TVE_ColorsCoord;
		uniform half _GlobalColors;
		uniform half4 _SubsurfaceColor;
		uniform sampler2D _MainMaskTex;
		uniform half _SubsurfaceMinValue;
		uniform half _SubsurfaceMaxValue;
		uniform half _MaterialShadingSpaceDrawer;
		uniform half _SubsurfaceValue;
		uniform half4 TVE_OverlayColor;
		uniform sampler2D TVE_OverlayAlbedoTex;
		uniform half __premul;
		uniform float _ObjectSmoothnessValue;
		uniform float TVE_Wetness;
		uniform half _GlobalWetness;


		half3 ObjectPosition_unity_ObjectToWorld(  )
		{
			return half3(unity_ObjectToWorld[0].w, unity_ObjectToWorld[1].w, unity_ObjectToWorld[2].w );
		}


		float3 DecodeFloatToVector3( int enc )
		{
			  float3 result ;
			  result.x = ((enc >> 0) & 0xFF) / 255.0f;
			  result.y = ((enc >> 8) & 0xFF) / 255.0;
			  result.z = ((enc >>16) & 0xFF) / 255.0;
			  //result.w = ((enc >> 32) & 0xFF) / 255.0;
			  return result;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			half3 localObjectPosition_unity_ObjectToWorld8_g11469 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_731_7_g11117 = localObjectPosition_unity_ObjectToWorld8_g11469;
			half Global_SizeFadeEnd287_g11117 = TVE_SizeFadeEnd;
			float temp_output_7_0_g11503 = Global_SizeFadeEnd287_g11117;
			half Global_SizeFadeStart276_g11117 = TVE_SizeFadeStart;
			half Global_SizeFade694_g11117 = _GlobalSizeFade;
			float lerpResult348_g11117 = lerp( 1.0 , saturate( ( ( distance( _WorldSpaceCameraPos , temp_output_731_7_g11117 ) - temp_output_7_0_g11503 ) / ( Global_SizeFadeStart276_g11117 - temp_output_7_0_g11503 ) ) ) , Global_SizeFade694_g11117);
			float3 ase_vertex3Pos = v.vertex.xyz;
			half3 VertexPos40_g11504 = ase_vertex3Pos;
			float3 appendResult74_g11504 = (float3(VertexPos40_g11504.x , 0.0 , 0.0));
			half3 VertexPosRotationAxis50_g11504 = appendResult74_g11504;
			float3 break84_g11504 = VertexPos40_g11504;
			float3 appendResult81_g11504 = (float3(0.0 , break84_g11504.y , break84_g11504.z));
			half3 VertexPosOtherAxis82_g11504 = appendResult81_g11504;
			float temp_output_7_0_g11472 = UNITY_PI;
			half Bounds_Height374_g11117 = _MaxBoundsInfo.y;
			#ifdef UNITY_INSTANCING_ENABLED
				float staticSwitch8_g11472 = temp_output_7_0_g11472;
			#else
				float staticSwitch8_g11472 = ( Bounds_Height374_g11117 * UNITY_PI );
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float staticSwitch10_g11472 = staticSwitch8_g11472;
			#else
				float staticSwitch10_g11472 = temp_output_7_0_g11472;
			#endif
			half Motion_Max_Bending1133_g11117 = staticSwitch10_g11472;
			half Global_Amplitude_136_g11117 = TVE_Amplitude1;
			float temp_output_4_0_g11489 = 1.0;
			float mulTime2_g11489 = _Time.y * temp_output_4_0_g11489;
			float2 temp_cast_0 = (TVE_NoiseSpeed).xx;
			half3 localObjectPosition_unity_ObjectToWorld8_g11488 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11487 = localObjectPosition_unity_ObjectToWorld8_g11488;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11487 = temp_output_7_0_g11487;
			#else
				float3 staticSwitch8_g11487 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11487 = staticSwitch8_g11487;
			#else
				float3 staticSwitch10_g11487 = temp_output_7_0_g11487;
			#endif
			float2 panner73_g11486 = ( mulTime2_g11489 * temp_cast_0 + ( (staticSwitch10_g11487).xz * TVE_NoiseSize ));
			float4 temp_cast_1 = (TVE_NoiseContrast).xxxx;
			float4 break142_g11486 = pow( abs( tex2Dlod( TVE_NoiseTex, float4( panner73_g11486, 0, 0.0) ) ) , temp_cast_1 );
			half Global_NoiseTex_R34_g11117 = break142_g11486.r;
			half3 localObjectPosition_unity_ObjectToWorld8_g11480 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11481 = localObjectPosition_unity_ObjectToWorld8_g11480;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11481 = temp_output_7_0_g11481;
			#else
				float3 staticSwitch8_g11481 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11481 = staticSwitch8_g11481;
			#else
				float3 staticSwitch10_g11481 = temp_output_7_0_g11481;
			#endif
			float3 temp_output_18_0_g11479 = (tex2Dlod( TVE_MotionTex, float4( ( (TVE_MotionCoord).xy + ( TVE_MotionCoord.z * (staticSwitch10_g11481).xz ) ), 0, 0.0) )).rgb;
			float3 linearToGamma28_g11479 = LinearToGammaSpace( temp_output_18_0_g11479 );
			float3 break322_g11478 = linearToGamma28_g11479;
			float3 appendResult323_g11478 = (float3(break322_g11478.x , 0.0 , break322_g11478.y));
			float3 temp_output_324_0_g11478 = (appendResult323_g11478*2.0 + -1.0);
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float3 temp_output_7_0_g11482 = ( mul( unity_WorldToObject, float4( temp_output_324_0_g11478 , 0.0 ) ).xyz * ase_objectScale );
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11482 = temp_output_7_0_g11482;
			#else
				float3 staticSwitch8_g11482 = temp_output_324_0_g11478;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11482 = staticSwitch8_g11482;
			#else
				float3 staticSwitch10_g11482 = temp_output_7_0_g11482;
			#endif
			float2 temp_output_1196_320_g11117 = (staticSwitch10_g11482).xz;
			half2 Motion_DirectionOS39_g11117 = temp_output_1196_320_g11117;
			half Input_Speed62_g11490 = _MotionSpeed_10;
			float temp_output_4_0_g11493 = Input_Speed62_g11490;
			float mulTime2_g11493 = _Time.y * temp_output_4_0_g11493;
			half3 localObjectPosition_unity_ObjectToWorld8_g11491 = ObjectPosition_unity_ObjectToWorld();
			float3 break9_g11491 = localObjectPosition_unity_ObjectToWorld8_g11491;
			half Motion_Variation284_g11490 = ( ( _MotionVariation_10 * v.color.a ) + ( break9_g11491.x + break9_g11491.z ) );
			float2 appendResult344_g11490 = (float2(ase_worldPos.x , ase_worldPos.z));
			float2 Motion_Scale287_g11490 = ( _MotionScale_10 * appendResult344_g11490 );
			half2 Sine_MinusOneToOne281_g11490 = sin( ( mulTime2_g11493 + Motion_Variation284_g11490 + Motion_Scale287_g11490 ) );
			float2 temp_cast_4 = (1.0).xx;
			half Input_Turbulence327_g11490 = Global_NoiseTex_R34_g11117;
			float2 lerpResult321_g11490 = lerp( Sine_MinusOneToOne281_g11490 , temp_cast_4 , Input_Turbulence327_g11490);
			half2 Motion_Interaction53_g11117 = ( _InteractionAmplitude * Motion_Max_Bending1133_g11117 * temp_output_1196_320_g11117 );
			half Motion_InteractionMask66_g11117 = break322_g11478.z;
			float2 lerpResult109_g11117 = lerp( ( ( _MotionAmplitude_10 * Motion_Max_Bending1133_g11117 ) * Global_Amplitude_136_g11117 * Global_NoiseTex_R34_g11117 * Motion_DirectionOS39_g11117 * lerpResult321_g11490 ) , Motion_Interaction53_g11117 , Motion_InteractionMask66_g11117);
			int enc15_g11494 = (int)v.texcoord3.x;
			float3 localDecodeFloatToVector315_g11494 = DecodeFloatToVector3( enc15_g11494 );
			float3 break17_g11494 = localDecodeFloatToVector315_g11494;
			half Mesh_Motion_182_g11117 = break17_g11494.x;
			half Motion_UseMain56_g11117 = ( _UseMotion_Main + ( _MotionCat * 0.0 ) );
			float2 break143_g11117 = ( lerpResult109_g11117 * Mesh_Motion_182_g11117 * Motion_UseMain56_g11117 );
			half Motion_Z190_g11117 = break143_g11117.y;
			half Angle44_g11504 = Motion_Z190_g11117;
			half3 VertexPos40_g11461 = ( VertexPosRotationAxis50_g11504 + ( VertexPosOtherAxis82_g11504 * cos( Angle44_g11504 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g11504 ) * sin( Angle44_g11504 ) ) );
			float3 appendResult74_g11461 = (float3(0.0 , 0.0 , VertexPos40_g11461.z));
			half3 VertexPosRotationAxis50_g11461 = appendResult74_g11461;
			float3 break84_g11461 = VertexPos40_g11461;
			float3 appendResult81_g11461 = (float3(break84_g11461.x , break84_g11461.y , 0.0));
			half3 VertexPosOtherAxis82_g11461 = appendResult81_g11461;
			half Motion_X216_g11117 = break143_g11117.x;
			half Angle44_g11461 = -Motion_X216_g11117;
			half3 Vertex_Motion_Main595_g11117 = ( VertexPosRotationAxis50_g11461 + ( VertexPosOtherAxis82_g11461 * cos( Angle44_g11461 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g11461 ) * sin( Angle44_g11461 ) ) );
			half3 Vertex_Motion833_g11117 = Vertex_Motion_Main595_g11117;
			float3 temp_output_689_0_g11117 = ( lerpResult348_g11117 * Vertex_Motion833_g11117 );
			half3 localObjectPosition_unity_ObjectToWorld8_g11484 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11485 = localObjectPosition_unity_ObjectToWorld8_g11484;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11485 = temp_output_7_0_g11485;
			#else
				float3 staticSwitch8_g11485 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11485 = staticSwitch8_g11485;
			#else
				float3 staticSwitch10_g11485 = temp_output_7_0_g11485;
			#endif
			float4 tex2DNode7_g11483 = tex2Dlod( TVE_ExtrasTex, float4( ( (TVE_ExtrasCoord).xy + ( TVE_ExtrasCoord.z * (staticSwitch10_g11485).xz ) ), 0, 0.0) );
			float3 linearToGamma9_g11483 = LinearToGammaSpace( tex2DNode7_g11483.rgb );
			#ifdef UNITY_COLORSPACE_GAMMA
				float3 staticSwitch13_g11483 = (tex2DNode7_g11483).rgb;
			#else
				float3 staticSwitch13_g11483 = linearToGamma9_g11483;
			#endif
			float3 break20_g11483 = staticSwitch13_g11483;
			half Global_ExtrasTex_G305_g11117 = break20_g11483.y;
			half Global_Size693_g11117 = _GlobalSize;
			float lerpResult346_g11117 = lerp( 1.0 , Global_ExtrasTex_G305_g11117 , Global_Size693_g11117);
			half3 Vertex_Position601_g11117 = ( temp_output_689_0_g11117 * lerpResult346_g11117 );
			float3 temp_output_7_0_g11522 = Vertex_Position601_g11117;
			float3 appendResult1094_g11117 = (float3(Motion_X216_g11117 , 0.0 , Motion_Z190_g11117));
			half3 Vertex_Motion_Main_Batched1096_g11117 = ( ase_vertex3Pos + appendResult1094_g11117 );
			half3 Vertex_Motion_Batched1118_g11117 = Vertex_Motion_Main_Batched1096_g11117;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11522 = temp_output_7_0_g11522;
			#else
				float3 staticSwitch8_g11522 = Vertex_Motion_Batched1118_g11117;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11522 = staticSwitch8_g11522;
			#else
				float3 staticSwitch10_g11522 = temp_output_7_0_g11522;
			#endif
			half3 Final_Vertex890_g11117 = ( staticSwitch10_g11522 + ( _IsStandardPipeline * 0.0 ) );
			v.vertex.xyz = Final_Vertex890_g11117;
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			half2 Main_UVs15_g11117 = ( ( i.uv_texcoord * (_MainUVs).xy ) + (_MainUVs).zw );
			half3 Main_Normal137_g11117 = UnpackScaleNormal( tex2D( _MainNormalTex, Main_UVs15_g11117 ), _MainNormalValue );
			float3 temp_output_13_0_g11518 = Main_Normal137_g11117;
			float3 switchResult12_g11518 = (((i.ASEVFace>0)?(temp_output_13_0_g11518):(( temp_output_13_0_g11518 * __normalsoptions ))));
			half3 Blend_Normal312_g11117 = switchResult12_g11518;
			half3 localObjectPosition_unity_ObjectToWorld8_g11514 = ObjectPosition_unity_ObjectToWorld();
			float2 temp_output_38_0_g11512 = ( ( i.uv_texcoord + (localObjectPosition_unity_ObjectToWorld8_g11514).xz ) * TVE_OverlayUVTilling * _OverlayUVTilling );
			float3 temp_output_13_0_g11513 = UnpackScaleNormal( tex2D( TVE_OverlayNormalTex, temp_output_38_0_g11512 ), TVE_OverlayNormalValue );
			float3 switchResult12_g11513 = (((i.ASEVFace>0)?(temp_output_13_0_g11513):(( temp_output_13_0_g11513 * __normalsoptions ))));
			half3 Global_OverlayNormal313_g11117 = switchResult12_g11513;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			half Bounds_Height374_g11117 = _MaxBoundsInfo.y;
			half Global_OverlayIntensity154_g11117 = TVE_OverlayIntensity;
			half3 localObjectPosition_unity_ObjectToWorld8_g11484 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11485 = localObjectPosition_unity_ObjectToWorld8_g11484;
			float3 ase_worldPos = i.worldPos;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11485 = temp_output_7_0_g11485;
			#else
				float3 staticSwitch8_g11485 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11485 = staticSwitch8_g11485;
			#else
				float3 staticSwitch10_g11485 = temp_output_7_0_g11485;
			#endif
			float4 tex2DNode7_g11483 = tex2D( TVE_ExtrasTex, ( (TVE_ExtrasCoord).xy + ( TVE_ExtrasCoord.z * (staticSwitch10_g11485).xz ) ) );
			float3 linearToGamma9_g11483 = LinearToGammaSpace( tex2DNode7_g11483.rgb );
			#ifdef UNITY_COLORSPACE_GAMMA
				float3 staticSwitch13_g11483 = (tex2DNode7_g11483).rgb;
			#else
				float3 staticSwitch13_g11483 = linearToGamma9_g11483;
			#endif
			float3 break20_g11483 = staticSwitch13_g11483;
			half Global_ExtrasTex_B156_g11117 = break20_g11483.z;
			float temp_output_1025_0_g11117 = ( Global_OverlayIntensity154_g11117 * _GlobalOverlay * Global_ExtrasTex_B156_g11117 );
			half Mask_Overlay269_g11117 = saturate( ( saturate( ( ( ase_vertex3Pos.y / Bounds_Height374_g11117 ) * ( (WorldNormalVector( i , Main_Normal137_g11117 )).y * 2.0 ) ) ) - ( 1.0 - temp_output_1025_0_g11117 ) ) );
			float3 lerpResult349_g11117 = lerp( Blend_Normal312_g11117 , Global_OverlayNormal313_g11117 , Mask_Overlay269_g11117);
			half3 Final_Normal366_g11117 = lerpResult349_g11117;
			o.Normal = Final_Normal366_g11117;
			half Mesh_Variation16_g11117 = i.vertexColor.a;
			float4 lerpResult26_g11117 = lerp( _MainColorVariation , _MainColor , Mesh_Variation16_g11117);
			float4 tex2DNode29_g11117 = tex2D( _MainAlbedoTex, Main_UVs15_g11117 );
			float4 temp_output_51_0_g11117 = ( lerpResult26_g11117 * tex2DNode29_g11117 );
			half3 Main_AlbedoRaw99_g11117 = (temp_output_51_0_g11117).rgb;
			float3 temp_cast_2 = (1.0).xxx;
			half3 localObjectPosition_unity_ObjectToWorld8_g11508 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11509 = localObjectPosition_unity_ObjectToWorld8_g11508;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11509 = temp_output_7_0_g11509;
			#else
				float3 staticSwitch8_g11509 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11509 = staticSwitch8_g11509;
			#else
				float3 staticSwitch10_g11509 = temp_output_7_0_g11509;
			#endif
			#ifdef UNITY_COLORSPACE_GAMMA
				float staticSwitch1_g11507 = 2.0;
			#else
				float staticSwitch1_g11507 = 4.594794;
			#endif
			float3 lerpResult108_g11117 = lerp( temp_cast_2 , ( (tex2D( TVE_ColorsTex, ( (TVE_ColorsCoord).xy + ( TVE_ColorsCoord.z * (staticSwitch10_g11509).xz ) ) )).rgb * staticSwitch1_g11507 ) , _GlobalColors);
			half3 Main_AlbedoColored863_g11117 = ( Main_AlbedoRaw99_g11117 * lerpResult108_g11117 );
			float4 tex2DNode35_g11117 = tex2D( _MainMaskTex, Main_UVs15_g11117 );
			half Main_Mask57_g11117 = tex2DNode35_g11117.b;
			float temp_output_7_0_g11501 = _SubsurfaceMinValue;
			half AutoRegister_MaterialShadingSpace1208_g11117 = _MaterialShadingSpaceDrawer;
			float temp_output_1345_0_g11117 = saturate( ( ( Main_Mask57_g11117 - temp_output_7_0_g11501 ) / ( ( _SubsurfaceMaxValue + AutoRegister_MaterialShadingSpace1208_g11117 ) - temp_output_7_0_g11501 ) ) );
			half3 Subsurface_Color884_g11117 = ( (_SubsurfaceColor).rgb * ( temp_output_1345_0_g11117 * _SubsurfaceValue ) );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 normalizeResult931_g11117 = normalize( ase_vertex3Pos );
			float dotResult933_g11117 = dot( ase_worldlightDir , normalizeResult931_g11117 );
			float temp_output_934_0_g11117 = (dotResult933_g11117*0.25 + 0.25);
			half Mask_Subsurface_Static935_g11117 = ( temp_output_934_0_g11117 * temp_output_934_0_g11117 );
			half3 Main_Albedo149_g11117 = ( Main_AlbedoColored863_g11117 + ( Subsurface_Color884_g11117 * saturate( ase_lightColor.rgb ) * Mask_Subsurface_Static935_g11117 ) );
			half3 Blend_Albedo265_g11117 = Main_Albedo149_g11117;
			float4 tex2DNode30_g11512 = tex2D( TVE_OverlayAlbedoTex, temp_output_38_0_g11512 );
			half3 Global_OverlayAlbedo277_g11117 = ( (TVE_OverlayColor).rgb * (tex2DNode30_g11512).rgb );
			float3 lerpResult336_g11117 = lerp( Blend_Albedo265_g11117 , Global_OverlayAlbedo277_g11117 , Mask_Overlay269_g11117);
			half3 Final_Albedo359_g11117 = lerpResult336_g11117;
			half Main_Alpha316_g11117 = (temp_output_51_0_g11117).a;
			float lerpResult354_g11117 = lerp( 1.0 , Main_Alpha316_g11117 , __premul);
			half Final_Premultiply355_g11117 = lerpResult354_g11117;
			o.Albedo = ( Final_Albedo359_g11117 * Final_Premultiply355_g11117 );
			half Global_Wetness1016_g11117 = ( TVE_Wetness * _GlobalWetness );
			half Global_ExtrasTex_A1033_g11117 = tex2DNode7_g11483.a;
			float lerpResult1037_g11117 = lerp( _ObjectSmoothnessValue , saturate( ( _ObjectSmoothnessValue + Global_Wetness1016_g11117 ) ) , Global_ExtrasTex_A1033_g11117);
			float temp_output_164_0 = max( lerpResult1037_g11117 , 0.01 );
			o.Specular = temp_output_164_0;
			o.Gloss = temp_output_164_0;
			o.Alpha = Main_Alpha316_g11117;
			half Main_AlphaRaw1203_g11117 = tex2DNode29_g11117.a;
			half Alpha5_g11524 = Main_AlphaRaw1203_g11117;
			#ifdef _ALPHATEST_ON
				float staticSwitch2_g11524 = Alpha5_g11524;
			#else
				float staticSwitch2_g11524 = 1.0;
			#endif
			half Final_Clip914_g11117 = staticSwitch2_g11524;
			clip( Final_Clip914_g11117 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "TVEShaderCoreGUI"
}
/*ASEBEGIN
Version=17802
1927;1;1906;1020;2842.659;497.052;1;True;False
Node;AmplifyShaderEditor.FunctionNode;165;-2175,-128;Inherit;False;Base;4;;11117;856f7164d1c579d43a5cf4968a75ca43;20,1034,1,860,1,1000,1,995,1,847,0,1114,0,850,1,853,0,1271,0,1298,0,1300,0,866,1,844,0,883,0,879,0,881,0,878,0,888,0,893,0,916,1;0;16;FLOAT3;0;FLOAT3;528;FLOAT;529;FLOAT;530;FLOAT;531;FLOAT;1235;FLOAT3;1230;FLOAT;1290;FLOAT;721;FLOAT;532;FLOAT;653;FLOAT;892;FLOAT3;534;FLOAT4;535;FLOAT;629;FLOAT;722
Node;AmplifyShaderEditor.RangedFloatNode;20;-1888,-640;Half;False;Property;__src;__src;155;1;[HideInInspector];Fetch;False;0;0;True;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-2016,-640;Half;False;Property;__cull;__cull;154;2;[HideInInspector];[Enum];Fetch;False;3;Both;0;Back;1;Front;2;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-1760,-640;Half;False;Property;__dst;__dst;156;1;[HideInInspector];Fetch;False;2;Opaque;0;Transparent;1;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;162;-1888,-512;Inherit;False;Property;_SubsurfaceMessageUniversal2;!!! Subsurface Message Universal !!!;2;0;Create;False;0;0;False;1;StyledMessage(Info, In Universal Render Pipeline the Subsurface only work for the Directional light. Other light types are not supported, 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;161;-2176,-512;Inherit;False;Property;_SubsurfaceMessageStandard2;!!! Subsurface Message !!!;1;0;Create;False;0;0;True;1;StyledMessage(Info, Subsurface shading requires Directional light and Ambient light higher than 0 to work. Other light types are not supported., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-2176,-640;Half;False;Property;_Cutoff;_Cutoff;153;1;[HideInInspector];Fetch;False;4;Alpha;0;Premultiply;1;Additive;2;Multiply;3;0;True;0;0.5;0.719;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;160;-1584,-512;Inherit;False;Property;_SubsurfaceMessageHD2;!!! Subsurface Message HD !!!;3;0;Create;False;0;0;False;1;StyledMessage(Info, Diffusion Profiles are not directly supported. You need to create an HDRP Lit material and assign a Diffusion Profile then drag it to the Subsurface Diffusion Material slot., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-2176,-768;Half;False;Property;_Banner;Banner;0;0;Create;True;0;0;True;1;StyledBanner(Cross Simple Lit);0;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1632,-640;Half;False;Property;__zw;__zw;157;1;[HideInInspector];Fetch;False;2;Opaque;0;Transparent;1;0;True;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;164;-1536,0;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.01;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;129;-1808,-768;Half;False;Property;_IsSimpleShader;_IsSimpleShader;150;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;81;-2032,-768;Half;False;Property;_IsCrossShader;_IsCrossShader;152;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;141;-1568,-768;Half;False;Property;_IsLitShader;_IsLitShader;151;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-1376,-128;Float;False;True;-1;2;TVEShaderCoreGUI;0;0;BlinnPhong;BOXOPHOBIC/The Vegetation Engine/Billboards/Cross Simple Lit;False;False;False;False;False;True;False;True;False;False;False;False;False;True;True;False;True;False;False;False;True;Back;0;True;17;0;False;-1;True;0;False;-1;-1;False;-1;False;0;Custom;0.719;True;True;0;True;Opaque;;Geometry;All;11;d3d11;glcore;gles3;metal;vulkan;xbox360;xboxone;ps4;psp2;n3ds;wiiu;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;1;0;True;20;0;True;7;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Absolute;0;;-1;-1;-1;-1;0;False;0;0;True;10;158;0;True;21;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;33;-2176,-256;Inherit;False;1024.392;100;Final;0;;0.3439424,0.5960785,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;37;-2176,-896;Inherit;False;1023.392;100;Internal;0;;1,0.252,0,1;0;0
WireConnection;164;0;165;530
WireConnection;0;0;165;0
WireConnection;0;1;165;528
WireConnection;0;3;164;0
WireConnection;0;4;164;0
WireConnection;0;9;165;532
WireConnection;0;10;165;653
WireConnection;0;11;165;534
ASEEND*/
//CHKSM=8DC059F3B2075E8438AF1D0D62BB25ED85E223C0