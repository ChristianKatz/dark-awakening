// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BOXOPHOBIC/The Vegetation Engine/Billboards/Cross Standard Lit"
{
	Properties
	{
		[StyledBanner(Cross Standard Lit)]_Banner("Banner", Float) = 0
		[StyledMessage(Info, Subsurface shading requires Directional light and Ambient light higher than 0 to work. Other light types are not supported., 5, 5 )]_SubsurfaceMessageStandard1("!!! Subsurface Message !!!", Float) = 0
		[HideInInspector]_IsVersion("_IsVersion", Float) = 110
		[HideInInspector]_IsStandardPipeline("_IsStandardPipeline", Float) = 1
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector][Toggle]_IsUpdated("_IsUpdated", Float) = 0
		[HideInInspector]_VariationMode("_VariationMode", Float) = -1
		[HideInInspector]_MaxBoundsInfo("_MaxBoundsInfo", Vector) = (1,1,1,1)
		[StyledCategory(Render Settings)]_RenderingCat("[ Rendering Cat ]", Float) = 0
		[Enum(Opaque,0,Transparent,1)]__surface("Render Mode", Float) = 0
		[Enum(Both,0,Back,1,Front,2)]__cull("Render Faces", Float) = 0
		[Enum(Flip,0,Mirror,1,None,2)]__normals("Render Normals", Float) = 0
		[StyledInteractive(__surface, 1)]_RenderMode_TransparentInteractive("# RenderMode_TransparentInteractive", Float) = 0
		[Enum(Alpha,0,Premultiply,1)]__blend("Render Blending", Float) = 0
		[Enum(Off,0,On,1)]__zw("Render ZWrite", Float) = 1
		[StyledInteractive(ON)]_RenderMode_ResetInteractive("# RenderMode_ResetInteractive", Float) = 0
		[Toggle][Space(10)]__clip("Alpha Clipping", Float) = 0
		[StyledInteractive(__clip, 1)]_AlphaClipInteractive("# AlphaClipInteractive", Float) = 0
		_Cutoff("Alpha Treshold", Range( 0 , 1)) = 0.5
		[HideInInspector]__cliptreshold("__cliptreshold", Float) = 0
		[StyledCategory(Global Settings)]_GlobalSettingsCat("[ Global Settings Cat ]", Float) = 0
		_GlobalColors("Global Colors", Range( 0 , 1)) = 1
		[HideInInspector]_GlobalOverlayMode("Global Overlay Mode", Float) = 1
		_GlobalOverlay("Global Overlay", Range( 0 , 1)) = 1
		_GlobalWetness("Global Wetness", Range( 0 , 1)) = 1
		_GlobalSize("Global Size", Range( 0 , 1)) = 1
		_GlobalSizeFade("Global Size Fade", Range( 0 , 1)) = 1
		[StyledCategory(Material Shading)]_MaterialShadingCat("[ Material Shading Cat ]", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Subsurface Mask. Use the Min Max sliders to adjust or invert the subsurface effect., _IsLeafShader, 1, 5, 10 )]_MessageSubsurfaceMask("!!! Message Leaf Subsurface Mask !!!", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Subsurface Mask. Use the Min Max sliders to adjust or invert the subsurface effect., _IsCrossShader, 1, 5, 10 )]_MessageCrossSubsurfaceMask("!!! Message Cross Subsurface Mask !!!", Float) = 0
		[StyledMessage(Info, The Main Mask B channel is used as Height Map. Use the Min Max values to adjust or invert the height., _IsTessellationShader, 1, 5, 10 )]_MessageHeightMap("!!! Message Height Map !!!", Float) = 0
		[HDR]_SubsurfaceColor("Subsurface Color", Color) = (0.3315085,0.490566,0,1)
		_SubsurfaceValue("Subsurface Intensity", Range( 0 , 1)) = 1
		_SubsurfaceMinValue("Subsurface Min", Range( 0 , 1)) = 0
		_SubsurfaceMaxValue("Subsurface Max", Range( 0 , 1)) = 1
		[StyledSpace(10)]_MaterialShadingSpaceDrawer("# Material Shading Space", Float) = 0
		[Space(10)]_OverlayUVTilling("Overlay Tilling", Range( 0 , 10)) = 1
		[StyledCategory(Main Shading)]_MainShadingCat("[ Main Shading Cat ]", Float) = 0
		[Enum(Constant,0,Variation,1)]_MainColorMode("Main Color Mode", Float) = 1
		[Space(10)]_MainColor("Main Color One", Color) = (1,1,1,1)
		_MainColorVariation("Main Color Two", Color) = (1,1,1,1)
		[NoScaleOffset]_MainAlbedoTex("Main Albedo", 2D) = "white" {}
		[NoScaleOffset]_MainNormalTex("Main Normal", 2D) = "bump" {}
		[NoScaleOffset]_MainMaskTex("Main Mask", 2D) = "white" {}
		[Space(10)]_MainUVs("Main UVs", Vector) = (1,1,0,0)
		_MainNormalValue("Main Normal", Range( -8 , 8)) = 1
		_MainMetallicValue("Main Metallic (R)", Range( 0 , 1)) = 0
		_MainSmoothnessValue("Main Smoothness (A)", Range( 0 , 1)) = 1
		[StyledCategory(Motion Settings)]_MotionCat("[ Motion Cat ]", Float) = 0
		[HideInInspector][Enum(Off,0,Procedural,1,Hierarchical,2)]_MotionMode("Motion Mode", Float) = 0
		[HideInInspector][Enum(Object Pivot,0,Element Pivot,1)]_PivotMode("Pivot Mode", Float) = 0
		[Toggle]_UseMotion_Main("Motion Main", Float) = 1
		[HideInInspector][Space(10)]_MotionAmplitude_10("Primary Bending", Float) = 0
		[HideInInspector][IntRange]_MotionSpeed_10("Primary Speed", Float) = 2
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector]_MotionScale_10("Primary Elasticity", Float) = 0
		[HideInInspector]_MotionVariation_10("Primary Variation", Float) = 0
		[HideInInspector][Space(10)]_InteractionAmplitude("Interaction Bending", Float) = 0
		[StyledCategory(Advanced)]_AdvancedCat("[ Advanced Cat]", Float) = 0
		[IntRange]__priority("Priority", Range( -50 , 50)) = 0
		[HideInInspector]__src("__src", Float) = 1
		[HideInInspector]__dst("__dst", Float) = 0
		[HideInInspector]__premul("__premul", Float) = 0
		[HideInInspector]_Color("_Color", Color) = (0,0,0,0)
		[HideInInspector]_MainTex("_MainTex", 2D) = "white" {}
		[HideInInspector]_VertexOcclusion("_VertexOcclusion", Float) = 0
		[HideInInspector]_MainMaskValue("_MainMaskValue", Float) = 0
		[HideInInspector][Enum(Translucency,0,Thickness,1)]_SubsurfaceMode("_SubsurfaceMode", Float) = 0
		[HideInInspector]_ObjectThicknessValue("_ObjectThicknessValue", Float) = 0
		[HideInInspector]__normalsoptions("__normalsoptions", Vector) = (1,1,1,0)
		[HideInInspector]_IsStandardShader("_IsStandardShader", Float) = 1
		[HideInInspector]_IsLitShader("_IsLitShader", Float) = 1
		[HideInInspector]_IsCrossShader("_IsCrossShader", Float) = 1
		[HideInInspector]_Cutoff("_Cutoff", Float) = 0.5
		[HideInInspector][Enum(Both,0,Back,1,Front,2)]__cull("__cull", Float) = 0
		[HideInInspector]__zw("__zw", Float) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" "DisableBatching" = "True" }
		Cull [__cull]
		ZWrite [__zw]
		Offset  0 , -1
		Blend [__src] [__dst]
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma shader_feature _ALPHATEST_ON
		  
		//FEATURES BEGIN
		//FEATURES END
		    
		#define TVE_DISABLE_UNSAFE_BATCHING
		#pragma exclude_renderers d3d9 d3d11_9x gles 
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows novertexlights nodynlightmap dithercrossfade vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			half ASEVFace : VFACE;
			float3 worldNormal;
			INTERNAL_DATA
			float4 vertexColor : COLOR;
		};

		uniform half _IsStandardShader;
		uniform half _IsLitShader;
		uniform float4 _MaxBoundsInfo;
		uniform half _AdvancedCat;
		uniform half __surface;
		uniform half _PivotMode;
		uniform half __zw;
		uniform half _GlobalSettingsCat;
		uniform half _IsTVEShader;
		uniform half _IsUpdated;
		uniform half _MaterialShadingCat;
		uniform half __blend;
		uniform half __src;
		uniform half _Cutoff;
		uniform half __dst;
		uniform half _RenderMode_ResetInteractive;
		uniform half _MessageHeightMap;
		uniform half _MessageCrossSubsurfaceMask;
		uniform float _SubsurfaceMode;
		uniform half _MainMaskValue;
		uniform half _VertexOcclusion;
		uniform half _MainColorMode;
		uniform half _RenderMode_TransparentInteractive;
		uniform half _GlobalOverlayMode;
		uniform half __cull;
		uniform half __clip;
		uniform sampler2D _MainTex;
		uniform half __normals;
		uniform half _VariationMode;
		uniform float4 _Color;
		uniform half _RenderingCat;
		uniform half _IsVersion;
		uniform half __cliptreshold;
		uniform half __priority;
		uniform half _MotionMode;
		uniform half _MainShadingCat;
		uniform half _AlphaClipInteractive;
		uniform half _ObjectThicknessValue;
		uniform half _MessageSubsurfaceMask;
		uniform half _Banner;
		uniform half _IsCrossShader;
		uniform float _SubsurfaceMessageStandard1;
		uniform half TVE_SizeFadeEnd;
		uniform half TVE_SizeFadeStart;
		uniform half _GlobalSizeFade;
		uniform half _MotionAmplitude_10;
		uniform half TVE_Amplitude1;
		uniform sampler2D TVE_NoiseTex;
		uniform half TVE_NoiseSpeed;
		uniform half TVE_NoiseSize;
		uniform half TVE_NoiseContrast;
		uniform sampler2D TVE_MotionTex;
		uniform half4 TVE_MotionCoord;
		uniform half _MotionSpeed_10;
		uniform half _MotionVariation_10;
		uniform half _MotionScale_10;
		uniform float _InteractionAmplitude;
		uniform half _UseMotion_Main;
		uniform half _MotionCat;
		uniform sampler2D TVE_ExtrasTex;
		uniform half4 TVE_ExtrasCoord;
		uniform half _GlobalSize;
		uniform half _IsStandardPipeline;
		uniform half _MainNormalValue;
		uniform sampler2D _MainNormalTex;
		uniform half4 _MainUVs;
		uniform half3 __normalsoptions;
		uniform half TVE_OverlayNormalValue;
		uniform sampler2D TVE_OverlayNormalTex;
		uniform half TVE_OverlayUVTilling;
		uniform half _OverlayUVTilling;
		uniform half TVE_OverlayIntensity;
		uniform half _GlobalOverlay;
		uniform half4 _MainColorVariation;
		uniform half4 _MainColor;
		uniform sampler2D _MainAlbedoTex;
		uniform sampler2D TVE_ColorsTex;
		uniform half4 TVE_ColorsCoord;
		uniform half _GlobalColors;
		uniform half4 _SubsurfaceColor;
		uniform sampler2D _MainMaskTex;
		uniform half _SubsurfaceMinValue;
		uniform half _SubsurfaceMaxValue;
		uniform half _MaterialShadingSpaceDrawer;
		uniform half _SubsurfaceValue;
		uniform half4 TVE_OverlayColor;
		uniform sampler2D TVE_OverlayAlbedoTex;
		uniform half __premul;
		uniform half _MainMetallicValue;
		uniform half _MainSmoothnessValue;
		uniform half TVE_OverlaySmoothness;
		uniform float TVE_Wetness;
		uniform half _GlobalWetness;


		half3 ObjectPosition_unity_ObjectToWorld(  )
		{
			return half3(unity_ObjectToWorld[0].w, unity_ObjectToWorld[1].w, unity_ObjectToWorld[2].w );
		}


		float3 DecodeFloatToVector3( int enc )
		{
			  float3 result ;
			  result.x = ((enc >> 0) & 0xFF) / 255.0f;
			  result.y = ((enc >> 8) & 0xFF) / 255.0;
			  result.z = ((enc >>16) & 0xFF) / 255.0;
			  //result.w = ((enc >> 32) & 0xFF) / 255.0;
			  return result;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			half3 localObjectPosition_unity_ObjectToWorld8_g11549 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_731_7_g11538 = localObjectPosition_unity_ObjectToWorld8_g11549;
			half Global_SizeFadeEnd287_g11538 = TVE_SizeFadeEnd;
			float temp_output_7_0_g11583 = Global_SizeFadeEnd287_g11538;
			half Global_SizeFadeStart276_g11538 = TVE_SizeFadeStart;
			half Global_SizeFade694_g11538 = _GlobalSizeFade;
			float lerpResult348_g11538 = lerp( 1.0 , saturate( ( ( distance( _WorldSpaceCameraPos , temp_output_731_7_g11538 ) - temp_output_7_0_g11583 ) / ( Global_SizeFadeStart276_g11538 - temp_output_7_0_g11583 ) ) ) , Global_SizeFade694_g11538);
			float3 ase_vertex3Pos = v.vertex.xyz;
			half3 VertexPos40_g11584 = ase_vertex3Pos;
			float3 appendResult74_g11584 = (float3(VertexPos40_g11584.x , 0.0 , 0.0));
			half3 VertexPosRotationAxis50_g11584 = appendResult74_g11584;
			float3 break84_g11584 = VertexPos40_g11584;
			float3 appendResult81_g11584 = (float3(0.0 , break84_g11584.y , break84_g11584.z));
			half3 VertexPosOtherAxis82_g11584 = appendResult81_g11584;
			float temp_output_7_0_g11552 = UNITY_PI;
			half Bounds_Height374_g11538 = _MaxBoundsInfo.y;
			#ifdef UNITY_INSTANCING_ENABLED
				float staticSwitch8_g11552 = temp_output_7_0_g11552;
			#else
				float staticSwitch8_g11552 = ( Bounds_Height374_g11538 * UNITY_PI );
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float staticSwitch10_g11552 = staticSwitch8_g11552;
			#else
				float staticSwitch10_g11552 = temp_output_7_0_g11552;
			#endif
			half Motion_Max_Bending1133_g11538 = staticSwitch10_g11552;
			half Global_Amplitude_136_g11538 = TVE_Amplitude1;
			float temp_output_4_0_g11569 = 1.0;
			float mulTime2_g11569 = _Time.y * temp_output_4_0_g11569;
			float2 temp_cast_0 = (TVE_NoiseSpeed).xx;
			half3 localObjectPosition_unity_ObjectToWorld8_g11568 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11567 = localObjectPosition_unity_ObjectToWorld8_g11568;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11567 = temp_output_7_0_g11567;
			#else
				float3 staticSwitch8_g11567 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11567 = staticSwitch8_g11567;
			#else
				float3 staticSwitch10_g11567 = temp_output_7_0_g11567;
			#endif
			float2 panner73_g11566 = ( mulTime2_g11569 * temp_cast_0 + ( (staticSwitch10_g11567).xz * TVE_NoiseSize ));
			float4 temp_cast_1 = (TVE_NoiseContrast).xxxx;
			float4 break142_g11566 = pow( abs( tex2Dlod( TVE_NoiseTex, float4( panner73_g11566, 0, 0.0) ) ) , temp_cast_1 );
			half Global_NoiseTex_R34_g11538 = break142_g11566.r;
			half3 localObjectPosition_unity_ObjectToWorld8_g11560 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11561 = localObjectPosition_unity_ObjectToWorld8_g11560;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11561 = temp_output_7_0_g11561;
			#else
				float3 staticSwitch8_g11561 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11561 = staticSwitch8_g11561;
			#else
				float3 staticSwitch10_g11561 = temp_output_7_0_g11561;
			#endif
			float3 temp_output_18_0_g11559 = (tex2Dlod( TVE_MotionTex, float4( ( (TVE_MotionCoord).xy + ( TVE_MotionCoord.z * (staticSwitch10_g11561).xz ) ), 0, 0.0) )).rgb;
			float3 linearToGamma28_g11559 = LinearToGammaSpace( temp_output_18_0_g11559 );
			float3 break322_g11558 = linearToGamma28_g11559;
			float3 appendResult323_g11558 = (float3(break322_g11558.x , 0.0 , break322_g11558.y));
			float3 temp_output_324_0_g11558 = (appendResult323_g11558*2.0 + -1.0);
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float3 temp_output_7_0_g11562 = ( mul( unity_WorldToObject, float4( temp_output_324_0_g11558 , 0.0 ) ).xyz * ase_objectScale );
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11562 = temp_output_7_0_g11562;
			#else
				float3 staticSwitch8_g11562 = temp_output_324_0_g11558;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11562 = staticSwitch8_g11562;
			#else
				float3 staticSwitch10_g11562 = temp_output_7_0_g11562;
			#endif
			float2 temp_output_1196_320_g11538 = (staticSwitch10_g11562).xz;
			half2 Motion_DirectionOS39_g11538 = temp_output_1196_320_g11538;
			half Input_Speed62_g11570 = _MotionSpeed_10;
			float temp_output_4_0_g11573 = Input_Speed62_g11570;
			float mulTime2_g11573 = _Time.y * temp_output_4_0_g11573;
			half3 localObjectPosition_unity_ObjectToWorld8_g11571 = ObjectPosition_unity_ObjectToWorld();
			float3 break9_g11571 = localObjectPosition_unity_ObjectToWorld8_g11571;
			half Motion_Variation284_g11570 = ( ( _MotionVariation_10 * v.color.a ) + ( break9_g11571.x + break9_g11571.z ) );
			float2 appendResult344_g11570 = (float2(ase_worldPos.x , ase_worldPos.z));
			float2 Motion_Scale287_g11570 = ( _MotionScale_10 * appendResult344_g11570 );
			half2 Sine_MinusOneToOne281_g11570 = sin( ( mulTime2_g11573 + Motion_Variation284_g11570 + Motion_Scale287_g11570 ) );
			float2 temp_cast_4 = (1.0).xx;
			half Input_Turbulence327_g11570 = Global_NoiseTex_R34_g11538;
			float2 lerpResult321_g11570 = lerp( Sine_MinusOneToOne281_g11570 , temp_cast_4 , Input_Turbulence327_g11570);
			half2 Motion_Interaction53_g11538 = ( _InteractionAmplitude * Motion_Max_Bending1133_g11538 * temp_output_1196_320_g11538 );
			half Motion_InteractionMask66_g11538 = break322_g11558.z;
			float2 lerpResult109_g11538 = lerp( ( ( _MotionAmplitude_10 * Motion_Max_Bending1133_g11538 ) * Global_Amplitude_136_g11538 * Global_NoiseTex_R34_g11538 * Motion_DirectionOS39_g11538 * lerpResult321_g11570 ) , Motion_Interaction53_g11538 , Motion_InteractionMask66_g11538);
			int enc15_g11574 = (int)v.texcoord3.x;
			float3 localDecodeFloatToVector315_g11574 = DecodeFloatToVector3( enc15_g11574 );
			float3 break17_g11574 = localDecodeFloatToVector315_g11574;
			half Mesh_Motion_182_g11538 = break17_g11574.x;
			half Motion_UseMain56_g11538 = ( _UseMotion_Main + ( _MotionCat * 0.0 ) );
			float2 break143_g11538 = ( lerpResult109_g11538 * Mesh_Motion_182_g11538 * Motion_UseMain56_g11538 );
			half Motion_Z190_g11538 = break143_g11538.y;
			half Angle44_g11584 = Motion_Z190_g11538;
			half3 VertexPos40_g11541 = ( VertexPosRotationAxis50_g11584 + ( VertexPosOtherAxis82_g11584 * cos( Angle44_g11584 ) ) + ( cross( float3(1,0,0) , VertexPosOtherAxis82_g11584 ) * sin( Angle44_g11584 ) ) );
			float3 appendResult74_g11541 = (float3(0.0 , 0.0 , VertexPos40_g11541.z));
			half3 VertexPosRotationAxis50_g11541 = appendResult74_g11541;
			float3 break84_g11541 = VertexPos40_g11541;
			float3 appendResult81_g11541 = (float3(break84_g11541.x , break84_g11541.y , 0.0));
			half3 VertexPosOtherAxis82_g11541 = appendResult81_g11541;
			half Motion_X216_g11538 = break143_g11538.x;
			half Angle44_g11541 = -Motion_X216_g11538;
			half3 Vertex_Motion_Main595_g11538 = ( VertexPosRotationAxis50_g11541 + ( VertexPosOtherAxis82_g11541 * cos( Angle44_g11541 ) ) + ( cross( float3(0,0,1) , VertexPosOtherAxis82_g11541 ) * sin( Angle44_g11541 ) ) );
			half3 Vertex_Motion833_g11538 = Vertex_Motion_Main595_g11538;
			float3 temp_output_689_0_g11538 = ( lerpResult348_g11538 * Vertex_Motion833_g11538 );
			half3 localObjectPosition_unity_ObjectToWorld8_g11564 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11565 = localObjectPosition_unity_ObjectToWorld8_g11564;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11565 = temp_output_7_0_g11565;
			#else
				float3 staticSwitch8_g11565 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11565 = staticSwitch8_g11565;
			#else
				float3 staticSwitch10_g11565 = temp_output_7_0_g11565;
			#endif
			float4 tex2DNode7_g11563 = tex2Dlod( TVE_ExtrasTex, float4( ( (TVE_ExtrasCoord).xy + ( TVE_ExtrasCoord.z * (staticSwitch10_g11565).xz ) ), 0, 0.0) );
			float3 linearToGamma9_g11563 = LinearToGammaSpace( tex2DNode7_g11563.rgb );
			#ifdef UNITY_COLORSPACE_GAMMA
				float3 staticSwitch13_g11563 = (tex2DNode7_g11563).rgb;
			#else
				float3 staticSwitch13_g11563 = linearToGamma9_g11563;
			#endif
			float3 break20_g11563 = staticSwitch13_g11563;
			half Global_ExtrasTex_G305_g11538 = break20_g11563.y;
			half Global_Size693_g11538 = _GlobalSize;
			float lerpResult346_g11538 = lerp( 1.0 , Global_ExtrasTex_G305_g11538 , Global_Size693_g11538);
			half3 Vertex_Position601_g11538 = ( temp_output_689_0_g11538 * lerpResult346_g11538 );
			float3 temp_output_7_0_g11602 = Vertex_Position601_g11538;
			float3 appendResult1094_g11538 = (float3(Motion_X216_g11538 , 0.0 , Motion_Z190_g11538));
			half3 Vertex_Motion_Main_Batched1096_g11538 = ( ase_vertex3Pos + appendResult1094_g11538 );
			half3 Vertex_Motion_Batched1118_g11538 = Vertex_Motion_Main_Batched1096_g11538;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11602 = temp_output_7_0_g11602;
			#else
				float3 staticSwitch8_g11602 = Vertex_Motion_Batched1118_g11538;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11602 = staticSwitch8_g11602;
			#else
				float3 staticSwitch10_g11602 = temp_output_7_0_g11602;
			#endif
			half3 Final_Vertex890_g11538 = ( staticSwitch10_g11602 + ( _IsStandardPipeline * 0.0 ) );
			v.vertex.xyz = Final_Vertex890_g11538;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			half2 Main_UVs15_g11538 = ( ( i.uv_texcoord * (_MainUVs).xy ) + (_MainUVs).zw );
			half3 Main_Normal137_g11538 = UnpackScaleNormal( tex2D( _MainNormalTex, Main_UVs15_g11538 ), _MainNormalValue );
			float3 temp_output_13_0_g11598 = Main_Normal137_g11538;
			float3 switchResult12_g11598 = (((i.ASEVFace>0)?(temp_output_13_0_g11598):(( temp_output_13_0_g11598 * __normalsoptions ))));
			half3 Blend_Normal312_g11538 = switchResult12_g11598;
			half3 localObjectPosition_unity_ObjectToWorld8_g11594 = ObjectPosition_unity_ObjectToWorld();
			float2 temp_output_38_0_g11592 = ( ( i.uv_texcoord + (localObjectPosition_unity_ObjectToWorld8_g11594).xz ) * TVE_OverlayUVTilling * _OverlayUVTilling );
			float3 temp_output_13_0_g11593 = UnpackScaleNormal( tex2D( TVE_OverlayNormalTex, temp_output_38_0_g11592 ), TVE_OverlayNormalValue );
			float3 switchResult12_g11593 = (((i.ASEVFace>0)?(temp_output_13_0_g11593):(( temp_output_13_0_g11593 * __normalsoptions ))));
			half3 Global_OverlayNormal313_g11538 = switchResult12_g11593;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			half Bounds_Height374_g11538 = _MaxBoundsInfo.y;
			half Global_OverlayIntensity154_g11538 = TVE_OverlayIntensity;
			half3 localObjectPosition_unity_ObjectToWorld8_g11564 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11565 = localObjectPosition_unity_ObjectToWorld8_g11564;
			float3 ase_worldPos = i.worldPos;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11565 = temp_output_7_0_g11565;
			#else
				float3 staticSwitch8_g11565 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11565 = staticSwitch8_g11565;
			#else
				float3 staticSwitch10_g11565 = temp_output_7_0_g11565;
			#endif
			float4 tex2DNode7_g11563 = tex2D( TVE_ExtrasTex, ( (TVE_ExtrasCoord).xy + ( TVE_ExtrasCoord.z * (staticSwitch10_g11565).xz ) ) );
			float3 linearToGamma9_g11563 = LinearToGammaSpace( tex2DNode7_g11563.rgb );
			#ifdef UNITY_COLORSPACE_GAMMA
				float3 staticSwitch13_g11563 = (tex2DNode7_g11563).rgb;
			#else
				float3 staticSwitch13_g11563 = linearToGamma9_g11563;
			#endif
			float3 break20_g11563 = staticSwitch13_g11563;
			half Global_ExtrasTex_B156_g11538 = break20_g11563.z;
			float temp_output_1025_0_g11538 = ( Global_OverlayIntensity154_g11538 * _GlobalOverlay * Global_ExtrasTex_B156_g11538 );
			half Mask_Overlay269_g11538 = saturate( ( saturate( ( ( ase_vertex3Pos.y / Bounds_Height374_g11538 ) * ( (WorldNormalVector( i , Main_Normal137_g11538 )).y * 2.0 ) ) ) - ( 1.0 - temp_output_1025_0_g11538 ) ) );
			float3 lerpResult349_g11538 = lerp( Blend_Normal312_g11538 , Global_OverlayNormal313_g11538 , Mask_Overlay269_g11538);
			half3 Final_Normal366_g11538 = lerpResult349_g11538;
			o.Normal = Final_Normal366_g11538;
			half Mesh_Variation16_g11538 = i.vertexColor.a;
			float4 lerpResult26_g11538 = lerp( _MainColorVariation , _MainColor , Mesh_Variation16_g11538);
			float4 tex2DNode29_g11538 = tex2D( _MainAlbedoTex, Main_UVs15_g11538 );
			float4 temp_output_51_0_g11538 = ( lerpResult26_g11538 * tex2DNode29_g11538 );
			half3 Main_AlbedoRaw99_g11538 = (temp_output_51_0_g11538).rgb;
			float3 temp_cast_2 = (1.0).xxx;
			half3 localObjectPosition_unity_ObjectToWorld8_g11588 = ObjectPosition_unity_ObjectToWorld();
			float3 temp_output_7_0_g11589 = localObjectPosition_unity_ObjectToWorld8_g11588;
			#ifdef UNITY_INSTANCING_ENABLED
				float3 staticSwitch8_g11589 = temp_output_7_0_g11589;
			#else
				float3 staticSwitch8_g11589 = ase_worldPos;
			#endif
			#ifdef TVE_USE_UNSAFE_BATCHING
				float3 staticSwitch10_g11589 = staticSwitch8_g11589;
			#else
				float3 staticSwitch10_g11589 = temp_output_7_0_g11589;
			#endif
			#ifdef UNITY_COLORSPACE_GAMMA
				float staticSwitch1_g11587 = 2.0;
			#else
				float staticSwitch1_g11587 = 4.594794;
			#endif
			float3 lerpResult108_g11538 = lerp( temp_cast_2 , ( (tex2D( TVE_ColorsTex, ( (TVE_ColorsCoord).xy + ( TVE_ColorsCoord.z * (staticSwitch10_g11589).xz ) ) )).rgb * staticSwitch1_g11587 ) , _GlobalColors);
			half3 Main_AlbedoColored863_g11538 = ( Main_AlbedoRaw99_g11538 * lerpResult108_g11538 );
			float4 tex2DNode35_g11538 = tex2D( _MainMaskTex, Main_UVs15_g11538 );
			half Main_Mask57_g11538 = tex2DNode35_g11538.b;
			float temp_output_7_0_g11581 = _SubsurfaceMinValue;
			half AutoRegister_MaterialShadingSpace1208_g11538 = _MaterialShadingSpaceDrawer;
			float temp_output_1345_0_g11538 = saturate( ( ( Main_Mask57_g11538 - temp_output_7_0_g11581 ) / ( ( _SubsurfaceMaxValue + AutoRegister_MaterialShadingSpace1208_g11538 ) - temp_output_7_0_g11581 ) ) );
			half3 Subsurface_Color884_g11538 = ( (_SubsurfaceColor).rgb * ( temp_output_1345_0_g11538 * _SubsurfaceValue ) );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 normalizeResult931_g11538 = normalize( ase_vertex3Pos );
			float dotResult933_g11538 = dot( ase_worldlightDir , normalizeResult931_g11538 );
			float temp_output_934_0_g11538 = (dotResult933_g11538*0.25 + 0.25);
			half Mask_Subsurface_Static935_g11538 = ( temp_output_934_0_g11538 * temp_output_934_0_g11538 );
			half3 Main_Albedo149_g11538 = ( Main_AlbedoColored863_g11538 + ( Subsurface_Color884_g11538 * saturate( ase_lightColor.rgb ) * Mask_Subsurface_Static935_g11538 ) );
			half3 Blend_Albedo265_g11538 = Main_Albedo149_g11538;
			float4 tex2DNode30_g11592 = tex2D( TVE_OverlayAlbedoTex, temp_output_38_0_g11592 );
			half3 Global_OverlayAlbedo277_g11538 = ( (TVE_OverlayColor).rgb * (tex2DNode30_g11592).rgb );
			float3 lerpResult336_g11538 = lerp( Blend_Albedo265_g11538 , Global_OverlayAlbedo277_g11538 , Mask_Overlay269_g11538);
			half3 Final_Albedo359_g11538 = lerpResult336_g11538;
			half Main_Alpha316_g11538 = (temp_output_51_0_g11538).a;
			float lerpResult354_g11538 = lerp( 1.0 , Main_Alpha316_g11538 , __premul);
			half Final_Premultiply355_g11538 = lerpResult354_g11538;
			o.Albedo = ( Final_Albedo359_g11538 * Final_Premultiply355_g11538 );
			half Main_Metallic237_g11538 = ( tex2DNode35_g11538.r * _MainMetallicValue );
			half Blend_Metallic306_g11538 = Main_Metallic237_g11538;
			float lerpResult342_g11538 = lerp( Blend_Metallic306_g11538 , 0.0 , Mask_Overlay269_g11538);
			half Final_Metallic367_g11538 = lerpResult342_g11538;
			o.Metallic = Final_Metallic367_g11538;
			half Main_Smoothness227_g11538 = ( tex2DNode35_g11538.a * _MainSmoothnessValue );
			half Blend_Smoothness314_g11538 = Main_Smoothness227_g11538;
			half Global_OverlaySmoothness311_g11538 = TVE_OverlaySmoothness;
			float lerpResult343_g11538 = lerp( Blend_Smoothness314_g11538 , Global_OverlaySmoothness311_g11538 , Mask_Overlay269_g11538);
			half Final_Smoothness371_g11538 = lerpResult343_g11538;
			half Global_Wetness1016_g11538 = ( TVE_Wetness * _GlobalWetness );
			half Global_ExtrasTex_A1033_g11538 = tex2DNode7_g11563.a;
			float lerpResult1037_g11538 = lerp( Final_Smoothness371_g11538 , saturate( ( Final_Smoothness371_g11538 + Global_Wetness1016_g11538 ) ) , Global_ExtrasTex_A1033_g11538);
			o.Smoothness = lerpResult1037_g11538;
			o.Alpha = Main_Alpha316_g11538;
			half Main_AlphaRaw1203_g11538 = tex2DNode29_g11538.a;
			half Alpha5_g11604 = Main_AlphaRaw1203_g11538;
			#ifdef _ALPHATEST_ON
				float staticSwitch2_g11604 = Alpha5_g11604;
			#else
				float staticSwitch2_g11604 = 1.0;
			#endif
			half Final_Clip914_g11538 = staticSwitch2_g11604;
			clip( Final_Clip914_g11538 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "TVEShaderCoreGUI"
}
/*ASEBEGIN
Version=17802
1927;1;1906;1020;2824.299;633.0284;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;261;-1584,-512;Inherit;False;Property;_SubsurfaceMessageHD1;!!! Subsurface Message HD !!!;3;0;Create;False;0;0;False;1;StyledMessage(Info, Diffusion Profiles are not directly supported. You need to create an HDRP Lit material and assign a Diffusion Profile then drag it to the Subsurface Diffusion Material slot., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;81;-2032,-768;Half;False;Property;_IsCrossShader;_IsCrossShader;152;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-2176,-768;Half;False;Property;_Banner;Banner;0;0;Create;True;0;0;True;1;StyledBanner(Cross Standard Lit);0;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;259;-1888,-512;Inherit;False;Property;_SubsurfaceMessageUniversal1;!!! Subsurface Message Universal !!!;2;0;Create;False;0;0;False;1;StyledMessage(Info, In Universal Render Pipeline the Subsurface only work for the Directional light. Other light types are not supported, 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;260;-2176,-512;Inherit;False;Property;_SubsurfaceMessageStandard1;!!! Subsurface Message !!!;1;0;Create;False;0;0;True;1;StyledMessage(Info, Subsurface shading requires Directional light and Ambient light higher than 0 to work. Other light types are not supported., 5, 5 );0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1632,-640;Half;False;Property;__zw;__zw;157;1;[HideInInspector];Fetch;False;2;Opaque;0;Transparent;1;0;True;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-1888,-640;Half;False;Property;__src;__src;155;1;[HideInInspector];Fetch;False;0;0;True;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;141;-1568,-768;Half;False;Property;_IsLitShader;_IsLitShader;151;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-2176,-640;Half;False;Property;_Cutoff;_Cutoff;153;1;[HideInInspector];Fetch;False;4;Alpha;0;Premultiply;1;Additive;2;Multiply;3;0;True;0;0.5;0.719;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;129;-1808,-768;Half;False;Property;_IsStandardShader;_IsStandardShader;150;1;[HideInInspector];Create;True;0;0;True;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-2016,-640;Half;False;Property;__cull;__cull;154;2;[HideInInspector];[Enum];Fetch;False;3;Both;0;Back;1;Front;2;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-1760,-640;Half;False;Property;__dst;__dst;156;1;[HideInInspector];Fetch;False;2;Opaque;0;Transparent;1;0;True;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;264;-2176,-128;Inherit;False;Base;4;;11538;856f7164d1c579d43a5cf4968a75ca43;20,1034,1,860,1,1000,1,995,1,847,0,1114,0,850,1,853,0,1271,1,1298,1,1300,1,866,1,844,0,883,0,879,0,881,0,878,0,888,0,893,0,916,1;0;16;FLOAT3;0;FLOAT3;528;FLOAT;529;FLOAT;530;FLOAT;531;FLOAT;1235;FLOAT3;1230;FLOAT;1290;FLOAT;721;FLOAT;532;FLOAT;653;FLOAT;892;FLOAT3;534;FLOAT4;535;FLOAT;629;FLOAT;722
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-1376,-128;Float;False;True;-1;2;TVEShaderCoreGUI;0;0;Standard;BOXOPHOBIC/The Vegetation Engine/Billboards/Cross Standard Lit;False;False;False;False;False;True;False;True;False;False;False;False;True;True;True;False;True;False;False;False;True;Back;0;True;17;0;False;-1;True;0;False;-1;-1;False;-1;False;0;Custom;0.719;True;True;0;True;Opaque;;Geometry;All;11;d3d11;glcore;gles3;metal;vulkan;xbox360;xboxone;ps4;psp2;n3ds;wiiu;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;1;0;True;20;0;True;7;0;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Absolute;0;;-1;-1;-1;-1;0;False;0;0;True;10;-1;0;True;21;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;37;-2176,-896;Inherit;False;1023.392;100;Internal;0;;1,0.252,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;33;-2176,-256;Inherit;False;1024.392;100;Final;0;;0.3439424,0.5960785,0,1;0;0
WireConnection;0;0;264;0
WireConnection;0;1;264;528
WireConnection;0;3;264;529
WireConnection;0;4;264;530
WireConnection;0;9;264;532
WireConnection;0;10;264;653
WireConnection;0;11;264;534
ASEEND*/
//CHKSM=5A745B89258E520BB1232B3629BB8BDA5625FA96