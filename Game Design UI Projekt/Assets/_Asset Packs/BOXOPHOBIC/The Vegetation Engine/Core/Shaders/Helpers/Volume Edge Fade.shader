// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Hidden/The Vegetation Engine/Volume Edge Fade"
{
	Properties
	{
		[HideInInspector]_IsStandardPipeline("_IsStandardPipeline", Float) = 0
		[HideInInspector]_IsTVEShader("_IsTVEShader", Float) = 1
		[HideInInspector]_ColorModeOptions("Color Mode Options", Vector) = (0,0,0,0)
		[HideInInspector]_MotionDirectionColor("MotionDirectionColor", Color) = (0.4980392,0.4980392,0,1)
		[IntRange]__priority("Priority", Range( -50 , 50)) = 0

	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
	LOD 0

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Back
		ColorMask RGBA
		ZWrite Off
		ZTest LEqual
		
		
		
		Pass
		{
			Name "Unlit"
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM

			

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			//only defining to not throw compilation error over Unity 5.5
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 worldPos : TEXCOORD0;
#endif
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord1 : TEXCOORD1;
			};

			uniform half __priority;
			uniform half _IsTVEShader;
			uniform half _IsStandardPipeline;
			uniform half4 _ColorModeOptions;
			uniform half4 _MotionDirectionColor;

			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.ase_texcoord1.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord1.zw = 0;
				float3 vertexValue = float3(0, 0, 0);
				#if ASE_ABSOLUTE_VERTEX_POS
				vertexValue = v.vertex.xyz;
				#endif
				vertexValue = ( half3(0,-0.01,0) + ( _IsStandardPipeline * 0.0 ) );
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);

#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
#endif
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
#ifdef ASE_NEEDS_FRAG_WORLD_POSITION
				float3 WorldPosition = i.worldPos;
#endif
				half4 color40 = IsGammaSpace() ? half4(1,1,1,1) : half4(1,1,1,1);
				half4 color41 = IsGammaSpace() ? half4(0.4980392,0.4980392,0.4980392,1) : half4(0.2122307,0.2122307,0.2122307,1);
				half2 temp_cast_0 = (0.1).xx;
				half2 temp_cast_1 = (0.9).xx;
				half2 temp_cast_2 = (-1.0).xx;
				half2 temp_cast_3 = (1.0).xx;
				half2 temp_cast_4 = (-8.0).xx;
				half2 break25 = saturate( pow( abs( (temp_cast_2 + (i.ase_texcoord1.xy - temp_cast_0) * (temp_cast_3 - temp_cast_2) / (temp_cast_1 - temp_cast_0)) ) , temp_cast_4 ) );
				half4 appendResult28 = (half4((( ( _ColorModeOptions.x * color40 ) + ( _ColorModeOptions.y * color41 ) + ( _ColorModeOptions.z * _MotionDirectionColor ) )).rgb , ( 1.0 - ( break25.x * break25.y ) )));
				
				
				finalColor = appendResult28;
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "TVEShaderElementGUI"
	
	
}
/*ASEBEGIN
Version=17801
1927;1;1906;1020;2315.197;3186.526;3.151062;True;False
Node;AmplifyShaderEditor.RangedFloatNode;20;-384,-608;Half;False;Constant;_Float4;Float 4;2;0;Create;True;0;0;False;0;-1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;34;-384,-528;Half;False;Constant;_Float5;Float 5;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;18;-384,-688;Half;False;Constant;_Float2;Float 2;2;0;Create;True;0;0;False;0;0.9;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;14;-384,-896;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;17;-384,-768;Half;False;Constant;_Float0;Float 0;2;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;16;-128,-896;Inherit;False;5;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT2;1,0;False;3;FLOAT2;0,0;False;4;FLOAT2;1,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;23;128,-768;Half;False;Constant;_Float3;Float 3;2;0;Create;True;0;0;False;0;-8;-32;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;21;64,-896;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PowerNode;22;320,-896;Inherit;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ColorNode;12;-384,-1408;Inherit;False;Property;_MotionDirectionColor;MotionDirectionColor;6;1;[HideInInspector];Create;True;0;0;False;0;0.4980392,0.4980392,0,1;0.4980392,0.4980392,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;39;-384,-2048;Half;False;Property;_ColorModeOptions;Color Mode Options;5;1;[HideInInspector];Create;False;0;0;False;0;0,0,0,0;0,1,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;27;512,-896;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ColorNode;40;-384,-1792;Inherit;False;Constant;_Color1;Color 1;2;0;Create;True;0;0;False;0;1,1,1,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;41;-384,-1600;Inherit;False;Constant;_Color2;Color 2;2;0;Create;True;0;0;False;0;0.4980392,0.4980392,0.4980392,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;-64,-1408;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;25;672,-896;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-64,-1600;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;-64,-1792;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;960,-896;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;44;128,-1792;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;50;1408,-480;Inherit;False;Is Pipeline;0;;1;6a59a34c4be5db64ca90ee69227573b8;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;30;1408,-640;Half;False;Constant;_Vector0;Vector 0;3;0;Create;True;0;0;False;0;0,-0.01,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SwizzleNode;13;320,-1792;Inherit;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;35;1152,-896;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;49;1664,-640;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;28;1408,-896;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;1;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;45;128,-2432;Half;False;Property;__priority;Priority;7;1;[IntRange];Create;False;0;0;True;0;0;0;-50;50;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;46;-384,-2432;Half;False;Property;_IsTVEShader;_IsTVEShader;4;1;[HideInInspector];Create;False;0;0;True;0;1;1;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;1792,-896;Half;False;True;-1;2;TVEShaderElementGUI;0;1;Hidden/The Vegetation Engine/Volume Edge Fade;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;True;0;False;-1;0;False;-1;True;False;True;0;False;-1;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;0;False;-1;True;False;0;False;-1;0;False;-1;True;2;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;True;2;0;False;False;False;False;False;False;False;False;False;True;1;LightMode=ForwardBase;False;0;;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;;0
Node;AmplifyShaderEditor.CommentaryNode;48;-384,-2560;Inherit;False;1025.136;100;Internal;0;;1,0.252,0,1;0;0
WireConnection;16;0;14;0
WireConnection;16;1;17;0
WireConnection;16;2;18;0
WireConnection;16;3;20;0
WireConnection;16;4;34;0
WireConnection;21;0;16;0
WireConnection;22;0;21;0
WireConnection;22;1;23;0
WireConnection;27;0;22;0
WireConnection;43;0;39;3
WireConnection;43;1;12;0
WireConnection;25;0;27;0
WireConnection;42;0;39;2
WireConnection;42;1;41;0
WireConnection;37;0;39;1
WireConnection;37;1;40;0
WireConnection;31;0;25;0
WireConnection;31;1;25;1
WireConnection;44;0;37;0
WireConnection;44;1;42;0
WireConnection;44;2;43;0
WireConnection;13;0;44;0
WireConnection;35;0;31;0
WireConnection;49;0;30;0
WireConnection;49;1;50;0
WireConnection;28;0;13;0
WireConnection;28;3;35;0
WireConnection;0;0;28;0
WireConnection;0;1;49;0
ASEEND*/
//CHKSM=870EC69AFD1817B44F06DFA624393E7514FEB996