﻿// Cristian Pop - https://boxophobic.com/

using UnityEngine;
using System.Collections.Generic;
using Boxophobic.StyledGUI;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace TheVegetationEngine
{
#if UNITY_EDITOR
    [ExecuteInEditMode]
    [AddComponentMenu("BOXOPHOBIC/The Vegetation Engine/TVE Prefab")]
#endif
    public class TVEPrefab : StyledMonoBehaviour
    {
#if UNITY_EDITOR
        [StyledBanner(0.890f, 0.745f, 0.309f, "Prefab", "", "")]
        public bool styledBanner;

        [HideInInspector]
        public GameObject storedPrefabBackup;
        [HideInInspector]
        public Vector4 storedMaxBoundsInfo;
        [HideInInspector]
        public string storedPresetPath;
#endif
        [HideInInspector]
        public List<Material> materials;

        //void GetAllMaterials()
        //{
        //    materials = new List<Material>();

        //    Renderer[] children;
        //    children = GetComponentsInChildren<Renderer>();

        //    for (int i = 0; i < children.Length; i++)
        //    {
        //        var subMaterials = children[i].sharedMaterials;
        //        for (int m = 0; m < subMaterials.Length; m++)
        //        {
        //            materials.Add(subMaterials[m]);
        //        }
        //    }
        //}
    }
}


