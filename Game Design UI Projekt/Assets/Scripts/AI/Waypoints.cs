﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour
{
   [SerializeField] float wayPointsSize;

    private void OnDrawGizmos()
    {        
        for (int i = 0; i < transform.childCount; i++)
        {
            Gizmos.DrawSphere(transform.GetChild(i).position, wayPointsSize);
            Gizmos.DrawLine(transform.GetChild(GetWaypoints(i)).position, transform.GetChild(GetWaypoints(i + 1)).position);
        }
    }

    private int GetWaypoints(int i)
    {
        if(i >= transform.childCount)
        {
            return 0;
        }

        else
        {
            return i;
        }
    }
}
