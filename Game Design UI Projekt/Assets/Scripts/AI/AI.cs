﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;


namespace RPG.Control
{

    public class AI : MonoBehaviour
    {
        [SerializeField]private Transform player;

        private NavMeshAgent navMeshAgent;
        private Animator animator;

        [SerializeField] float playerDetectionRadius;
        [SerializeField] private float enemySearchRadiusAfterChase;
        private bool searchPlayer = false;

        [SerializeField] private float maxSearchDuraction;
        private float searchCooldown;

        [SerializeField] private float rotationSpeed = 10;

        private float movementSpeed;
        private float attackRange;
        private bool playAudio = true;
        private float currentHealth;
        [SerializeField] private float waitForNextPath;
        private float waitCooldown;

        private float chaseTime;
        [SerializeField] private float maxChaseDuration;

        [SerializeField] private GameObject waypoints;
        private int counter;

        private bool lastChasing = false;
        private bool patrolling = true;

        void Start()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
            animator = GetComponent<Animator>();

            movementSpeed = navMeshAgent.speed;

            attackRange = navMeshAgent.stoppingDistance;

            chaseTime = maxChaseDuration;

            searchCooldown = maxSearchDuraction;

        }

        void Update()
        {
            if (Attack()) return;
            SearchForPlayer();

            if (searchPlayer == false)
            {
                Chase();
            }

            animator.SetFloat("movementSpeed", navMeshAgent.speed);
        }

        private void PlayerVoiceAtKill()
        {
            if(playAudio)
            {
                playAudio = false;
            }
        }

        private void MeleeAttack()
        {

        }

        public float GetHealth()
        {
            return 0;
        }

        private float GetDistanceToPlayer()
        {
            return Vector3.Distance(transform.position, player.transform.position);
        }

        private void RotateToPlayer()
        {
            Vector3 direction =  new Vector3(player.transform.position.x - transform.position.x, 0, player.transform.position.z - transform.position.z);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction, Vector3.up), rotationSpeed * Time.deltaTime);
        }

        private void Patrolling()
        {
            navMeshAgent.stoppingDistance = 0;
            if (!navMeshAgent.hasPath)
            {
                navMeshAgent.speed = 0;
                waitCooldown -= Time.deltaTime;

                if(waitCooldown <= 0)
                {
                    navMeshAgent.speed = movementSpeed / 2;

                    if (counter >= waypoints.transform.childCount)
                    {
                        navMeshAgent.SetDestination(waypoints.transform.GetChild(counter - 1).position);
                        counter = 0;
                        return;
                    }
                    else if (counter >= 0)
                    {
                        navMeshAgent.SetDestination(waypoints.transform.GetChild(counter).position);
                    }
                    counter++;
                    waitCooldown = waitForNextPath;
                }

            }
        }

        private void Chase()
        {
            ChaseWhenTakeDamage();

            if (GetDistanceToPlayer() < playerDetectionRadius && GetDistanceToPlayer() > navMeshAgent.stoppingDistance)
            {
                searchPlayer = false;
                patrolling = false;
                chaseTime = maxChaseDuration;
                navMeshAgent.ResetPath();
                navMeshAgent.stoppingDistance = attackRange;
                navMeshAgent.SetDestination(player.transform.position);
                navMeshAgent.speed = movementSpeed;
                animator.SetBool("attack", false);
                chaseTime = maxChaseDuration;
                lastChasing = true;
            }

            else if (lastChasing == true)
            {
                LastChase();
            }

            else if(patrolling == true)
            {
                Patrolling();
            }

        }

        private void LastChase()
        {
            if(GetDistanceToPlayer() > playerDetectionRadius)
            {
                chaseTime -= Time.deltaTime;
                navMeshAgent.SetDestination(player.transform.position);
                navMeshAgent.speed = movementSpeed;
                patrolling = false;

                if (chaseTime <= 0)
                {
                    searchPlayer = true;
                    navMeshAgent.ResetPath();
                    currentHealth = GetHealth();
                    chaseTime = maxChaseDuration;
                    lastChasing = false;
                }
            }
            
        }

        private bool ChaseWhenTakeDamage()
        {
            if (GetHealth() < currentHealth)
            {
                LastChase();
                return true;
            }
            return false;
        }

        private void SearchForPlayer()
        {
            if(searchPlayer == true)
            {
                navMeshAgent.speed = movementSpeed / 2;
                navMeshAgent.stoppingDistance = 0;
                searchCooldown -= Time.deltaTime;

                if (!navMeshAgent.hasPath && searchCooldown > 0)
                {
                    Vector3 destination = transform.position + new Vector3(UnityEngine.Random.Range(-enemySearchRadiusAfterChase, enemySearchRadiusAfterChase), 0, UnityEngine.Random.Range(-enemySearchRadiusAfterChase, enemySearchRadiusAfterChase));
                    navMeshAgent.SetDestination(destination);
                }
                else if (searchCooldown < 0)
                {
                    navMeshAgent.ResetPath();
                    searchCooldown = maxSearchDuraction;
                    patrolling = true;
                    searchPlayer = false;
                }
                else if(GetDistanceToPlayer() < playerDetectionRadius)
                {
                    searchCooldown = maxSearchDuraction;
                    searchPlayer = false;

                }
                else if(GetHealth() < currentHealth)
                {
                    searchCooldown = maxSearchDuraction;
                    searchPlayer = false;
                }               
            }
        }

        private bool Attack()
        {
            navMeshAgent.stoppingDistance = attackRange;
            if (GetDistanceToPlayer() < navMeshAgent.stoppingDistance)
            {
                currentHealth = GetHealth();
                RotateToPlayer();
                navMeshAgent.ResetPath();
                navMeshAgent.speed = 0;
                animator.SetBool("attack", true);
                return true;
            }
            return false;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, playerDetectionRadius);
        }
    }
}
